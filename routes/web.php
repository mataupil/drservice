<?php

use Illuminate\Support\Facades\Route;
$url_root = 'App\Http\Controllers';
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*************************************** USUARIO  *******************************************************/
Route::get('/api/security/locales', $url_root.'\Security\UserController@getLocales'); 
Route::post('/api/security/login', $url_root.'\Security\UserController@login'); // ok
Route::post('/api/security/registrar-usuario', $url_root.'\Security\UserController@registrarUsuario'); // ok
Route::post('/api/security/buscar-usuario/{row_by_page}', $url_root.'\Security\UserController@buscarUsuario'); // ok
Route::get('/api/security/buscar-usuario-id/{id}', $url_root.'\Security\UserController@buscarUsuarioID'); // ok
Route::put('/api/security/actualizar-usuario/{id}', $url_root.'\Security\UserController@actualizarUsuario'); 
Route::delete('/api/security/eliminar-usuario/{id_emp}', $url_root.'\Security\UserController@eliminarUsuario'); // ok

/*************************************** CLIENTE  *******************************************************/
Route::post('/api/principal/registrar-cliente', $url_root.'\Principal\ClienteController@registrarCliente'); // ok
Route::get('/api/principal/buscar-cliente-id/{id}/{id_user_auth?}', $url_root.'\Principal\ClienteController@buscarClienteID'); // ok
Route::post('/api/principal/buscar-cliente/{row_by_page}', $url_root.'\Principal\ClienteController@buscarCliente'); // ok
Route::put('/api/principal/actualizar-cliente/{id}', $url_root.'\Principal\ClienteController@actualizarCliente');
Route::delete('/api/principal/eliminar-cliente/{id}', $url_root.'\Principal\ClienteController@eliminarCliente'); // ok

/*************************************** DIRECCIÓN  *******************************************************/
Route::post('/api/principal/registrar-direccion', $url_root.'\Principal\ClienteController@registrarDireccion'); // ok
Route::get('/api/principal/listar-direcciones/{id_cli}', $url_root.'\Principal\ClienteController@listarDirecciones'); // ok
Route::get('/api/principal/buscar-direccion-id/{id}', $url_root.'\Principal\ClienteController@buscarDireccionID'); // ok
Route::put('/api/principal/actualizar-direccion/{id}', $url_root.'\Principal\ClienteController@actualizarDireccion'); // ok
Route::delete('/api/principal/eliminar-direccion/{id}/{id_cli}', $url_root.'\Principal\ClienteController@eliminarDireccion');  // ok
Route::post('/api/principal/set-direccion-default', $url_root.'\Principal\ClienteController@setDireccionDefault'); // ok

/*************************************** REV  *******************************************************/
Route::get('/api/principal/clientebyid/{id}', $url_root.'\Principal\ClienteConstroller@listarClienteById');
Route::post('/api/principal/actualizacliente/{id}', $url_root.'\Principal\ClienteConstroller@actualizarCliente');
Route::get('/api/principal/direccionclientebyid/{id}', $url_root.'\Principal\ClienteConstroller@direccionClienteById');
Route::post('/api/principal/agregardireccioncliente', $url_root.'\Principal\ClienteConstroller@agregarDireccionCliente');
Route::post('/api/principal/actualizardireccionclienteactive/{id1}/{id2}', $url_root.'\Principal\ClienteConstroller@actualizarDireccionClienteActive');

/*************************************** PROVEEDOR  *******************************************************/
Route::post('/api/principal/registrar-proveedor', $url_root.'\Principal\ProveedorController@registrarProveedor'); // ok
Route::get('/api/principal/listar-proveedores', $url_root.'\Principal\ProveedorController@listarProveedores'); // ok
Route::get('/api/principal/buscar-proveedor-id/{id}', $url_root.'\Principal\ProveedorController@buscarProveedorID'); // ok
Route::put('/api/principal/actualizar-proveedor/{id}', $url_root.'\Principal\ProveedorController@actualizarProveedor'); // ok
Route::delete('/api/principal/eliminar-proveedor/{id}', $url_root.'\Principal\ProveedorController@eliminarProveedor'); // ok

/*************************************** REV  *******************************************************/
Route::get('/api/principal/listarproveedor', $url_root.'\Principal\ProveedorController@listarProveedor');
Route::post('/api/principal/insertarproveedor', $url_root.'\Principal\ProveedorController@insertarProveedor');
Route::get('/api/principal/listarproveedorbyid/{id}', $url_root.'\Principal\ProveedorController@listarProveedorById');
Route::post('/api/principal/actualizarproveedor/{id}', $url_root.'\Principal\ProveedorController@actualizarProveedor'); 
Route::post('/api/principal/eliminarproveedorbyid/{id}', $url_root.'\Principal\ProveedorController@eliminarProveedorById');

/*************************************** CONTACTO  *******************************************************/
Route::post('/api/principal/registrar-contacto/{id_pro}', $url_root.'\Principal\ProveedorController@registrarContacto'); // ok
Route::get('/api/principal/listar-contactos/{id_pro}', $url_root.'\Principal\ProveedorController@listarContactos'); // ok
Route::get('/api/principal/buscar-contacto-id/{id}', $url_root.'\Principal\ProveedorController@buscarContactoID'); // ok
Route::put('/api/principal/actualizar-contacto/{id}', $url_root.'\Principal\ProveedorController@actualizarContacto'); // ok
Route::delete('/api/principal/eliminar-contacto/{id}', $url_root.'\Principal\ProveedorController@eliminarContacto'); // ok

/*************************************** LOCAL  *******************************************************/
Route::post('/api/principal/registrar-local', $url_root.'\Principal\LocalController@registrarLocal'); // ok
Route::get('/api/principal/listar-locales', $url_root.'\Principal\LocalController@listarLocales'); // ok
Route::get('/api/principal/buscar-local-id/{id}', $url_root.'\Principal\LocalController@buscarLocalID'); // ok
Route::put('/api/principal/actualizar-local/{id}', $url_root.'\Principal\LocalController@actualizarLocal'); // ok
Route::delete('/api/principal/eliminar-local/{id}', $url_root.'\Principal\LocalController@eliminarLocal'); // ok

/*************************************** PEDIDO  *******************************************************/
Route::post('/api/ventas/registrar-pedido', $url_root.'\Ventas\PedidoController@registrarPedido'); // ok
Route::get('/api/ventas/listar-pedidos', $url_root.'\Ventas\PedidoController@listarPedidos'); // ok
Route::get('/api/ventas/buscar-pedido-id/{id}', $url_root.'\Ventas\PedidoController@buscarPedidoID'); // ok
Route::put('/api/ventas/actualizar-pedido/{id}', $url_root.'\Ventas\PedidoController@actualizarPedido'); // ok
Route::delete('/api/ventas/eliminar-pedido/{id}', $url_root.'\Ventas\PedidoController@eliminarPedido'); // ok

/*************************************** UBIGEO  *******************************************************/
Route::get('/api/ubigeo/listar-departments', $url_root.'\UbigeoController@getDepartments'); // ok
Route::get('/api/ubigeo/listar-provinces-id/{department_id}', $url_root.'\UbigeoController@getProvinces'); // ok
Route::get('/api/ubigeo/listar-districts-id/{province_id}', $url_root.'\UbigeoController@getDistricts'); // ok

/*************************************** ROL  *******************************************************/
Route::post('/api/mantenimiento/registrar-rol', $url_root.'\Mantenimiento\RolController@registrarRol'); // ok
Route::get('/api/mantenimiento/listar-roles', $url_root.'\Mantenimiento\RolController@listarRoles'); // ok
Route::get('/api/mantenimiento/buscar-rol-id/{id}', $url_root.'\Mantenimiento\RolController@buscarRolID'); // ok
Route::put('/api/mantenimiento/actualizar-rol/{id}', $url_root.'\Mantenimiento\RolController@actualizarRol'); // ok
Route::delete('/api/mantenimiento/eliminar-rol/{id}', $url_root.'\Mantenimiento\RolController@eliminarRol'); // ok

/******************************************** EMPLEADO ************************************************************* */
Route::get('/api/principal/listarempleado', $url_root.'\Principal\EmpleadoController@listarEmpleado');
Route::get('/api/principal/listamozos', $url_root.'\Principal\EmpleadoController@listarMozos');
Route::post('/api/principal/insertarempleado', $url_root.'\Principal\EmpleadoController@insertarEmpleado');
Route::get('/api/principal/listarempleadobyid/{id}', $url_root.'\Principal\EmpleadoController@listarEmpleadoById');
Route::post('/api/principal/actualizarempleado/{id}', $url_root.'\Principal\EmpleadoController@actualizarEmpleado'); 
Route::get('/api/principal/listarempleadolocalbyid/{id}', $url_root.'\Principal\EmpleadoController@listarEmpleadoLocalByid'); 
Route::post('/api/principal/agregarempleadolocal', $url_root.'\Principal\EmpleadoController@agregarEmpleadoLocal'); 
Route::post('/api/principal/quitarempleadolocalbyid/{id1}/{id2}', $url_root.'\Principal\EmpleadoController@quitarEmpleadoLocalById'); 



/**************************************** LOCAL ***********************************************/
Route::get('/api/principal/listarlocal', $url_root.'\Principal\LocalController@listarLocal');
Route::post('/api/principal/insertarLocal', $url_root.'\Principal\LocalController@insertarLocal');
Route::get('/api/principal/listarlocalbyid/{id}', $url_root.'\Principal\LocalController@listarLocalById');
Route::post('/api/principal/actualizarlocal/{id}', $url_root.'\Principal\LocalController@actualizarLocal'); 


/**************************************** MATERIA PRIMA ***********************************************/
Route::get('/api/inventario/listarmateriaprima', $url_root.'\Inventario\MateriaPrimaController@listarMateriaPrima');
Route::post('/api/inventario/insertarmateriaprima', $url_root.'\Inventario\MateriaPrimaController@insertarMateriaPrima');
Route::get('/api/inventario/obtenermateriaprimabyid/{id}', $url_root.'\Inventario\MateriaPrimaController@obtenerMateriaPrimaById');
Route::get('/api/inventario/obtenerMateriaprimalocalbyid/{id}', $url_root.'\Inventario\MateriaPrimaController@obtenerMateriaPrimaLocalById');
Route::post('/api/inventario/actualizarmateriaprima/{id}', $url_root.'\Inventario\MateriaPrimaController@actualizarMateriaPrima'); 
Route::post('/api/inventario/agregarmateriaprimalocal', $url_root.'\Inventario\MateriaPrimaController@agregarMateriaPrimaLocal'); 
Route::post('/api/inventario/quitarmateriaprimalocalbyid/{id1}/{id2}', $url_root.'\Inventario\MateriaPrimaController@quitarMateriaPrimaLocalById'); 

/**************************************** ALMACEN ***********************************************/
Route::get('/api/inventario/listaralmacen', $url_root.'\Inventario\AlmacenController@listarAlmacen');
Route::post('/api/inventario/insertaralmacen', $url_root.'\Inventario\AlmacenController@insertarAlmacen');
Route::get('/api/inventario/obteneralmacenbyid/{id}', $url_root.'\Inventario\AlmacenController@obtenerAlmacenById');
Route::post('/api/inventario/actualizaralmacen/{id}', $url_root.'\Inventario\AlmacenController@actualizarAlmacen'); 

/**************************************** PRODUCTO ***********************************************/
Route::get('/api/inventario/listarproducto', $url_root.'\Inventario\ProductoController@listarProducto');
Route::post('/api/inventario/insertarproducto', $url_root.'\Inventario\ProductoController@insertarProducto');
Route::get('/api/inventario/obtenerproductobyid/{id}', $url_root.'\Inventario\ProductoController@obtenerProductoById');
Route::post('/api/inventario/actualizarproducto/{id}', $url_root.'\Inventario\ProductoController@actualizarProducto'); 


/**************************************** PRODUCTO ***********************************************/
Route::get('/api/inventario/listarproducto', $url_root.'\Inventario\ProductoController@listarProducto');
Route::post('/api/inventario/insertarproducto', $url_root.'\Inventario\ProductoController@insertarProducto');


/**************************************** Tipo Pago ***********************************************/
Route::get('/api/mantenimiento/listartipopago', $url_root.'\Mantenimiento\TipoPagoController@listarTipoPago');
Route::post('/api/mantenimiento/insertartipopago', $url_root.'\Mantenimiento\TipoPagoController@insertarTipoPago');

Route::get('/api/mantenimiento/listartipopagobyid/{id}', $url_root.'\Mantenimiento\TipoPagoController@listarTipoPagoById');
Route::post('/api/mantenimiento/actualizartipopago/{id}', $url_root.'\Mantenimiento\TipoPagoController@actualizarTipoPago'); 
Route::post('/api/mantenimiento/eliminartipopagobyid/{id}', $url_root.'\Mantenimiento\TipoPagoController@eliminarTipoPagoById');

/**************************************** Tipo Pedido ***********************************************/
Route::get('/api/mantenimiento/listartipopedido', $url_root.'\Mantenimiento\TipoPedidoController@listarTipoPedido');
Route::post('/api/mantenimiento/insertartipopedido', $url_root.'\Mantenimiento\TipoPedidoController@insertarTipoPedido');

Route::get('/api/mantenimiento/listartipopedidobyid/{id}', $url_root.'\Mantenimiento\TipoPedidoController@listarTipoPedidoById');
Route::post('/api/mantenimiento/actualizartipopedido/{id}', $url_root.'\Mantenimiento\TipoPedidoController@actualizarTipoPedido'); 

Route::post('/api/mantenimiento/eliminartipopedidobyid/{id}', $url_root.'\Mantenimiento\TipoPedidoController@eliminarTipoPedidoById');

/**************************************** VENTAS ******************************************************************/

Route::get('/api/ventas/listarPedidos', $url_root.'\Ventas\PeididosController@listarPedidos');
Route::get('/api/ventas/obtenerPedido/{id}', $url_root.'\ventas\PedidoController@obtenerPedidoById');
Route::post('/api/ventas/saveProductoPedido', $url_root.'\ventas\PedidoController@saveProductoPedido');
Route::post('/api/ventas/savePedido', $url_root.'\ventas\PedidoController@savePedido');

/**************************************** VENTAS COCINA ******************************************************************/

Route::get('/api/ventas/listapedidococina', $url_root.'\Ventas\CocinaController@listarPedidoCocina');  
Route::post('/api/ventas/actualizarencurso/{id}', $url_root.'\Ventas\CocinaController@actualizarEnCurso'); 




