-- MariaDB dump 10.19  Distrib 10.4.18-MariaDB, for Win64 (AMD64)
--
-- Host: localhost    Database: donrojas_sistema
-- ------------------------------------------------------
-- Server version	10.4.18-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ingrediente_producto`
--

DROP TABLE IF EXISTS `ingrediente_producto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingrediente_producto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chr_codproducto` char(6) CHARACTER SET utf8mb4 NOT NULL,
  `int_cantidad` int(11) DEFAULT NULL,
  `int_ingredienteid` int(11) NOT NULL,
  `int_usermodified` int(11) DEFAULT NULL,
  `int_datemodified` int(11) DEFAULT NULL,
  `int_usercreated` int(11) DEFAULT NULL,
  `int_datecreated` int(11) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT 1,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `int_proveedorid` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `XIF1ingrediente_producto` (`chr_codproducto`),
  KEY `XIF2ingrediente_producto` (`int_ingredienteid`,`int_proveedorid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingrediente_producto`
--

LOCK TABLES `ingrediente_producto` WRITE;
/*!40000 ALTER TABLE `ingrediente_producto` DISABLE KEYS */;
/*!40000 ALTER TABLE `ingrediente_producto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_almacen`
--

DROP TABLE IF EXISTS `tbl_almacen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_almacen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chr_code` varchar(6) DEFAULT NULL,
  `chr_nombre` varchar(50) DEFAULT NULL,
  `chr_descripcion` varchar(150) DEFAULT NULL,
  `int_localid` int(11) NOT NULL,
  `updated_at` varchar(45) DEFAULT NULL,
  `created_at` varchar(45) DEFAULT NULL,
  `is_active` int(11) DEFAULT NULL,
  `is_deleted` int(11) DEFAULT NULL,
  `int_usercreated` int(11) DEFAULT NULL,
  `int_usermodified` int(11) DEFAULT NULL,
  `int_datecreated` int(11) DEFAULT NULL,
  `int_datemodified` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_almacen`
--

LOCK TABLES `tbl_almacen` WRITE;
/*!40000 ALTER TABLE `tbl_almacen` DISABLE KEYS */;
INSERT INTO `tbl_almacen` VALUES (1,'L1SJM','SAN JUAN','test',1,'2021-03-11 17:31:36','2021-03-11 17:31:36',NULL,NULL,1,NULL,1615483896,NULL);
/*!40000 ALTER TABLE `tbl_almacen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_clientes`
--

DROP TABLE IF EXISTS `tbl_clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_clientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chr_dni` char(8) CHARACTER SET utf8mb4 DEFAULT NULL,
  `chr_nombre` varchar(40) CHARACTER SET utf8mb4 NOT NULL,
  `chr_apellido` varchar(60) CHARACTER SET utf8mb4 NOT NULL,
  `chr_celular` char(9) CHARACTER SET utf8mb4 DEFAULT NULL,
  `chr_direccion` varchar(150) CHARACTER SET utf8mb4 DEFAULT NULL,
  `chr_telefono` varchar(9) CHARACTER SET utf8mb4 DEFAULT NULL,
  `chr_correo` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  `is_subscriptor` int(11) DEFAULT 0,
  `int_usermodified` int(11) DEFAULT NULL,
  `int_datemodified` int(11) DEFAULT NULL,
  `int_usercreated` int(11) DEFAULT NULL,
  `int_datecreated` int(11) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT 1,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `updated_at` varchar(200) DEFAULT NULL,
  `created_at` varchar(200) DEFAULT NULL,
  `date_fecha_inicio` date DEFAULT NULL,
  `date_fecha_fin` date DEFAULT NULL,
  `int_tipo` int(11) DEFAULT NULL,
  `chr_ruc` char(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_clientes`
--

LOCK TABLES `tbl_clientes` WRITE;
/*!40000 ALTER TABLE `tbl_clientes` DISABLE KEYS */;
INSERT INTO `tbl_clientes` VALUES (5,'47271299','Luis','Tello Romero','982751100','Av. CesarVallejo 2716 int 2','2761523',NULL,0,NULL,NULL,1,1612510354,1,0,'2021-02-21 22:28:31','2021-02-05T07:32:34.000000Z',NULL,NULL,NULL,NULL),(6,'66665544','MARCO','test','976562524','Av.sevilla 24232 donde roban','4772312',NULL,0,NULL,NULL,1,1612597486,1,0,'2021-02-21T21:59:59.000000Z','2021-02-06T07:44:46.000000Z',NULL,NULL,NULL,NULL),(7,'88888','mistico','test','987256','vilal city','232323',NULL,0,NULL,NULL,1,1612637173,0,1,'2022-02-02 16:40:04','2021-02-06T18:46:13.000000Z',NULL,NULL,NULL,NULL),(8,'77767665','ROSA','ME','98726255','AV,RECILUCUION 2','4327265',NULL,0,NULL,NULL,1,1612642460,1,0,'2021-02-06 20:14:39','2021-02-06 20:14:20',NULL,NULL,NULL,NULL),(9,'44334551','Andrea','tello','76552761','av.cesar vallejos 2','2871156',NULL,0,NULL,NULL,1,1612642973,1,0,'2021-02-06 20:23:08','2021-02-06 20:22:53',NULL,NULL,NULL,NULL),(10,'44445456','roger','acuña','98652625','	az J lote 13 villa city','2897261','roger2@gmail.com',0,NULL,NULL,1,1612643228,1,0,'2021-02-06 20:36:55','2021-02-06T20:27:08.000000Z',NULL,NULL,NULL,NULL),(11,'4458546','cesar','guszam','23233222','ves 121','2223','cesar@gmail.com',0,NULL,NULL,1,1612851829,1,0,'2021-02-09 06:23:49','2021-02-09 06:23:49',NULL,NULL,NULL,NULL),(12,'33433555','pepito','gomina','98762351','luri 12','2765617','llas@gmail.com',0,NULL,NULL,1,1613103381,1,0,'2021-02-12 04:16:21','2021-02-12 04:16:21','2021-02-11','2021-02-20',NULL,NULL),(13,'55562441','maria ','rojas torres','98272653','JUANN VLE V1234','2871651','LKAA@GMAIL.COM',1,NULL,NULL,1,1613103730,1,0,'2021-02-12T04:22:10.000000Z','2021-02-12T04:22:10.000000Z','2021-02-11','2021-02-25',NULL,NULL),(14,'77778888','SANDRA','DRA','666665555',NULL,'9999111','SANDRA@GMAIL.COM',1,NULL,NULL,1,1643815093,1,0,'2022-02-02 15:18:13','2022-02-02 15:18:13',NULL,NULL,1,'98765432199'),(15,'11113334','SANDRA','DRA','666665555',NULL,'9999111','SANDRA@GMAIL.COM',1,1,1643819130,1,1643815179,1,0,'2022-02-02 16:25:30','2022-02-02 15:19:39',NULL,NULL,2,'98765432198'),(16,'66663333','GOKU','SAYAYIN','666665555',NULL,'9999111','GOKU@GMAIL.COM',1,NULL,NULL,1,1643824690,1,0,'2022-02-02 17:58:10','2022-02-02 17:58:10',NULL,NULL,1,NULL);
/*!40000 ALTER TABLE `tbl_clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_combo`
--

DROP TABLE IF EXISTS `tbl_combo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_combo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chr_codcombo` varchar(8) CHARACTER SET utf8mb4 NOT NULL,
  `chr_nombre` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL,
  `double_precio` double(10,2) DEFAULT NULL,
  `int_tipocomboid` int(11) NOT NULL,
  `int_usermodified` int(11) DEFAULT NULL,
  `int_datemodified` int(11) DEFAULT NULL,
  `int_usercreated` int(11) DEFAULT NULL,
  `int_datetimecreated` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `XIF1COMBO` (`int_tipocomboid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_combo`
--

LOCK TABLES `tbl_combo` WRITE;
/*!40000 ALTER TABLE `tbl_combo` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_combo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_contacto_proveedor`
--

DROP TABLE IF EXISTS `tbl_contacto_proveedor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_contacto_proveedor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `int_proveedor` int(11) DEFAULT NULL,
  `chr_nombre` varchar(45) NOT NULL,
  `chr_descripcion` varchar(150) DEFAULT NULL,
  `chr_telefono` varchar(9) DEFAULT NULL,
  `chr_celular` char(9) DEFAULT NULL,
  `is_active` int(11) DEFAULT 1,
  `is_deleted` int(11) DEFAULT 0,
  `int_usercreated` int(11) DEFAULT NULL,
  `int_datecreated` int(11) DEFAULT NULL,
  `int_usermodified` int(11) DEFAULT NULL,
  `int_datemodified` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_contacto_proveedor`
--

LOCK TABLES `tbl_contacto_proveedor` WRITE;
/*!40000 ALTER TABLE `tbl_contacto_proveedor` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_contacto_proveedor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_det_factura`
--

DROP TABLE IF EXISTS `tbl_det_factura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_det_factura` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `int_facturaid` int(11) NOT NULL,
  `int_usermodified` int(11) DEFAULT NULL,
  `int_datemodified` int(11) DEFAULT NULL,
  `int_usercreated` int(11) DEFAULT NULL,
  `int_datecreated` int(11) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT 1,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `XIF1DET_FACTURA` (`int_facturaid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_det_factura`
--

LOCK TABLES `tbl_det_factura` WRITE;
/*!40000 ALTER TABLE `tbl_det_factura` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_det_factura` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_direccion_cliente`
--

DROP TABLE IF EXISTS `tbl_direccion_cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_direccion_cliente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `int_clienteid` int(11) NOT NULL,
  `chr_direccion` varchar(150) CHARACTER SET utf8mb4 NOT NULL,
  `updated_at` varchar(200) DEFAULT NULL,
  `created_at` varchar(200) DEFAULT NULL,
  `is_active` int(1) NOT NULL DEFAULT 1,
  `is_delete` int(1) DEFAULT 0,
  `is_default` char(1) DEFAULT '0',
  `int_usercreated` int(11) DEFAULT NULL,
  `int_datecreated` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_direccion_cliente`
--

LOCK TABLES `tbl_direccion_cliente` WRITE;
/*!40000 ALTER TABLE `tbl_direccion_cliente` DISABLE KEYS */;
INSERT INTO `tbl_direccion_cliente` VALUES (10,5,'Sector 2 grupo 10 mz i Lote 9 - Villa El Salvador','2021-02-21 22:28:31','2021-02-05 07:32:34',1,0,'0',NULL,NULL),(11,5,'Av. CesarVallejo 2716 int 2','2021-02-21 22:28:31','2021-02-05 08:01:16',0,0,'1',NULL,NULL),(12,6,'Av.sevilla 24232 donde roban','2021-02-21 21:59:59','2021-02-06 07:44:46',0,0,'1',NULL,NULL),(13,6,'Av. Las sirenitas 50so','2021-02-21 21:59:59','2021-02-06 08:03:47',1,0,'0',NULL,NULL),(14,7,'vilal city','2022-02-02 16:41:46','2021-02-06 18:46:13',0,1,'1',NULL,NULL),(15,7,'villa city 2','2022-02-02 16:41:46','2021-02-06 18:47:31',0,1,'0',NULL,NULL),(16,7,'villa 3','2021-02-21 22:00:59','2021-02-06 18:50:52',0,1,'0',NULL,NULL),(17,5,'SJL 1','2021-02-21 22:28:31','2021-02-06 20:13:36',0,0,'0',NULL,NULL),(18,8,'AV,CANEVARO','2021-02-06 20:14:39','2021-02-06 20:14:20',1,0,'0',NULL,NULL),(19,8,'AV,RECILUCUION 2','2021-02-06 20:14:39','2021-02-06 20:14:30',0,0,'1',NULL,NULL),(20,9,'av.cesarvallejo 120','2021-02-06 20:23:08','2021-02-06 20:22:53',1,0,'0',NULL,NULL),(21,9,'av.cesar vallejos 2','2021-02-06 20:23:08','2021-02-06 20:23:06',0,0,'1',NULL,NULL),(22,10,'az J lote 12 villa city','2021-02-06 20:36:55','2021-02-06 20:27:08',1,0,'0',NULL,NULL),(23,10,'	az J lote 13 villa city','2021-02-06 20:36:55','2021-02-06 20:36:51',0,0,'1',NULL,NULL),(24,11,'ves 121','2021-02-09 06:23:49','2021-02-09 06:23:49',1,0,'1',NULL,NULL),(25,12,'luri 12','2021-02-12 04:16:21','2021-02-12 04:16:21',1,0,'1',NULL,NULL),(26,13,'JUANN VLE V1234','2021-02-12 04:22:10','2021-02-12 04:22:10',1,0,'1',NULL,NULL),(27,5,'av.los parques','2021-02-21 22:28:31','2021-02-21 07:47:09',0,0,'0',NULL,NULL),(28,14,'DIREC PRUEBA','2022-02-02 15:18:13','2022-02-02 15:18:13',1,0,'1',NULL,NULL),(29,15,'DIREC PRUEBA','2022-02-02 15:19:39','2022-02-02 15:19:39',1,0,'1',NULL,NULL),(30,16,'DIREC GOKU','2022-02-02 20:30:46','2022-02-02 17:58:10',1,0,'0',NULL,NULL),(31,16,'LIMA SUR','2022-02-02 18:11:02','2022-02-02 17:59:29',1,0,'0',NULL,NULL),(38,16,'lima ZAK','2022-02-02 20:32:46','2022-02-02 18:16:04',0,1,'0',NULL,NULL),(39,16,'LIMA SUR F','2022-02-02 20:01:23','2022-02-02 18:22:39',1,0,'0',1,1643826159);
/*!40000 ALTER TABLE `tbl_direccion_cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_empleados`
--

DROP TABLE IF EXISTS `tbl_empleados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_empleados` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chr_codempleado` char(6) CHARACTER SET utf8mb4 NOT NULL,
  `chr_nombre` varchar(80) CHARACTER SET utf8mb4 NOT NULL,
  `chr_apellido` varchar(80) CHARACTER SET utf8mb4 NOT NULL,
  `chr_telefono` varchar(9) CHARACTER SET utf8mb4 DEFAULT NULL,
  `chr_celular` varchar(9) CHARACTER SET utf8mb4 NOT NULL,
  `chr_direccion` varchar(80) CHARACTER SET utf8mb4 NOT NULL,
  `chr_documento` varchar(8) CHARACTER SET utf8mb4 NOT NULL,
  `chr_correo` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `chr_distrito` char(18) CHARACTER SET utf8mb4 DEFAULT NULL,
  `chr_provincia` char(18) CHARACTER SET utf8mb4 DEFAULT NULL,
  `chr_departamento` char(18) CHARACTER SET utf8mb4 DEFAULT NULL,
  `chr_password` char(255) CHARACTER SET utf8mb4 NOT NULL,
  `chr_codcargo` char(6) CHARACTER SET utf8mb4 DEFAULT NULL,
  `int_usercreated` int(11) DEFAULT NULL,
  `int_datecreated` int(11) DEFAULT NULL,
  `int_usermodified` int(11) DEFAULT NULL,
  `int_datemodified` int(11) DEFAULT NULL,
  `is_active` int(11) DEFAULT 1,
  `is_deleted` int(11) DEFAULT 0,
  `chr_urlfoto` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL,
  `created_at` varchar(45) DEFAULT NULL,
  `updated_at` varchar(45) DEFAULT NULL,
  `int_rolid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `XIF1EMPLEADOS` (`chr_codcargo`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_empleados`
--

LOCK TABLES `tbl_empleados` WRITE;
/*!40000 ALTER TABLE `tbl_empleados` DISABLE KEYS */;
INSERT INTO `tbl_empleados` VALUES (1,'LUTE47','LUIS','TELLO ROMERO','982751100','982751100','villa22 city tu terror','47271299','luistello652@gmaiil.com','VILLA EL SALVADOR','LIMA','LIMA','47271299',NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,NULL,4),(2,'FEAM88','felipe','amau','2976251','9887788','villa3','88788777','felia@gmail.com',NULL,NULL,NULL,'88788777',NULL,1,1613831859,NULL,NULL,1,0,NULL,'2021-02-20T14:37:39.000000Z','2021-02-20T14:37:39.000000Z',4),(3,'MABR22','marco','brito','23872327','989834','sdfasd','222222','mara@gmail.com',NULL,NULL,NULL,'222222',NULL,1,1613834611,NULL,NULL,1,0,NULL,'2021-02-20 15:23:31','2021-02-20 15:23:31',4),(4,'ROPE87','ROSA','PEREZ','2782111','987826887','av. sanboraj123','8777829','ROSA@gmail.com',NULL,NULL,NULL,'8777829',NULL,1,1613944579,NULL,NULL,1,0,NULL,'2021-02-21 21:56:19','2021-02-21 21:56:19',4),(5,'JORO48','JOSE','ROJAS','292345754','987654231','sjm','48986754','jose.rojas@gmail.com',NULL,NULL,NULL,'48986754',NULL,1,1613946833,NULL,NULL,1,0,NULL,'2021-02-21 22:33:53','2021-02-21 22:33:53',4),(8,'SAKSA','nombre prueba','apellido prueba',NULL,'123456789','direc prueba','88882222','prueba@gmail.com','distrito A','provincia A','departamento A','5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8',NULL,1,1643670143,NULL,NULL,1,0,'url foto','2022-01-31 23:02:23','2022-01-31 23:02:23',1),(9,'YURIA','nombre prueba','apellido prueba',NULL,'123456789','direc prueba','88882221','prueba@gmail.com','distrito A','provincia A','departamento A','5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8',NULL,1,1643726781,NULL,NULL,1,0,'url foto','2022-02-01 14:46:21','2022-02-01 14:46:21',1),(10,'ZEREF','nombre prueba','apellido prueba',NULL,'123456782','direc prueba','88882229','zeref@gmail.com','distrito A','provincia A','departamento A','5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8',NULL,1,1643729272,NULL,NULL,1,0,'url foto','2022-02-01 15:27:52','2022-02-01 15:27:52',1),(11,'KING','nombre prueba','apellido prueba',NULL,'978333111','direc prueba','88882227','king@gmail.com','distrito A','provincia A','departamento A','5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8',NULL,1,1643729836,NULL,NULL,1,0,'url foto','2022-02-01 15:37:16','2022-02-01 15:37:16',1),(12,'NAAP81','NAME','APE',NULL,'978333112','direc prueba','81882227','name@gmail.com','distrito A','provincia A','departamento A','5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8',NULL,1,1643730663,NULL,NULL,1,0,'url foto','2022-02-01 15:51:03','2022-02-01 15:51:03',1),(13,'SAFX11','pixx','hhh',NULL,'978333100','DIREC PRUEBA','11112222','SAM@GMAIL.COM','DISTRITO A','PROVINCIA A','DEPARTAMENTO A','0554a5df02ee12f1ae36a51caaef34a31deb9458a48b629da554a2b322466f4a',NULL,1,1643735369,1,1643744606,1,0,'url foto 2','2022-02-01 17:09:29','2022-02-01 19:43:26',1),(15,'GXTT11','PP','TT',NULL,'888844443','DIREC PRUEBA','11114444','PP@GMAIL.COM','DISTRITO A','PROVINCIA A','DEPARTAMENTO A','35f58ff0636e21ce307aba765f4d755f7a41a3bedd0d99d00c1c5bba8e1b50db',NULL,1,1643745676,1,1643746705,0,1,'url foto','2022-02-01 20:01:16','2022-02-01 20:33:50',1),(16,'MATH55','MAX','THPP',NULL,'888844445','DIREC PRUEBA','55551111','MAX@GMAIL.COM','DISTRITO A','PROVINCIA A','DEPARTAMENTO A','446ff62d0be392ac59f5660c71a6fe24d610ae128cd59d7b522f570a12cc7425',NULL,1,1643749371,NULL,NULL,1,0,'url foto','2022-02-01 21:02:51','2022-02-01 21:02:51',2);
/*!40000 ALTER TABLE `tbl_empleados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_empleados_local`
--

DROP TABLE IF EXISTS `tbl_empleados_local`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_empleados_local` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `int_empleado` int(11) NOT NULL,
  `int_local` int(11) NOT NULL,
  `int_rolid` int(11) NOT NULL,
  `chr_codempleado` char(6) CHARACTER SET utf8mb4 NOT NULL,
  `chr_codlocal` char(6) CHARACTER SET utf8mb4 NOT NULL,
  `int_usercreated` int(11) DEFAULT NULL,
  `int_usermodified` int(11) DEFAULT NULL,
  `int_datecreated` int(11) DEFAULT NULL,
  `int_datemodified` int(11) DEFAULT NULL,
  `int_localdefault` char(1) CHARACTER SET utf8mb4 DEFAULT '0',
  `is_active` int(11) DEFAULT 1,
  `is_deleted` int(11) DEFAULT 0,
  `created_at` varchar(45) DEFAULT NULL,
  `updated_at` varchar(45) DEFAULT NULL,
  `is_assigned` char(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `XIF1EMPLEADOS_LOCAL` (`chr_codlocal`),
  KEY `XIF2EMPLEADOS_LOCAL` (`chr_codempleado`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_empleados_local`
--

LOCK TABLES `tbl_empleados_local` WRITE;
/*!40000 ALTER TABLE `tbl_empleados_local` DISABLE KEYS */;
INSERT INTO `tbl_empleados_local` VALUES (9,1,3,1,'LUTE47','SJM3',1,NULL,1613893397,NULL,'0',1,0,'2021-02-21 07:43:17','2021-02-21 07:43:17','1'),(13,3,4,1,'MABR22','SJM4',1,NULL,1613893408,NULL,'0',1,0,'2021-02-21 07:43:28','2021-02-21 07:43:28','1'),(15,2,3,1,'FEAM88','SJM3',1,NULL,1613893434,NULL,'0',1,0,'2021-02-21 07:43:54','2021-02-21 07:43:54','1'),(17,4,3,1,'ROPE87','SJM3',1,NULL,1613944593,NULL,'0',1,0,'2021-02-21 21:56:33','2021-02-21 21:56:33','1'),(18,4,2,1,'ROPE87','SJM2',1,NULL,1613944595,NULL,'0',1,0,'2021-02-21 21:56:35','2021-02-21 21:56:35','1'),(19,5,1,1,'JORO48','SJM01',1,NULL,1613946876,NULL,'0',1,0,'2021-02-21 22:34:36','2021-02-21 22:34:36','1'),(20,1,1,1,'LUTE47','SJM01',1,NULL,1615089816,NULL,'0',1,0,'2021-03-07 04:03:36','2021-03-07 04:03:36','1'),(21,8,1,1,'SAKSA','SJM01',1,NULL,1643670143,NULL,'0',1,0,'2022-01-31 23:02:23','2022-01-31 23:02:23','1'),(22,9,1,1,'YURIA','SJM01',1,NULL,1643726781,NULL,'0',1,0,'2022-02-01 14:46:21','2022-02-01 14:46:21','1'),(23,10,1,1,'ZEREF','SJM01',1,NULL,1643729272,NULL,'0',1,0,'2022-02-01 15:27:52','2022-02-01 15:27:52','1'),(24,11,1,1,'KING','SJM01',1,NULL,1643729836,NULL,'0',1,0,'2022-02-01 15:37:16','2022-02-01 15:37:16','1'),(25,12,1,1,'NAAP81','SJM01',1,NULL,1643730663,NULL,'0',1,0,'2022-02-01 15:51:03','2022-02-01 15:51:03','1'),(26,13,1,1,'SAFX11','SJM01',1,1,1643735369,1643744606,'0',1,0,'2022-02-01 17:09:29','2022-02-01 19:43:26','1'),(27,15,2,1,'GXTT11','SJM2',1,1,1643745676,1643746705,'0',0,1,'2022-02-01 20:01:16','2022-02-01 20:33:50','1'),(28,16,2,2,'MATH55','SJM2',1,NULL,1643749371,NULL,'0',1,0,'2022-02-01 21:02:51','2022-02-01 21:02:51','1');
/*!40000 ALTER TABLE `tbl_empleados_local` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_estado`
--

DROP TABLE IF EXISTS `tbl_estado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_estado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `int_facturaid` int(11) NOT NULL,
  `int_pedidoid` int(11) NOT NULL,
  `chr_descripcion` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL,
  `int_usercreated` int(11) DEFAULT NULL,
  `int_usermodified` int(11) DEFAULT NULL,
  `int_datecreated` int(11) DEFAULT NULL,
  `int_datemodified` int(11) DEFAULT NULL,
  `is_active` int(11) DEFAULT 1,
  `is_deleted` int(11) DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `XIF1ESTADO` (`int_facturaid`,`int_pedidoid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_estado`
--

LOCK TABLES `tbl_estado` WRITE;
/*!40000 ALTER TABLE `tbl_estado` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_estado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_facturas`
--

DROP TABLE IF EXISTS `tbl_facturas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_facturas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chr_codfactura` varchar(20) CHARACTER SET utf8mb4 NOT NULL,
  `chr_serie` varchar(4) CHARACTER SET utf8mb4 NOT NULL,
  `chr_numero` varchar(8) CHARACTER SET utf8mb4 NOT NULL,
  `chr_codempleado` char(6) CHARACTER SET utf8mb4 NOT NULL,
  `int_clienteid` int(11) NOT NULL,
  `double_igv` double(5,2) NOT NULL,
  `double_subtotal` double(5,2) NOT NULL,
  `double_montoigv` double(5,2) NOT NULL,
  `double_total` double(5,2) NOT NULL,
  `double_tipocambio` double(4,2) DEFAULT NULL,
  `int_formapagoid` int(11) NOT NULL,
  `int_usercreated` int(11) DEFAULT NULL,
  `int_usermodified` int(11) DEFAULT NULL,
  `int_datecreated` int(11) DEFAULT NULL,
  `int_datemodified` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `XIF1FACTURAS` (`chr_codempleado`),
  KEY `XIF2FACTURAS` (`int_clienteid`),
  KEY `XIF3FACTURAS` (`int_formapagoid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_facturas`
--

LOCK TABLES `tbl_facturas` WRITE;
/*!40000 ALTER TABLE `tbl_facturas` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_facturas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_forma_pago`
--

DROP TABLE IF EXISTS `tbl_forma_pago`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_forma_pago` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chr_nombre` varchar(40) CHARACTER SET utf8mb4 DEFAULT NULL,
  `chr_descripcion` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL,
  `int_usercreated` int(11) DEFAULT NULL,
  `int_usermodified` int(11) DEFAULT NULL,
  `int_datecreated` int(11) DEFAULT NULL,
  `int_datemodified` int(11) DEFAULT NULL,
  `is_active` int(11) DEFAULT 1,
  `is_deleted` int(11) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_forma_pago`
--

LOCK TABLES `tbl_forma_pago` WRITE;
/*!40000 ALTER TABLE `tbl_forma_pago` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_forma_pago` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_ingrediente`
--

DROP TABLE IF EXISTS `tbl_ingrediente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_ingrediente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chr_codingrediente` varchar(6) CHARACTER SET utf8mb4 NOT NULL,
  `chr_nombre` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  `chr_marca` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL,
  `date_vencimiento` date DEFAULT NULL,
  `int_proveedorid` int(11) DEFAULT NULL,
  `chr_unidadmedida` varchar(6) CHARACTER SET utf8mb4 DEFAULT NULL,
  `int_usercreated` int(11) DEFAULT NULL,
  `int_usermodified` int(11) DEFAULT NULL,
  `int_datecreated` int(11) DEFAULT NULL,
  `int_datemodified` int(11) DEFAULT NULL,
  `created_at` varchar(45) DEFAULT NULL,
  `updated_at` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `XIF1INGREDIENTE` (`int_proveedorid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_ingrediente`
--

LOCK TABLES `tbl_ingrediente` WRITE;
/*!40000 ALTER TABLE `tbl_ingrediente` DISABLE KEYS */;
INSERT INTO `tbl_ingrediente` VALUES (1,'VER01','lechuga','generico',NULL,NULL,'UNI2',1,NULL,NULL,NULL,NULL,NULL),(2,'CAR01','POLLO','SAN FERNANDO',NULL,NULL,'UNI',1,NULL,1614838788,NULL,'2021-03-04 06:19:48','2021-03-04 06:19:48'),(3,'CRE01','MAYONESA','ALACENA',NULL,NULL,'GR',1,NULL,1614838873,NULL,'2021-03-04 06:21:13','2021-03-04 06:21:13'),(4,'CRE02','MOSTAZA','ALACENA',NULL,NULL,'GR2',1,NULL,1614838928,NULL,'2021-03-04T06:22:08.000000Z','2021-03-04T06:22:08.000000Z');
/*!40000 ALTER TABLE `tbl_ingrediente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_ingrediente_local`
--

DROP TABLE IF EXISTS `tbl_ingrediente_local`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_ingrediente_local` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chr_codlocal` char(2) CHARACTER SET utf8mb4 DEFAULT NULL,
  `int_local` int(11) NOT NULL,
  `int_ingredienteid` int(11) NOT NULL,
  `int_usercreated` int(11) DEFAULT NULL,
  `int_usermodified` int(11) DEFAULT NULL,
  `int_datecreated` int(11) DEFAULT NULL,
  `int_datemodified` int(11) DEFAULT NULL,
  `int_proveedorid` int(11) DEFAULT NULL,
  `created_at` varchar(45) DEFAULT NULL,
  `updated_at` varchar(45) DEFAULT NULL,
  `is_assigned` char(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_ingrediente_local`
--

LOCK TABLES `tbl_ingrediente_local` WRITE;
/*!40000 ALTER TABLE `tbl_ingrediente_local` DISABLE KEYS */;
INSERT INTO `tbl_ingrediente_local` VALUES (1,NULL,2,2,1,NULL,1615088574,NULL,NULL,'2021-03-07 03:42:54','2021-03-07 03:42:54','1'),(2,NULL,1,1,1,NULL,1615483771,NULL,NULL,'2021-03-11 17:29:31','2021-03-11 17:29:31','1');
/*!40000 ALTER TABLE `tbl_ingrediente_local` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_ingrediente_ocompra`
--

DROP TABLE IF EXISTS `tbl_ingrediente_ocompra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_ingrediente_ocompra` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `int_ordencompraid` int(11) NOT NULL,
  `chr_codlocal` char(2) CHARACTER SET utf8mb4 NOT NULL,
  `int_ingredienteid` int(11) NOT NULL,
  `int_cantidad` int(11) DEFAULT NULL,
  `chr_unidadmedida` varchar(6) CHARACTER SET utf8mb4 DEFAULT NULL,
  `txt_observaciones` text CHARACTER SET utf8mb4 DEFAULT NULL,
  `int_usercreated` int(11) DEFAULT NULL,
  `int_usermodified` int(11) DEFAULT NULL,
  `int_datecreated` int(11) DEFAULT NULL,
  `int_datemodified` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_ingrediente_ocompra`
--

LOCK TABLES `tbl_ingrediente_ocompra` WRITE;
/*!40000 ALTER TABLE `tbl_ingrediente_ocompra` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_ingrediente_ocompra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_ingrediente_pedido`
--

DROP TABLE IF EXISTS `tbl_ingrediente_pedido`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_ingrediente_pedido` (
  `ID_PEDIDO` int(11) NOT NULL,
  `COD_LOCAL` char(2) NOT NULL,
  `ID_INGREDIENTE` int(11) NOT NULL,
  `CANTIDAD` int(11) DEFAULT NULL,
  `OBSERVACIONES` varchar(20) DEFAULT NULL,
  `USR_MODIFICACION` varchar(20) DEFAULT NULL,
  `FEC_MODIFICACION` date DEFAULT NULL,
  `USR_CREACION` varchar(20) DEFAULT NULL,
  `FEC_CREACION` date DEFAULT NULL,
  PRIMARY KEY (`ID_INGREDIENTE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_ingrediente_pedido`
--

LOCK TABLES `tbl_ingrediente_pedido` WRITE;
/*!40000 ALTER TABLE `tbl_ingrediente_pedido` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_ingrediente_pedido` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_ingrediente_producto`
--

DROP TABLE IF EXISTS `tbl_ingrediente_producto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_ingrediente_producto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chr_codproducto` char(6) CHARACTER SET utf8mb4 NOT NULL,
  `int_cantidad` int(11) DEFAULT NULL,
  `int_idproducto` int(11) NOT NULL,
  `int_ingredienteid` int(11) NOT NULL,
  `int_usermodified` int(11) DEFAULT NULL,
  `int_datemodified` int(11) DEFAULT NULL,
  `int_usercreated` int(11) DEFAULT NULL,
  `int_datecreated` int(11) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT 1,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `int_proveedorid` int(11) DEFAULT NULL,
  `created_at` varchar(45) DEFAULT NULL,
  `updated_at` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `XIF1ingrediente_producto` (`chr_codproducto`),
  KEY `XIF2ingrediente_producto` (`int_ingredienteid`,`int_proveedorid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_ingrediente_producto`
--

LOCK TABLES `tbl_ingrediente_producto` WRITE;
/*!40000 ALTER TABLE `tbl_ingrediente_producto` DISABLE KEYS */;
INSERT INTO `tbl_ingrediente_producto` VALUES (1,'P01',NULL,1,3,NULL,NULL,1,1629665306,1,0,NULL,'2021-08-22 20:48:26','2021-08-22 20:48:26'),(2,'P01',NULL,1,2,NULL,NULL,1,1629665306,1,0,NULL,'2021-08-22 20:48:26','2021-08-22 20:48:26');
/*!40000 ALTER TABLE `tbl_ingrediente_producto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_inventario`
--

DROP TABLE IF EXISTS `tbl_inventario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_inventario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `int_almacen` int(11) NOT NULL,
  `int_localid` int(11) NOT NULL,
  `int_statusid` int(11) NOT NULL,
  `chr_nombre` varchar(150) DEFAULT NULL,
  `txt_detalle` text DEFAULT NULL,
  `int_year` int(11) DEFAULT 2021,
  `is_active` int(11) DEFAULT 1,
  `is_deleted` int(11) DEFAULT 0,
  `date_timecreated` int(11) DEFAULT NULL,
  `date_timemodified` int(11) DEFAULT NULL,
  `created_at` varchar(45) DEFAULT NULL,
  `updated_at` varchar(45) DEFAULT NULL,
  `int_creatorid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_inventario`
--

LOCK TABLES `tbl_inventario` WRITE;
/*!40000 ALTER TABLE `tbl_inventario` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_inventario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_inventario_detalle`
--

DROP TABLE IF EXISTS `tbl_inventario_detalle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_inventario_detalle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `int_inventarioid` int(11) NOT NULL,
  `int_ingredienteid` int(11) NOT NULL,
  `int_movientoid` int(11) NOT NULL,
  `int_movimientodetalleid` int(11) DEFAULT NULL,
  `int_unidadmedidaid` int(11) DEFAULT 1,
  `double_cantidad_salida` double(5,2) DEFAULT 0.00,
  `double_cantidad_entrada` double(5,2) DEFAULT 0.00,
  `int_datetimecreated` int(11) DEFAULT NULL,
  `int_datetimemodified` int(11) DEFAULT NULL,
  `int_creatorid` int(11) DEFAULT NULL,
  `created_at` varchar(45) DEFAULT NULL,
  `update_at` varchar(45) DEFAULT NULL,
  `is_active` int(11) DEFAULT 1,
  `is_deleted` int(11) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_inventario_detalle`
--

LOCK TABLES `tbl_inventario_detalle` WRITE;
/*!40000 ALTER TABLE `tbl_inventario_detalle` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_inventario_detalle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_locales`
--

DROP TABLE IF EXISTS `tbl_locales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_locales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chr_codlocal` char(6) CHARACTER SET utf8mb4 NOT NULL,
  `chr_nombre` varchar(40) CHARACTER SET utf8mb4 DEFAULT NULL,
  `chr_direccion` varchar(80) CHARACTER SET utf8mb4 DEFAULT NULL,
  `chr_telefono` varchar(45) DEFAULT NULL,
  `int_latitud` int(11) DEFAULT NULL,
  `int_longitud` int(11) DEFAULT NULL,
  `int_usercreated` int(11) DEFAULT NULL,
  `int_usermodified` int(11) DEFAULT NULL,
  `int_datecreated` int(11) DEFAULT NULL,
  `int_datemodified` int(11) DEFAULT NULL,
  `updated_at` varchar(45) DEFAULT NULL,
  `created_at` varchar(45) DEFAULT NULL,
  `is_active` int(11) DEFAULT 1,
  `is_deleted` int(11) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_locales`
--

LOCK TABLES `tbl_locales` WRITE;
/*!40000 ALTER TABLE `tbl_locales` DISABLE KEYS */;
INSERT INTO `tbl_locales` VALUES (1,'SJM01','LOCAL 01','Av.Canevaro','98265625',-12443,20287,NULL,NULL,NULL,NULL,NULL,NULL,1,0),(2,'SJM2','villa','av cesar calle','98268201',NULL,NULL,1,NULL,1613826977,NULL,'2021-02-20T13:16:17.000000Z','2021-02-20T13:16:17.000000Z',1,0),(3,'SJM3','rojas2','av. sarat','6892933',NULL,NULL,1,NULL,1613827029,NULL,'2021-02-20T13:17:09.000000Z','2021-02-20T13:17:09.000000Z',1,0),(4,'SJM4','brasas 2','av.ervolcuion 12','9888721',NULL,NULL,1,NULL,1613827936,NULL,'2021-02-20T13:32:16.000000Z','2021-02-20T13:32:16.000000Z',1,0);
/*!40000 ALTER TABLE `tbl_locales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_menu`
--

DROP TABLE IF EXISTS `tbl_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `int_padreid` int(11) DEFAULT 0,
  `chr_title` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `chr_nombre` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `chr_descripcion` text CHARACTER SET utf8mb4 NOT NULL,
  `chr_ruta_sistema` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `chr_icon` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `int_order` int(11) DEFAULT 0,
  `int_level` int(11) DEFAULT 0,
  `int_usercreated` int(11) DEFAULT NULL,
  `int_usermodified` int(11) DEFAULT NULL,
  `int_datecreated` int(11) DEFAULT NULL,
  `int_datemodified` int(11) DEFAULT NULL,
  `is_active` int(11) DEFAULT 1,
  `is_deleted` int(11) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_menu`
--

LOCK TABLES `tbl_menu` WRITE;
/*!40000 ALTER TABLE `tbl_menu` DISABLE KEYS */;
INSERT INTO `tbl_menu` VALUES (1,0,'Dashboard','Dashboard','Dashboard','/modulos/dashboard','fas fa-tachometer-alt',1,2,1,NULL,NULL,NULL,1,0),(2,0,'Principal','Principal','Principal','','fas fa-home',1,1,1,NULL,NULL,NULL,1,0),(3,0,'Artículo','Artículo','Artículo','','fas fa-align-justify',2,1,1,NULL,NULL,NULL,1,0),(4,2,'Cliente','Cliente','Cliente','/principal/cliente','fas fa-user',1,2,1,NULL,NULL,NULL,1,0),(5,2,'Proveedor','Proveedor','Proveedor','/principal/proveedor','fas fa-cart-plus',2,2,1,NULL,NULL,NULL,1,0),(6,2,'Empleado','Empleado','Empleado','/principal/empleado','fas fa-users',3,2,1,NULL,NULL,NULL,1,0),(7,2,'Local','Local','Local','/principal/local','fas fa-store-alt',4,2,1,NULL,NULL,NULL,1,0),(8,3,'Ingredientes','Ingredientes','Ingredientes','/articulo/materiaPrima','fas fa-apple-alt',1,2,1,NULL,NULL,NULL,1,0),(9,3,'Almacen','Almacen','Almacen','/articulo/almacen','fas fa-box',2,2,1,NULL,NULL,NULL,1,0),(10,3,'Producto','Producto','Producto','/articulo/producto','fas fa-star',3,2,1,NULL,NULL,NULL,1,0),(11,0,'Ventas','Ventas','Módulo de ventas','','',3,1,1,NULL,NULL,NULL,1,0),(12,0,'Compras','Compras','Módulo de Compras','','',4,1,1,NULL,NULL,NULL,1,0),(13,0,'Reportes','Reportes','Reportes','','',5,1,1,NULL,NULL,NULL,1,0),(14,11,'Mozo','Mozo','Vista del Mozo','/ventas/mozo','',1,2,1,NULL,NULL,NULL,1,0),(15,11,'Cajera','Cajera','Vista de la Cajera','/ventas/cajera','',2,2,1,NULL,NULL,NULL,1,0),(16,11,'Cocina','Cocina','Cocina','/ventas/cocina','',3,2,1,NULL,NULL,NULL,1,0),(17,11,'Boletas','Boletas','Boletas','/ventas/boletas','',4,2,1,NULL,NULL,NULL,1,0),(18,11,'Facturas','Facturas','Facturas','/ventas/facturas','',5,2,1,NULL,NULL,NULL,1,0),(19,11,'Comprobantes','Comprobastes','Comprobantes','/ventas/comprobantes','',6,2,1,NULL,NULL,NULL,1,0),(20,12,'Ordenes de Compra','Ordenes de Compra','Ordenes de Compra','/compras/ordencompra','',1,2,1,NULL,NULL,NULL,1,0),(21,12,'Comprobantes','Comprobantes','Comprobantes','/compras/comprobantes','',2,2,1,NULL,NULL,NULL,1,0),(22,12,'Boleta','Boleta','Boleta','/compras/boleta','',3,2,1,NULL,NULL,NULL,1,0),(23,12,'Factura','Factura','Factura','/compras/factura','',4,2,1,NULL,NULL,NULL,1,0),(24,13,'Reporte Pedidos','Reporte de Pedidos','Reporte de Pedidos','/reporte/pedidos','',1,2,1,NULL,NULL,NULL,1,0),(25,13,'Reporte de Local','Reporte de Local','Reporte de Local','/reporte/local','',2,2,1,NULL,NULL,NULL,1,0),(26,13,'Reporte de Gastos','Reporte de Gastos','Reporte de Gastos','/reporte/gastos','',3,2,1,NULL,NULL,NULL,1,0),(27,13,'Reporte de Almacen','Reporte de Almacen','Reporte de Almacen','/reporte/almacen','',4,2,1,NULL,NULL,NULL,1,0),(28,0,'Mantenimiento','Mantenimiento ','mantenimiento','','',6,1,1,NULL,NULL,NULL,1,0),(29,28,'Tipo pago','Tipo pago','Tipo pago','/mantenimiento/tipopago','',1,2,1,NULL,NULL,NULL,1,0),(30,28,'Tipo pedido','Tipo pedido','Tipo pedido','/mantenimiento/tipopedido','',2,2,1,NULL,NULL,NULL,1,0);
/*!40000 ALTER TABLE `tbl_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_mesa`
--

DROP TABLE IF EXISTS `tbl_mesa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_mesa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chr_codmesa` char(2) CHARACTER SET utf8mb4 NOT NULL,
  `chr_codlocal` char(2) CHARACTER SET utf8mb4 NOT NULL,
  `int_capacidad` int(11) DEFAULT NULL,
  `int_usercreated` int(11) DEFAULT NULL,
  `int_usermodified` int(11) DEFAULT NULL,
  `int_datecreated` int(11) DEFAULT NULL,
  `int_datemodified` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_mesa`
--

LOCK TABLES `tbl_mesa` WRITE;
/*!40000 ALTER TABLE `tbl_mesa` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_mesa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_mesa_reserva`
--

DROP TABLE IF EXISTS `tbl_mesa_reserva`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_mesa_reserva` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chr_codmesa` char(2) CHARACTER SET utf8mb4 NOT NULL,
  `int_reservaid` int(11) NOT NULL,
  `int_usercreated` int(11) DEFAULT NULL,
  `int_usermodified` int(11) DEFAULT NULL,
  `int_datecreated` int(11) DEFAULT NULL,
  `int_datemodified` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_mesa_reserva`
--

LOCK TABLES `tbl_mesa_reserva` WRITE;
/*!40000 ALTER TABLE `tbl_mesa_reserva` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_mesa_reserva` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_moviemiento`
--

DROP TABLE IF EXISTS `tbl_moviemiento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_moviemiento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `int_tipomoviemientoid` int(11) NOT NULL,
  `int_documentoid` int(11) NOT NULL,
  `int_origen` int(11) NOT NULL,
  `int_tipomovimiento` tinyint(1) DEFAULT 1,
  `int_datetimecreated` int(11) DEFAULT NULL,
  `int_datetimemodified` int(11) DEFAULT NULL,
  `int_creatorid` int(11) DEFAULT NULL,
  `is_active` int(11) DEFAULT 1,
  `is_deleted` int(11) DEFAULT 0,
  `created_at` varchar(45) DEFAULT NULL,
  `update_at` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_moviemiento`
--

LOCK TABLES `tbl_moviemiento` WRITE;
/*!40000 ALTER TABLE `tbl_moviemiento` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_moviemiento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_movimiento_detalle`
--

DROP TABLE IF EXISTS `tbl_movimiento_detalle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_movimiento_detalle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `int_movimientoid` int(11) NOT NULL,
  `int_ingredienteid` int(11) NOT NULL,
  `double_cantidad` double(5,2) DEFAULT 0.00,
  `double_costo_sinigv` double(5,2) DEFAULT 0.00,
  `double_costo_conigv` double(5,2) DEFAULT 0.00,
  `double_igv100` double(5,2) DEFAULT 18.00,
  `int_creatorid` int(11) DEFAULT NULL,
  `int_datetimecreated` int(11) DEFAULT NULL,
  `int_datetimemodified` int(11) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT 1,
  `is_deleted` tinyint(1) DEFAULT 0,
  `create_at` varchar(45) DEFAULT NULL,
  `deleted_at` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_movimiento_detalle`
--

LOCK TABLES `tbl_movimiento_detalle` WRITE;
/*!40000 ALTER TABLE `tbl_movimiento_detalle` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_movimiento_detalle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_mst_estado`
--

DROP TABLE IF EXISTS `tbl_mst_estado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_mst_estado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chr_name` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL,
  `chr_descripcion` text CHARACTER SET utf8mb4 DEFAULT NULL,
  `chr_colorstyle` varchar(50) DEFAULT NULL,
  `int_usercreated` int(11) DEFAULT NULL,
  `int_usermodified` int(11) DEFAULT NULL,
  `int_datecreated` int(11) DEFAULT NULL,
  `int_datemodified` int(11) DEFAULT NULL,
  `is_active` int(11) DEFAULT 1,
  `is_deleted` int(11) DEFAULT 0,
  `int_grupoid` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_mst_estado`
--

LOCK TABLES `tbl_mst_estado` WRITE;
/*!40000 ALTER TABLE `tbl_mst_estado` DISABLE KEYS */;
INSERT INTO `tbl_mst_estado` VALUES (1,'PEDIDO_BORRADOR','PEDIDO_BORRADOR',NULL,1,NULL,NULL,NULL,1,0,'PEDIDO'),(2,'PEDIDO_REGISTRADO','PEDIDO_REGISTRADO',NULL,1,NULL,NULL,NULL,1,0,'PEDIDO'),(3,'PEDIDO_COCINA_RECIBIDO','PEDIDO_COCINA_RECIBIDO',NULL,1,NULL,NULL,NULL,1,0,'COCINA'),(4,'PEDIDO_COCINA_PROCESO','PEDIDO_COCINA_PROCESO',NULL,1,NULL,NULL,NULL,1,0,'COCINA'),(5,'PEDIDO_COCINA_DESPACHADO','PEDIDO_COCINA_DESPACHADO',NULL,1,NULL,NULL,NULL,1,0,'COCINA'),(6,'PEDIDO_FACTURACION','PEDIDO_FACTURACION',NULL,1,NULL,NULL,NULL,1,0,'PEDIDO'),(7,'PEDIDO_DELIVERY','PEDIDO_DELIVERY',NULL,1,NULL,NULL,NULL,1,0,'PEDIDO'),(8,'PEDIDO_FINALIZADO','PEDIDO_FINALIZADO',NULL,1,NULL,NULL,NULL,1,0,'PEDIDO');
/*!40000 ALTER TABLE `tbl_mst_estado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_orden_compra`
--

DROP TABLE IF EXISTS `tbl_orden_compra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_orden_compra` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chr_serie` varchar(4) CHARACTER SET utf8mb4 NOT NULL,
  `chr_numero` varchar(8) CHARACTER SET utf8mb4 NOT NULL,
  `date_fecha` date DEFAULT NULL,
  `chr_codlocal` char(2) CHARACTER SET utf8mb4 NOT NULL,
  `int_usercreated` int(11) DEFAULT NULL,
  `int_usermodified` int(11) DEFAULT NULL,
  `int_datecreated` int(11) DEFAULT NULL,
  `int_datemodified` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_orden_compra`
--

LOCK TABLES `tbl_orden_compra` WRITE;
/*!40000 ALTER TABLE `tbl_orden_compra` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_orden_compra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_orden_factura`
--

DROP TABLE IF EXISTS `tbl_orden_factura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_orden_factura` (
  `COD_FACTURA` varchar(20) NOT NULL,
  `COD_ORDEN_COMPRA` varchar(20) NOT NULL,
  `ID_COMBO` int(11) NOT NULL,
  `COD_PRODUCTO` char(6) NOT NULL,
  `USR_MODIFICACION` varchar(20) DEFAULT NULL,
  `FEC_MODIFICACION` date DEFAULT NULL,
  `USR_CREACION` varchar(20) DEFAULT NULL,
  `FEC_CREACION` date DEFAULT NULL,
  `COD_MESA` int(11) NOT NULL,
  PRIMARY KEY (`COD_FACTURA`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_orden_factura`
--

LOCK TABLES `tbl_orden_factura` WRITE;
/*!40000 ALTER TABLE `tbl_orden_factura` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_orden_factura` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_pedido`
--

DROP TABLE IF EXISTS `tbl_pedido`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_pedido` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chr_numdocumento` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL,
  `chr_serie` varchar(4) CHARACTER SET utf8mb4 DEFAULT NULL,
  `chr_numero` varchar(8) CHARACTER SET utf8mb4 DEFAULT NULL,
  `int_empleadoid` int(11) DEFAULT NULL,
  `int_clienteid` int(11) DEFAULT NULL,
  `date_fecha` date DEFAULT NULL,
  `int_tipopagoid` int(11) DEFAULT 1,
  `int_monedaid` int(11) DEFAULT 1,
  `double_tipocambio` double(5,2) DEFAULT 0.00,
  `double_subTotal` double(5,2) DEFAULT 0.00,
  `double_igv100` double(5,2) DEFAULT 0.00,
  `double_igvTotal` double(5,2) DEFAULT 0.00,
  `double_descuento` double(5,2) DEFAULT 0.00,
  `double_Total` double(5,2) DEFAULT 0.00,
  `txt_direccion` text DEFAULT NULL,
  `txt_motorizado` text DEFAULT NULL,
  `int_usercreated` int(11) DEFAULT NULL,
  `int_usermodified` int(11) DEFAULT NULL,
  `int_datecreated` int(11) DEFAULT NULL,
  `int_datemodified` int(11) DEFAULT NULL,
  `int_estadoid` int(11) DEFAULT NULL,
  `is_active` int(11) DEFAULT 1,
  `is_deleted` int(11) DEFAULT 0,
  `txt_observaciones` text CHARACTER SET utf8mb4 DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `XIF1PEDIDO` (`chr_numdocumento`)
) ENGINE=InnoDB AUTO_INCREMENT=229 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_pedido`
--

LOCK TABLES `tbl_pedido` WRITE;
/*!40000 ALTER TABLE `tbl_pedido` DISABLE KEYS */;
INSERT INTO `tbl_pedido` VALUES (1,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1616945302,NULL,1,1,0,NULL),(2,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1616947628,NULL,1,1,0,NULL),(3,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1616948000,NULL,1,1,0,NULL),(4,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1616948030,NULL,1,1,0,NULL),(5,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1616948145,NULL,1,1,0,NULL),(6,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1616948186,NULL,1,1,0,NULL),(7,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1616948217,NULL,1,1,0,NULL),(8,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1616948288,NULL,1,1,0,NULL),(9,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1616948387,NULL,1,1,0,NULL),(10,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1616949067,NULL,1,1,0,NULL),(11,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1616949315,NULL,1,1,0,NULL),(12,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1616949341,NULL,1,1,0,NULL),(13,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1616949628,NULL,1,1,0,NULL),(14,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1616950640,NULL,1,1,0,NULL),(15,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1616954618,NULL,1,1,0,NULL),(16,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1616954779,NULL,1,1,0,NULL),(17,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1616954836,NULL,1,1,0,NULL),(18,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1616954841,NULL,1,1,0,NULL),(19,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1616954878,NULL,1,1,0,NULL),(20,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1616954881,NULL,1,1,0,NULL),(21,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1616954980,NULL,1,1,0,NULL),(22,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1616955025,NULL,1,1,0,NULL),(23,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1616955030,NULL,1,1,0,NULL),(24,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1616955053,NULL,1,1,0,NULL),(25,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1616955331,NULL,1,1,0,NULL),(26,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1616955345,NULL,1,1,0,NULL),(27,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1616955795,NULL,1,1,0,NULL),(28,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1616955899,NULL,1,1,0,NULL),(29,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1616956428,NULL,1,1,0,NULL),(30,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1616956449,NULL,1,1,0,NULL),(31,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1616956633,NULL,1,1,0,NULL),(32,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1616956742,NULL,1,1,0,NULL),(33,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1616956791,NULL,1,1,0,NULL),(34,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1616956891,NULL,1,1,0,NULL),(35,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1616956981,NULL,1,1,0,NULL),(36,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1616957056,NULL,1,1,0,NULL),(37,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1616957112,NULL,1,1,0,NULL),(38,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1616957190,NULL,1,1,0,NULL),(39,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1616957212,NULL,1,1,0,NULL),(40,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1616957458,NULL,1,1,0,NULL),(41,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1616957495,NULL,1,1,0,NULL),(42,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1616958571,NULL,1,1,0,NULL),(43,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1616958748,NULL,1,1,0,NULL),(44,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1616958861,NULL,1,1,0,NULL),(45,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1616958864,NULL,1,1,0,NULL),(46,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1616960885,NULL,1,1,0,NULL),(47,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1616960929,NULL,1,1,0,NULL),(48,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1616961301,NULL,1,1,0,NULL),(49,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1616961388,NULL,1,1,0,NULL),(50,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1617157110,NULL,1,1,0,NULL),(51,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1617157443,NULL,1,1,0,NULL),(52,NULL,NULL,NULL,1,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1619144671,NULL,4,1,0,NULL),(53,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1619145674,NULL,1,1,0,NULL),(54,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1619145686,NULL,1,1,0,NULL),(55,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1619146378,NULL,1,1,0,NULL),(56,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1619146553,NULL,1,1,0,NULL),(57,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1619146572,NULL,1,1,0,NULL),(58,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1619146813,NULL,1,1,0,NULL),(59,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1619146823,NULL,1,1,0,NULL),(60,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1619146829,NULL,1,1,0,NULL),(61,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1619146980,NULL,1,1,0,NULL),(62,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1619148872,NULL,1,1,0,NULL),(63,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1619148894,NULL,1,1,0,NULL),(64,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1619149018,NULL,1,1,0,NULL),(65,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1619149037,NULL,1,1,0,NULL),(66,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1619149078,NULL,1,1,0,NULL),(67,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1619149142,NULL,1,1,0,NULL),(68,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1619149295,NULL,1,1,0,NULL),(69,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1619149312,NULL,1,1,0,NULL),(70,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1619149393,NULL,1,1,0,NULL),(71,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1619149402,NULL,1,1,0,NULL),(72,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1619149492,NULL,1,1,0,NULL),(73,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1619149842,NULL,1,1,0,NULL),(74,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1619149933,NULL,1,1,0,NULL),(75,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1619150024,NULL,1,1,0,NULL),(76,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1619150246,NULL,1,1,0,NULL),(77,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1619150256,NULL,1,1,0,NULL),(78,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1619150406,NULL,1,1,0,NULL),(79,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1619150413,NULL,1,1,0,NULL),(80,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1619150651,NULL,1,1,0,NULL),(81,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1619150772,NULL,1,1,0,NULL),(82,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1619150925,NULL,1,1,0,NULL),(83,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1619150935,NULL,1,1,0,NULL),(84,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1619151271,NULL,1,1,0,NULL),(85,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1619151478,NULL,1,1,0,NULL),(86,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1619151512,NULL,1,1,0,NULL),(87,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1619151872,NULL,1,1,0,NULL),(88,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1619151936,NULL,1,1,0,NULL),(89,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1619151991,NULL,1,1,0,NULL),(90,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1619152016,NULL,1,1,0,NULL),(91,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1619152094,NULL,1,1,0,NULL),(92,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1619152353,NULL,1,1,0,NULL),(93,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1619152361,NULL,1,1,0,NULL),(94,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1619152461,NULL,1,1,0,NULL),(95,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1619152493,NULL,1,1,0,NULL),(96,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1619152498,NULL,1,1,0,NULL),(97,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1619152528,NULL,1,1,0,NULL),(98,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1619152805,NULL,1,1,0,NULL),(99,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1619152887,NULL,1,1,0,NULL),(100,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1619153072,NULL,1,1,0,NULL),(101,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1629771469,NULL,1,1,0,NULL),(102,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1629772708,NULL,1,1,0,NULL),(103,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1629772753,NULL,1,1,0,NULL),(104,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1629772769,NULL,1,1,0,NULL),(105,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1629773226,NULL,1,1,0,NULL),(106,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1629773285,NULL,1,1,0,NULL),(107,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1629773391,NULL,1,1,0,NULL),(108,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1629773427,NULL,1,1,0,NULL),(109,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1629773448,NULL,1,1,0,NULL),(110,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1629773787,NULL,1,1,0,NULL),(111,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1629774122,NULL,1,1,0,NULL),(112,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1629774146,NULL,1,1,0,NULL),(113,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1629774194,NULL,1,1,0,NULL),(114,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1629774567,NULL,1,1,0,NULL),(115,NULL,NULL,NULL,1,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1629774784,NULL,4,1,0,NULL),(116,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1629775934,NULL,1,1,0,NULL),(117,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1629775948,NULL,1,1,0,NULL),(118,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1629775967,NULL,1,1,0,NULL),(119,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1629775973,NULL,1,1,0,NULL),(120,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1629775979,NULL,1,1,0,NULL),(121,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1629776804,NULL,1,1,0,NULL),(122,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1629777067,NULL,1,1,0,NULL),(123,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1629777159,NULL,1,1,0,NULL),(124,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1629777165,NULL,1,1,0,NULL),(125,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1629777235,NULL,1,1,0,NULL),(126,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1629777278,NULL,1,1,0,NULL),(127,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1629777283,NULL,1,1,0,NULL),(128,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1629777496,NULL,1,1,0,NULL),(129,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1629777573,NULL,1,1,0,NULL),(130,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1629778074,NULL,1,1,0,NULL),(131,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1629778102,NULL,1,1,0,NULL),(132,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1629778159,NULL,1,1,0,NULL),(133,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630180985,NULL,1,1,0,NULL),(134,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630181113,NULL,1,1,0,NULL),(135,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630181612,NULL,1,1,0,NULL),(136,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630182210,NULL,1,1,0,NULL),(137,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630182925,NULL,1,1,0,NULL),(138,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630183045,NULL,1,1,0,NULL),(139,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630183141,NULL,1,1,0,NULL),(140,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630183169,NULL,1,1,0,NULL),(141,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630183283,NULL,1,1,0,NULL),(142,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630183426,NULL,1,1,0,NULL),(143,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630183824,NULL,1,1,0,NULL),(144,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630183840,NULL,1,1,0,NULL),(145,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630183854,NULL,1,1,0,NULL),(146,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630184915,NULL,1,1,0,NULL),(147,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630772809,NULL,1,1,0,NULL),(148,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630773901,NULL,1,1,0,NULL),(149,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630780946,NULL,1,1,0,NULL),(150,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630781445,NULL,1,1,0,NULL),(151,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630781447,NULL,1,1,0,NULL),(152,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630782564,NULL,1,1,0,NULL),(153,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630782579,NULL,1,1,0,NULL),(154,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630782592,NULL,1,1,0,NULL),(155,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630782629,NULL,1,1,0,NULL),(156,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630782653,NULL,1,1,0,NULL),(157,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630782789,NULL,1,1,0,NULL),(158,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630782869,NULL,1,1,0,NULL),(159,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630782951,NULL,1,1,0,NULL),(160,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630782965,NULL,1,1,0,NULL),(161,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630782978,NULL,1,1,0,NULL),(162,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630783133,NULL,1,1,0,NULL),(163,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630783221,NULL,1,1,0,NULL),(164,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630783256,NULL,1,1,0,NULL),(165,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630783369,NULL,1,1,0,NULL),(166,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630783751,NULL,1,1,0,NULL),(167,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630784910,NULL,1,1,0,NULL),(168,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630785092,NULL,1,1,0,NULL),(169,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630785121,NULL,1,1,0,NULL),(170,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630785128,NULL,1,1,0,NULL),(171,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630785154,NULL,1,1,0,NULL),(172,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630785240,NULL,1,1,0,NULL),(173,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630785284,NULL,1,1,0,NULL),(174,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630785364,NULL,1,1,0,NULL),(175,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630785407,NULL,1,1,0,NULL),(176,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630785426,NULL,1,1,0,NULL),(177,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630785453,NULL,1,1,0,NULL),(178,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630785472,NULL,1,1,0,NULL),(179,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630785666,NULL,1,1,0,NULL),(180,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630785920,NULL,1,1,0,NULL),(181,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630786763,NULL,1,1,0,NULL),(182,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630786958,NULL,1,1,0,NULL),(183,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630787003,NULL,1,1,0,NULL),(184,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630787024,NULL,1,1,0,NULL),(185,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630787077,NULL,1,1,0,NULL),(186,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630787192,NULL,1,1,0,NULL),(187,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630787321,NULL,1,1,0,NULL),(188,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630787522,NULL,1,1,0,NULL),(189,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630787662,NULL,1,1,0,NULL),(190,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630787930,NULL,1,1,0,NULL),(191,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630787989,NULL,1,1,0,NULL),(192,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630884151,NULL,1,1,0,NULL),(193,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630884319,NULL,1,1,0,NULL),(194,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630884365,NULL,1,1,0,NULL),(195,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630884381,NULL,1,1,0,NULL),(196,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630884676,NULL,1,1,0,NULL),(197,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630884716,NULL,1,1,0,NULL),(198,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630884780,NULL,1,1,0,NULL),(199,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630884824,NULL,1,1,0,NULL),(200,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630885378,NULL,1,1,0,NULL),(201,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630885677,NULL,1,1,0,NULL),(202,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630886112,NULL,1,1,0,NULL),(203,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630886140,NULL,1,1,0,NULL),(204,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630886169,NULL,1,1,0,NULL),(205,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630886301,NULL,1,1,0,NULL),(206,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630887156,NULL,1,1,0,NULL),(207,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630887212,NULL,1,1,0,NULL),(208,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630887479,NULL,1,1,0,NULL),(209,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630887538,NULL,1,1,0,NULL),(210,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630887674,NULL,1,1,0,NULL),(211,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630887690,NULL,1,1,0,NULL),(212,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630887743,NULL,1,1,0,NULL),(213,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630887928,NULL,1,1,0,NULL),(214,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630888061,NULL,1,1,0,NULL),(215,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630888085,NULL,1,1,0,NULL),(216,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630888111,NULL,1,1,0,NULL),(217,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630888173,NULL,1,1,0,NULL),(218,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630888231,NULL,1,1,0,NULL),(219,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630888238,NULL,1,1,0,NULL),(220,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630888244,NULL,1,1,0,NULL),(221,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630888253,NULL,1,1,0,NULL),(222,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630888313,NULL,1,1,0,NULL),(223,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630888468,NULL,1,1,0,NULL),(224,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630888589,NULL,1,1,0,NULL),(225,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630888898,NULL,1,1,0,NULL),(226,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630888968,NULL,1,1,0,NULL),(227,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630893889,NULL,1,1,0,NULL),(228,NULL,NULL,NULL,NULL,NULL,NULL,1,1,0.00,0.00,0.00,0.00,0.00,0.00,NULL,NULL,1,NULL,1630893960,NULL,1,1,0,NULL);
/*!40000 ALTER TABLE `tbl_pedido` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_pedido_detalle`
--

DROP TABLE IF EXISTS `tbl_pedido_detalle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_pedido_detalle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `int_pedidoid` int(11) DEFAULT NULL,
  `int_productoid` int(11) DEFAULT NULL,
  `int_cantidad` int(11) DEFAULT 0,
  `double_subTotal` double(5,2) DEFAULT 0.00,
  `double_igv100` double(5,2) DEFAULT 0.00,
  `double_igvTotal` double(5,2) DEFAULT 0.00,
  `double_descuento` double(5,2) DEFAULT 0.00,
  `double_Total` double(5,2) DEFAULT 0.00,
  `int_usercreated` int(11) DEFAULT NULL,
  `int_usermodified` int(11) DEFAULT NULL,
  `int_datecreated` int(11) DEFAULT NULL,
  `int_datemodified` int(11) DEFAULT NULL,
  `int_estadoid` int(11) DEFAULT NULL,
  `is_active` int(11) DEFAULT 1,
  `is_deleted` int(11) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_pedido_detalle`
--

LOCK TABLES `tbl_pedido_detalle` WRITE;
/*!40000 ALTER TABLE `tbl_pedido_detalle` DISABLE KEYS */;
INSERT INTO `tbl_pedido_detalle` VALUES (1,46,1,1,37.72,18.00,8.28,0.00,46.00,1,NULL,1616960888,NULL,1,0,0),(2,47,1,1,37.72,18.00,8.28,0.00,46.00,1,NULL,1616960935,NULL,1,0,0),(3,48,1,1,37.72,18.00,8.28,0.00,46.00,1,NULL,1616961306,NULL,1,0,0),(4,48,2,1,12.30,18.00,2.70,0.00,15.00,1,NULL,1616961311,NULL,1,0,0),(5,51,2,1,12.30,18.00,2.70,0.00,15.00,1,NULL,1617157791,NULL,1,0,0),(6,52,1,1,37.72,18.00,8.28,0.00,46.00,1,NULL,1619144698,NULL,1,0,0),(7,52,2,1,12.30,18.00,2.70,0.00,15.00,1,NULL,1619144705,NULL,1,0,0),(8,101,1,1,37.72,18.00,8.28,0.00,46.00,1,NULL,1629772469,NULL,1,0,0),(9,101,2,1,12.30,18.00,2.70,0.00,15.00,1,NULL,1629772474,NULL,1,0,0),(10,113,1,1,37.72,18.00,8.28,0.00,46.00,1,NULL,1629774398,NULL,1,0,0),(11,113,2,1,12.30,18.00,2.70,0.00,15.00,1,NULL,1629774404,NULL,1,0,0),(12,115,1,1,37.72,18.00,8.28,0.00,46.00,1,NULL,1629774814,NULL,1,0,0),(13,115,2,1,12.30,18.00,2.70,0.00,15.00,1,NULL,1629775049,NULL,1,0,0),(14,120,1,1,37.72,18.00,8.28,0.00,46.00,1,NULL,1629776077,NULL,1,0,0),(15,127,1,1,37.72,18.00,8.28,0.00,46.00,1,NULL,1629777299,NULL,1,0,0),(16,131,1,1,37.72,18.00,8.28,0.00,46.00,1,NULL,1629778130,NULL,1,0,0),(17,133,1,1,37.72,18.00,8.28,0.00,46.00,1,NULL,1630181002,NULL,1,0,0),(18,134,1,1,37.72,18.00,8.28,0.00,46.00,1,NULL,1630181142,NULL,1,0,0),(19,135,2,1,12.30,18.00,2.70,0.00,15.00,1,NULL,1630181628,NULL,1,0,0),(20,136,1,1,37.72,18.00,8.28,0.00,46.00,1,NULL,1630182280,NULL,1,0,0),(21,141,1,1,37.72,18.00,8.28,0.00,46.00,1,NULL,1630183307,NULL,1,0,0),(22,142,1,1,37.72,18.00,8.28,0.00,46.00,1,NULL,1630183453,NULL,1,0,0),(23,146,1,1,37.72,18.00,8.28,0.00,46.00,1,NULL,1630184928,NULL,1,0,0),(24,149,1,1,37.72,18.00,8.28,0.00,46.00,1,NULL,1630780984,NULL,1,0,0),(25,151,1,1,37.72,18.00,8.28,0.00,46.00,1,NULL,1630781717,NULL,1,0,0),(26,165,1,1,37.72,18.00,8.28,0.00,46.00,1,NULL,1630783401,NULL,1,0,0),(27,165,1,1,37.72,18.00,8.28,0.00,46.00,1,NULL,1630786156,NULL,1,0,0),(28,180,1,1,37.72,18.00,8.28,0.00,46.00,1,NULL,1630786212,NULL,1,0,0),(29,NULL,1,1,37.72,18.00,8.28,0.00,46.00,1,NULL,1630787939,NULL,1,0,0),(30,NULL,1,1,37.72,18.00,8.28,0.00,46.00,1,NULL,1630788046,NULL,1,0,0),(31,199,1,1,37.72,18.00,8.28,0.00,46.00,1,NULL,1630884844,NULL,1,0,0),(32,222,1,1,37.72,18.00,8.28,0.00,46.00,1,NULL,1630888416,NULL,1,0,0),(33,224,1,1,37.72,18.00,8.28,0.00,46.00,1,NULL,1630888622,NULL,1,0,0),(34,225,1,1,37.72,18.00,8.28,0.00,46.00,1,NULL,1630888913,NULL,1,0,0),(35,226,1,1,37.72,18.00,8.28,0.00,46.00,1,NULL,1630889176,NULL,1,0,0),(36,228,2,1,12.30,18.00,2.70,0.00,15.00,1,NULL,1630894076,NULL,1,0,0),(37,228,1,1,37.72,18.00,8.28,0.00,46.00,1,NULL,1630894086,NULL,1,0,0);
/*!40000 ALTER TABLE `tbl_pedido_detalle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_pedido_factura`
--

DROP TABLE IF EXISTS `tbl_pedido_factura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_pedido_factura` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `int_facturaid` int(11) NOT NULL,
  `int_pedidoid` int(11) NOT NULL,
  `int_clienteid` int(11) NOT NULL,
  `chr_codmesa` int(11) NOT NULL,
  `int_usercreated` int(11) DEFAULT NULL,
  `int_usermodified` int(11) DEFAULT NULL,
  `int_datecreated` int(11) DEFAULT NULL,
  `int_datemodified` int(11) DEFAULT NULL,
  `is_active` int(11) DEFAULT 1,
  `is_deleted` int(11) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_pedido_factura`
--

LOCK TABLES `tbl_pedido_factura` WRITE;
/*!40000 ALTER TABLE `tbl_pedido_factura` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_pedido_factura` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_producto`
--

DROP TABLE IF EXISTS `tbl_producto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_producto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chr_codproducto` char(6) CHARACTER SET utf8mb4 NOT NULL,
  `chr_nombre` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL,
  `double_preciounitario` double(5,2) NOT NULL DEFAULT 0.00,
  `date_desde` date DEFAULT NULL,
  `date_hasta` date DEFAULT NULL,
  `int_usercreated` int(11) DEFAULT NULL,
  `int_usermodified` int(11) DEFAULT NULL,
  `int_datecreated` int(11) DEFAULT NULL,
  `int_datemodified` int(11) DEFAULT NULL,
  `is_active` int(11) DEFAULT 1,
  `is_deleted` int(11) DEFAULT 0,
  `created_at` varchar(45) DEFAULT NULL,
  `is_indeterminate` char(1) DEFAULT NULL,
  `updated_at` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_producto`
--

LOCK TABLES `tbl_producto` WRITE;
/*!40000 ALTER TABLE `tbl_producto` DISABLE KEYS */;
INSERT INTO `tbl_producto` VALUES (1,'P01','1 POLLO ENTERO',46.00,NULL,NULL,1,NULL,1615484078,NULL,1,0,'2021-03-11T17:34:38.000000Z',NULL,'2021-03-11T17:34:38.000000Z'),(2,'M01','MOLLEJITAS',15.00,NULL,NULL,1,NULL,1616908076,NULL,1,0,'2021-03-28 05:07:56',NULL,'2021-03-28 05:07:56');
/*!40000 ALTER TABLE `tbl_producto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_producto_combo`
--

DROP TABLE IF EXISTS `tbl_producto_combo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_producto_combo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chr_codproducto` char(6) CHARACTER SET utf8mb4 NOT NULL,
  `chr_codcombo` varchar(8) NOT NULL,
  `int_usercreated` int(11) DEFAULT NULL,
  `int_usermodified` int(11) DEFAULT NULL,
  `int_datecreated` int(11) DEFAULT NULL,
  `int_datemodified` int(11) DEFAULT NULL,
  `is_active` int(11) DEFAULT 1,
  `is_deleted` int(11) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_producto_combo`
--

LOCK TABLES `tbl_producto_combo` WRITE;
/*!40000 ALTER TABLE `tbl_producto_combo` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_producto_combo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_proveedores`
--

DROP TABLE IF EXISTS `tbl_proveedores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_proveedores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `int_tipo` char(1) CHARACTER SET utf8mb4 NOT NULL COMMENT '1:DNI | 2:RUC | 3:por definir',
  `chr_dni` varchar(10) CHARACTER SET utf8mb4 DEFAULT NULL,
  `chr_ruc` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL,
  `chr_telefono` varchar(9) CHARACTER SET utf8mb4 DEFAULT NULL,
  `chr_celular` char(9) CHARACTER SET utf8mb4 DEFAULT NULL,
  `chr_telefono2` varchar(9) CHARACTER SET utf8mb4 DEFAULT NULL,
  `chr_razonsocial` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL,
  `chr_nombrecomercial` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL,
  `chr_nombre` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  `chr_apellido` varchar(80) CHARACTER SET utf8mb4 DEFAULT NULL,
  `int_usercreated` int(11) DEFAULT NULL,
  `int_usermodified` int(11) DEFAULT NULL,
  `int_datecreated` int(11) DEFAULT NULL,
  `int_datemodified` int(11) DEFAULT NULL,
  `is_active` int(11) DEFAULT 1,
  `is_deleted` int(11) DEFAULT 0,
  `chr_direccion` varchar(150) DEFAULT NULL,
  `updated_at` varchar(45) DEFAULT NULL,
  `created_at` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_proveedores`
--

LOCK TABLES `tbl_proveedores` WRITE;
/*!40000 ALTER TABLE `tbl_proveedores` DISABLE KEYS */;
INSERT INTO `tbl_proveedores` VALUES (3,'1',NULL,'20202020','23223','98862322','232323','pepesSAC1','pepessa1',NULL,NULL,1,NULL,1612837605,NULL,0,1,'san juan11','2021-02-09 03:57:28','2021-02-09T02:26:45.000000Z'),(4,'2','47271288',NULL,'2872727','98276178',NULL,NULL,NULL,'luis','tello',1,NULL,1612837734,NULL,0,1,'Villa tu terror22','2021-02-09 03:53:19','2021-02-09T02:28:54.000000Z'),(5,'2','47271277',NULL,'2872344','98276222',NULL,NULL,NULL,'marco','brinchis',1,NULL,1612837793,NULL,0,1,'donde roban','2021-02-09 06:27:51','2021-02-09 02:29:53'),(6,'1',NULL,'4141414567','323222','76991109','232322','marcosoft1','marcosTI12',NULL,NULL,1,NULL,1612837836,NULL,0,1,'donde roban 2','2021-02-09 04:04:03','2021-02-09T02:30:36.000000Z'),(7,'2','77766555','77766555','2282672','98726276',NULL,'felix rojas perez',NULL,'felix','rojas perez',1,NULL,1612851877,NULL,1,0,'san juan4','2021-02-09T06:24:37.000000Z','2021-02-09T06:24:37.000000Z'),(8,'1',NULL,'2020212452','23232121','98725627','2323442','carro sac','mororer asda',NULL,NULL,1,NULL,1612852055,NULL,1,0,'vistoca 12143','2021-02-09 06:27:35','2021-02-09 06:27:35'),(10,'1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1643842275,NULL,1,0,NULL,'2022-02-02 22:51:15','2022-02-02 22:51:15');
/*!40000 ALTER TABLE `tbl_proveedores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_puesto_laboral`
--

DROP TABLE IF EXISTS `tbl_puesto_laboral`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_puesto_laboral` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chr_codcargo` varchar(6) CHARACTER SET utf8mb4 NOT NULL,
  `chr_nombre` varchar(30) CHARACTER SET utf8mb4 DEFAULT NULL,
  `chr_descripcion` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL,
  `int_usercreated` int(11) DEFAULT NULL,
  `int_usermodified` int(11) DEFAULT NULL,
  `int_datecreated` int(11) DEFAULT NULL,
  `int_datemodified` int(11) DEFAULT NULL,
  `is_active` int(11) DEFAULT 1,
  `is_deleted` int(11) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_puesto_laboral`
--

LOCK TABLES `tbl_puesto_laboral` WRITE;
/*!40000 ALTER TABLE `tbl_puesto_laboral` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_puesto_laboral` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_reservacion`
--

DROP TABLE IF EXISTS `tbl_reservacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_reservacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_fecha` date NOT NULL,
  `time_hora` time NOT NULL,
  `int_clienteid` varchar(8) CHARACTER SET utf8mb4 NOT NULL,
  `int_usercreated` int(11) DEFAULT NULL,
  `int_usermodified` int(11) DEFAULT NULL,
  `int_datecreated` int(11) DEFAULT NULL,
  `int_datemodified` int(11) DEFAULT NULL,
  `is_active` int(11) DEFAULT 1,
  `is_deleted` int(11) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_reservacion`
--

LOCK TABLES `tbl_reservacion` WRITE;
/*!40000 ALTER TABLE `tbl_reservacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_reservacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_rol`
--

DROP TABLE IF EXISTS `tbl_rol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_rol` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chr_nombre` varchar(20) CHARACTER SET utf8mb4 NOT NULL,
  `chr_descripcion` varchar(20) CHARACTER SET utf8mb4 NOT NULL,
  `int_usercreated` int(11) DEFAULT NULL,
  `int_usermodified` int(11) DEFAULT NULL,
  `int_datecreated` int(11) DEFAULT NULL,
  `int_datemodified` int(11) DEFAULT NULL,
  `is_active` int(11) DEFAULT 1,
  `is_deleted` int(11) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_rol`
--

LOCK TABLES `tbl_rol` WRITE;
/*!40000 ALTER TABLE `tbl_rol` DISABLE KEYS */;
INSERT INTO `tbl_rol` VALUES (1,'ADMINISTRADOR','ADMINISTRADOR',1,0,0,0,1,0),(2,'CAJERA','CAJERA',1,0,0,0,1,0),(3,'COCINERO','COCINERO',1,0,0,0,1,0),(4,'MOZO','MOZO',1,0,0,0,1,0),(5,'MOTORIZADO','MOTORIZADO',1,0,0,0,1,0);
/*!40000 ALTER TABLE `tbl_rol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_rol_menu`
--

DROP TABLE IF EXISTS `tbl_rol_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_rol_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `int_menuid` int(11) NOT NULL,
  `int_rolid` int(11) NOT NULL,
  `int_order` int(11) DEFAULT 0,
  `int_usercreated` int(11) DEFAULT NULL,
  `int_usermodified` int(11) DEFAULT NULL,
  `int_datecreated` int(11) DEFAULT NULL,
  `int_datemodified` int(11) DEFAULT NULL,
  `is_active` int(11) DEFAULT 1,
  `is_deleted` int(11) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_rol_menu`
--

LOCK TABLES `tbl_rol_menu` WRITE;
/*!40000 ALTER TABLE `tbl_rol_menu` DISABLE KEYS */;
INSERT INTO `tbl_rol_menu` VALUES (1,1,1,0,1,NULL,NULL,NULL,1,0),(2,2,1,0,1,NULL,NULL,NULL,1,0),(3,3,1,0,1,NULL,NULL,NULL,1,0),(4,4,1,0,1,NULL,NULL,NULL,1,0),(5,5,1,0,1,NULL,NULL,NULL,1,0),(6,6,1,0,1,NULL,NULL,NULL,1,0),(7,7,1,0,1,NULL,NULL,NULL,1,0),(8,8,1,0,1,NULL,NULL,NULL,1,0),(9,9,1,0,1,NULL,NULL,NULL,1,0),(10,10,1,0,1,NULL,NULL,NULL,1,0),(11,3,4,0,1,NULL,NULL,NULL,1,0),(12,10,4,0,1,NULL,NULL,NULL,1,0),(13,8,4,0,1,NULL,NULL,NULL,1,0),(14,11,1,0,1,NULL,NULL,NULL,1,0),(15,12,1,0,1,NULL,NULL,NULL,1,0),(16,13,1,0,1,NULL,NULL,NULL,1,0),(17,14,1,0,1,NULL,NULL,NULL,1,0),(18,15,1,0,1,NULL,NULL,NULL,1,0),(19,16,1,0,1,NULL,NULL,NULL,1,0),(20,17,1,0,1,NULL,NULL,NULL,1,0),(21,18,1,0,1,NULL,NULL,NULL,1,0),(22,19,1,0,1,NULL,NULL,NULL,1,0),(23,20,1,0,1,NULL,NULL,NULL,1,0),(24,21,1,0,1,NULL,NULL,NULL,1,0),(25,22,1,0,1,NULL,NULL,NULL,1,0),(26,23,1,0,1,NULL,NULL,NULL,1,0),(27,24,1,0,1,NULL,NULL,NULL,1,0),(28,25,1,0,1,NULL,NULL,NULL,1,0),(29,26,1,0,1,NULL,NULL,NULL,1,0),(30,27,1,0,1,NULL,NULL,NULL,1,0),(31,28,1,0,1,NULL,NULL,NULL,1,0),(32,29,1,0,1,NULL,NULL,NULL,1,0),(33,30,1,0,1,NULL,NULL,NULL,1,0);
/*!40000 ALTER TABLE `tbl_rol_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_tipo_pago`
--

DROP TABLE IF EXISTS `tbl_tipo_pago`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_tipo_pago` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chr_nombre` varchar(45) DEFAULT NULL,
  `created_at` varchar(45) DEFAULT NULL,
  `updated_at` varchar(45) DEFAULT NULL,
  `is_active` int(11) DEFAULT 1,
  `is_deleted` int(11) DEFAULT 0,
  `int_usercreated` int(11) DEFAULT NULL,
  `int_datecreated` int(11) DEFAULT NULL,
  `int_usermodified` int(11) DEFAULT NULL,
  `int_datemodified` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_tipo_pago`
--

LOCK TABLES `tbl_tipo_pago` WRITE;
/*!40000 ALTER TABLE `tbl_tipo_pago` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_tipo_pago` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_tipo_pedido`
--

DROP TABLE IF EXISTS `tbl_tipo_pedido`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_tipo_pedido` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chr_nombre` varchar(45) DEFAULT NULL,
  `created_at` varchar(45) DEFAULT NULL,
  `updated_at` varchar(45) DEFAULT NULL,
  `is_active` int(11) DEFAULT 1,
  `is_deleted` int(11) DEFAULT 0,
  `int_usercreated` int(11) DEFAULT NULL,
  `int_datecreated` int(11) DEFAULT NULL,
  `int_usermodified` int(11) DEFAULT NULL,
  `int_datemodified` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_tipo_pedido`
--

LOCK TABLES `tbl_tipo_pedido` WRITE;
/*!40000 ALTER TABLE `tbl_tipo_pedido` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_tipo_pedido` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_combo`
--

DROP TABLE IF EXISTS `tipo_combo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_combo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chr_nombre` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL,
  `int_usercreated` int(11) DEFAULT NULL,
  `int_usermodified` int(11) DEFAULT NULL,
  `int_datecreated` int(11) DEFAULT NULL,
  `int_datemodified` int(11) DEFAULT NULL,
  `is_active` int(11) DEFAULT 1,
  `is_deleted` int(11) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_combo`
--

LOCK TABLES `tipo_combo` WRITE;
/*!40000 ALTER TABLE `tipo_combo` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipo_combo` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-02-02 17:52:34
