<?php

namespace App\Models\Ventas;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
    use HasFactory;
    protected $table = 'tbl_pedido';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'chr_numdocumento',
        'chr_serie',
        'chr_numero',
        'int_empleadoid',
        'int_clienteid',
        'date_fecha',        
        'int_tipopagoid',
        'int_monedaid',
        'double_tipocambio',
        'double_subTotal',
        'double_igv100',
        'double_igvTotal',
        'double_descuento',
        'double_Total',
        'txt_direccion',
        'txt_motorizado',
        'int_usercreated',
        'int_usermodified',
        'int_datecreated',
        'int_datemodified',
        'int_estadoid',
        'is_active',
        'is_deleted',
        'txt_observaciones',
    ];
}
