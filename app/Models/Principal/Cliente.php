<?php

namespace App\Models\Principal;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    use HasFactory;
    protected $table = 'tbl_clientes';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'chr_celular', 
        'chr_telefono', 
        'chr_correo', 
        'is_subscriptor', 
        'int_usermodified', 
        'int_datemodified',
        'int_usercreated',
        'int_datecreated', 
        'is_active', 
        'is_deleted',
        'date_fecha_inicio',
        'date_fecha_fin',
        'int_tipo',
        'int_mst_estado',
        'chr_ruc',
        'int_id_persona',
        'int_id_empresa',
    ];
}
