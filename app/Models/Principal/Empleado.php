<?php

namespace App\Models\Principal;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Empleado extends Model
{
    use HasFactory;
    protected $table = 'tbl_empleados';
    protected $primaryKey = 'id';
    protected $fillable = [
        'chr_codempleado', 
        'chr_nombre', 
        'chr_apellido', 
        'chr_telefono', 
        'chr_celular', 
        'chr_direccion', 
        'chr_documento', 
        'chr_correo', 
        'chr_password', 
        'int_usercreated', 
        'int_datecreated', 
        'int_usermodified', 
        'int_datemodified', 
        'is_active', 
        'is_deleted', 
        'chr_urlfoto', 
        'int_rolid',
        'ubigeo_id',
    ];
}
