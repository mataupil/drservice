<?php

namespace App\Models\Principal;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    use HasFactory;
    protected $table = 'tbl_personas';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'chr_dni', 
        'chr_nombre', 
        'chr_apellido',
        'chr_ruc',
    ];
}
