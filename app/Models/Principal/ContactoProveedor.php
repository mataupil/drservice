<?php

namespace App\Models\Principal;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContactoProveedor extends Model
{
    use HasFactory;
    protected $table = 'tbl_contacto_proveedor';
    protected $primaryKey = 'id';
    protected $fillable = [
        'int_proveedor', 
        'chr_nombre', 
        'chr_descripcion', 
        'chr_telefono', 
        'chr_celular', 
        'chr_correo', 
        'is_active', 
        'is_deleted',
        'int_usercreated',
        'int_datecreated', 
        'int_usermodified',
        'int_datemodified',
    ];
}
