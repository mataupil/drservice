<?php

namespace App\Models\Principal;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    use HasFactory;
    protected $table = 'tbl_empresas';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'chr_ruc', 
        'chr_razon_social', 
        'chr_nombre_comercial',
    ];
}
