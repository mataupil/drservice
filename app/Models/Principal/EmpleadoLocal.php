<?php

namespace App\Models\Principal;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmpleadoLocal extends Model
{
    use HasFactory;
    protected $table = 'tbl_empleados_local';
    protected $primaryKey = 'id';
    protected $fillable = [
        'int_empleado', 
        'int_local', 
        'int_rolid', 
        'int_usercreated', 
        'int_usermodified', 
        'int_datecreated', 
        'int_datemodified', 
        'int_localdefault', 
        'is_active', 
        'is_deleted', 
        'is_assigned'
    ];
}
