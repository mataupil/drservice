<?php

namespace App\Models\Principal;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Proveedor extends Model
{
    use HasFactory;
    protected $table = 'tbl_proveedores';
    protected $primaryKey = 'id';
    protected $fillable = [
        'int_tipo',
        'chr_dni',
        'chr_ruc',
        'chr_telefono',
        'chr_celular',
        'chr_telefono2',
        'chr_razonsocial',
        'chr_nombrecomercial',
        'chr_nombre',
        'chr_apellido',
        'int_usercreated',
        'int_usermodified',
        'int_datecreated',
        'int_datemodified',
        'is_active',
        'is_deleted',
        'chr_direccion',
    ]; 
}
