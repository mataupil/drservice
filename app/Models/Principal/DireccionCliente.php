<?php

namespace App\Models\Principal;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DireccionCliente extends Model
{
    use HasFactory;
    protected $table = 'tbl_direccion_cliente';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'int_clienteid', 
        'chr_direccion',
        'chr_referencia',
        'is_active',
        'is_delete',
        'is_default',
        'int_usercreated',
        'int_datecreated',
        'int_mst_estado',
    ];
}
