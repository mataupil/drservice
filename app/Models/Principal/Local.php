<?php

namespace App\Models\Principal;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Local extends Model
{
    use HasFactory;
    protected $table = 'tbl_locales';
    protected $primaryKey = 'id';
    protected $fillable = [
        'chr_codlocal', 
        'chr_nombre', 
        'chr_direccion', 
        'chr_telefono', 
        'int_latitud', 
        'int_longitud', 
        'int_usercreated', 
        'int_usermodified', 
        'int_datecreated', 
        'int_datemodified', 
        'is_active', 
        'is_deleted'
    ];
}
