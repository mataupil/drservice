<?php

namespace App\Models\Inventario;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MateriaPrimaLocal extends Model
{
    use HasFactory;
    protected $table = 'tbl_ingrediente_local';
    protected $primaryKey = 'id';
}
