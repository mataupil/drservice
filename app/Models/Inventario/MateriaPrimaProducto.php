<?php

namespace App\Models\Inventario;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MateriaPrimaProducto extends Model
{
    use HasFactory;
    protected $table = 'tbl_ingrediente_producto';
    protected $primaryKey = 'id';
}
