<?php

namespace App\Models\Mantenimiento;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rol extends Model
{
    use HasFactory;
    protected $table = 'tbl_rol';
    protected $primaryKey = 'id';
    protected $fillable = [
        'chr_nombre', 
        'chr_descripcion', 
        'int_usercreated', 
        'int_usermodified', 
        'int_datecreated', 
        'int_datemodified', 
        'is_active', 
        'is_deleted', 
    ];
    public $timestamps = false;
}
