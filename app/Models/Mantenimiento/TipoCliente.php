<?php

namespace App\Models\Mantenimiento;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TipoCliente extends Model
{
    use HasFactory;
    protected $table = 'tbl_tipo_cliente';
    protected $primaryKey = 'id';
    protected $fillable = [
        'chr_nombre', 
    ];
}
