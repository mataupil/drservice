<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ImgBase64ServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        require_once app_path() . '/Helpers/ImgBase64.php'; // cargando el helper dentro de este servicio
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
