<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class StrUtf8ServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        require_once app_path() . '/Helpers/StrUtf8.php'; // cargando el helper dentro de este servicio
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
