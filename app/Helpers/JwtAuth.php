<?php
namespace App\Helpers;

use App\Models\User;
use App\Models\Principal\Empleado;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
// use Doctrine\Instantiator\Exception\UnexpectedValueException;
// use DomainException;

class JwtAuth{

    public $key;

    public function __construct(){
        $this->key = 'esta-es-mi-clave-secreta-28479837498374';
    }

    public function signup($chr_codempleado, $chr_password, $geToken=null){
        $empleado = Empleado::where(
            array(
                'chr_codempleado' => $chr_codempleado,
                'chr_password' => $chr_password
            )
        )->first();

        $signup = false;

        if(is_object($empleado)){
            $signup = true;
        }

        if($signup){
            // generar token
            $token = array(
                'sub' => $empleado->id,
                'chr_codempleado' => $empleado->chr_codempleado,
                'chr_nombre' => $empleado->chr_nombre,
                'chr_apellido' => $empleado->chr_apellido,
                'chr_documento' => $empleado->chr_documento,
                'iat' => time(),
                'exp' => time() + (1*24*60*60) // el token caducara en 1 dia
            );

            // token cifrado creado
            $jwt = JWT::encode($token, $this->key, 'HS256'); // 'HS256' es un algoritmo de codificacion

            // objeto del usuario que se ha logeado
            // $decoded = JWT::decode($jwt, $this->key, array('HS256')); // obsoleto
            $decoded = JWT::decode($jwt, new Key($this->key, 'HS256'));
            // return response()->json($decoded, 200);
            // die();

            if(is_null($geToken)){
                return $jwt;
            }else{
                return $decoded;
            }

        }else{
            // devolver error
            return array('status' => 'error', 'message' => 'Login ha fallado!!');
        }
    }

    public function checkToken($jwt, $getIdentity = false){
        $auth = false;
        $decoded = '';

        try{
            // $decoded = JWT::decode($jwt, $this->key, array('HS256')); obsoleto
            $decoded = JWT::decode($jwt, new Key($this->key, 'HS256'));
        }catch(\UnexpectedValueException $e){
            $auth = false;
        }catch(\DomainException $e){
            $auth = false;
        }

        if(is_object($decoded) && isset($decoded->sub)){
            $auth = true;
        }else{
            $auth = false;
        }

        if($getIdentity){
            return $decoded;
        }

        return $auth;
    }
}
