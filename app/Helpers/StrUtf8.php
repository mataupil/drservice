<?php
namespace App\Helpers;

// use Doctrine\Instantiator\Exception\UnexpectedValueException;
// use DomainException;

class StrUtf8{

    public function __construct(){
       
    }

    public function convertUpperCase($str){  
        $upperCase = '';
        mb_internal_encoding('UTF-8');

        if(!mb_check_encoding($str, 'UTF-8') OR !($str === mb_convert_encoding(mb_convert_encoding($str, 'UTF-32', 'UTF-8' ), 'UTF-8', 'UTF-32'))) {
            $str = mb_convert_encoding($str, 'UTF-8'); 
            $upperCase = mb_convert_case($str, MB_CASE_UPPER, "UTF-8"); 
        }else{
            $upperCase = strtoupper($str);
        }

        return $upperCase;
    }
}
