<?php

namespace App\Http\Controllers\Ventas;

use stdClass;
use App\Helpers\JwtAuth;
use Illuminate\Http\Request;
use App\Models\Ventas\Pedido;
use App\Models\Principal\Cliente;
use App\Models\Principal\Empleado;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Ventas\PedidoDetalle;

class PedidoController extends Controller
{
    const MENU_LEVEL_PADRE = 1;
    const MENU_LEVEL_HIJO = 2;
    const PEDIDO_STATUS_BORRADOR = 1;
    const PEDIDO_REGISTRADO = 2; 
    const PEDIDO_COCINA_RECIBIDO = 3;
    const PEDIDO_COCINA_PROCESO = 4;
    const PEDIDO_COCINA_DESPACHADO = 5;
    const PEDIDO_FACTURACION = 6;
    const PEDIDO_DELIVERY = 7;
    const PEDIDO_FINALIZADO = 8;
    
    public function registrarPedido(Request $req){
        $hash = $req->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if($checkToken){
            $this->validate($req, [
                'chr_numdocumento'  => 'required|max:20|string',
                'chr_serie'         => 'required|max:4|string',
                'chr_numero'        => 'required|max:8|string',
                'int_empleadoid'    => 'required|integer',
                'int_clienteid'     => 'required|integer',
                'date_fecha'        => 'required',
                'int_tipopagoid'    => 'required|integer',
                'int_monedaid'      => 'required|integer',
                'double_tipocambio' => 'required|numeric',
                'double_subTotal'   => 'required|numeric',
                'double_igv100'     => 'required|numeric',
                'double_igvTotal'   => 'required|numeric',
                'double_descuento'  => 'required|numeric',
                'double_total'      => 'required|numeric',
                'txt_direccion'     => 'required|max:300|string',
                'txt_motorizado'    => 'required|max:300|string',
                'int_usercreated'   => 'required|integer',
                'int_estadoid'      => 'required|integer',
                'txt_observaciones' => 'required|max:300|string'
            ]);
            if($req->chr_numdocumento){
                $checkExistsNumDocumento = Pedido::where('chr_numdocumento', $req->chr_numdocumento)->count();
                if ($checkExistsNumDocumento >= 1) {
                    return response()->json([
                        'success' => false,
                        'message' => "Este número de documento ya se encuentra registrado!"
                    ], 406);
                }
            }
            if($req->chr_serie){
                $checkExistsSerie = Pedido::where('chr_serie', $req->chr_serie)->count();
                if ($checkExistsSerie >= 1) {
                    return response()->json([
                        'success' => false,
                        'message' => "Esta serie ya se encuentra registrada!"
                    ], 406);
                }
            }
            if($req->chr_numero){
                $checkExistsNum = Pedido::where('chr_numero', $req->chr_numero)->count();
                if ($checkExistsNum >= 1) {
                    return response()->json([
                        'success' => false,
                        'message' => "Esta número ya se encuentra registrado!"
                    ], 406);
                }
            }
            $checkExistsUserCreated = Empleado::find($req->int_usercreated);
            if(!$checkExistsUserCreated){
                return response()->json([
                    'success' => false,
                    'message' => 'El usuario que intento hacer esta operación, no existe en el sistema!',
                ], 406);
            }
            if($checkExistsUserCreated){
                $time = time();

                $pedido = Pedido::create([
                    'chr_numdocumento'  => $req->chr_numdocumento,
                    'chr_serie'         => $req->chr_serie,
                    'chr_numero'        => $req->chr_numero,
                    'int_empleadoid'    => $req->int_empleadoid,
                    'int_clienteid'     => $req->int_clienteid,
                    'date_fecha'        => $req->date_fecha,
                    'int_tipopagoid'    => $req->int_tipopagoid,
                    'int_monedaid'      => $req->int_monedaid,
                    'double_tipocambio' => $req->double_tipocambio,
                    'double_subTotal'   => $req->double_subTotal,
                    'double_igv100'     => $req->double_igv100,
                    'double_igvTotal'   => $req->double_igvTotal,
                    'double_descuento'  => $req->double_descuento,
                    'double_Total'      => $req->double_total,
                    'txt_direccion'     => $req->txt_direccion ? strtoupper($req->txt_direccion) : null,
                    'txt_motorizado'    => $req->txt_motorizado ? strtoupper($req->txt_motorizado) : null,
                    'int_usercreated'   => $req->int_usercreated,
                    'int_datecreated'   => $time,
                    'int_estadoid'      => $req->int_estadoid,
                    'txt_observaciones' => $req->txt_observaciones ? strtoupper($req->txt_observaciones) : null,
                ]);

                return response()->json([
                    'success'   => true,
                    'message'   => 'Pedido registrado exitosamente!',
                    'data'      => $pedido,
                ], 200);
            }
        }else{
            return response()->json([
                'success' => false,
                'message' => "El usuario que intento hacer esta operación no se encuentra logeado en el sistema!"
            ], 406);
        }
    }

    public function listarPedidos(Request $req){
        $hash = $req->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if($checkToken){
            $lista_pedidos = DB::table('tbl_pedido as pe')->join('tbl_clientes as cl', 'cl.id', '=', 'pe.int_clienteid')
                                                          ->join('tbl_mst_estado as est', 'est.id', '=', 'pe.int_estadoid')
                                                          ->where('pe.is_active', 1)
                                                          ->select('pe.id', 'pe.chr_numdocumento', 'pe.chr_serie', 'pe.chr_numero', 'pe.int_estadoid', 'cl.chr_nombre as cli_nombre', 'cl.chr_apellido', 'est.chr_colorstyle')
                                                          ->orderBy('pe.id', 'desc')
                                                          ->get();

            return response()->json([
                'success'   => true,
                'data'      => $lista_pedidos
            ], 200);
        }else{
            return response()->json([
                'success' => false,
                'message' => "El usuario que intento hacer esta operación no se encuentra logeado en el sistema!"
            ], 406);
        }
    }

    public function buscarPedidoID($id, Request $req){
        $hash = $req->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if($checkToken){
            $pedido = Pedido::find($id);
            $cliente = Cliente::find($pedido->int_clienteid);

            return response()->json([
                'success'   => true,
                'data'      => [
                    'pedido'    => $pedido,
                    'cliente'   => $cliente,
                ]
            ], 200);
        }else{
            return response()->json([
                'success' => false,
                'message' => "El usuario que intento hacer esta operación no se encuentra logeado en el sistema!"
            ], 406);
        }
    }

    public function actualizarPedido($id, Request $req){
        $hash = $req->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if($checkToken){
            $this->validate($req, [
                // 'chr_numdocumento'  => 'required|max:20|string',
                'int_empleadoid'    => 'required|integer',
                'int_clienteid'     => 'required|integer',
                'date_fecha'        => 'required',
                'int_tipopagoid'    => 'required|integer',
                'int_monedaid'      => 'required|integer',
                'double_tipocambio' => 'required|numeric',
                'double_subTotal'   => 'required|numeric',
                'double_igv100'     => 'required|numeric',
                'double_igvTotal'   => 'required|numeric',
                'double_descuento'  => 'required|numeric',
                'double_total'      => 'required|numeric',
                'txt_direccion'     => 'required|max:300|string',
                'txt_motorizado'    => 'required|max:300|string',
                'int_usermodified'  => 'required|integer',
                'int_estadoid'      => 'required|integer',
                'txt_observaciones' => 'required|max:300|string'
            ]);

            $checkExistsUserModified = Empleado::find($req->int_usermodified);
            if(!$checkExistsUserModified){
                return response()->json([
                    'success' => false,
                    'message' => 'El usuario que intento hacer esta operación, no existe en el sistema!',
                ], 406);
            }
            if($checkExistsUserModified){
                $time = time();

                $pedido = Pedido::find($id);

                $pedido->update([
                    // 'chr_numdocumento'  => $req->chr_numdocumento,
                    'int_empleadoid'    => $req->int_empleadoid,
                    'int_clienteid'     => $req->int_clienteid,
                    'date_fecha'        => $req->date_fecha,
                    'int_tipopagoid'    => $req->int_tipopagoid,
                    'int_monedaid'      => $req->int_monedaid,
                    'double_tipocambio' => $req->double_tipocambio,
                    'double_subTotal'   => $req->double_subTotal,
                    'double_igv100'     => $req->double_igv100,
                    'double_igvTotal'   => $req->double_igvTotal,
                    'double_descuento'  => $req->double_descuento,
                    'double_Total'      => $req->double_total,
                    'txt_direccion'     => $req->txt_direccion ? strtoupper($req->txt_direccion) : null,
                    'txt_motorizado'    => $req->txt_motorizado ? strtoupper($req->txt_motorizado) : null,
                    'int_usermodifiedd' => $req->int_usermodifiedd,                   
                    'int_dateumodified' => $time,
                    'int_estadoid'      => $req->int_estadoid,
                    'txt_observaciones' => $req->txt_observaciones ? strtoupper($req->txt_observaciones) : null,
                ]);

                return response()->json([
                    'success'   => true,
                    'message'   => 'Pedido actualizado exitosamente!',
                    'data'      => $pedido,
                ], 200);
            }
        }else{
            return response()->json([
                'success' => false,
                'message' => "El usuario que intento hacer esta operación no se encuentra logeado en el sistema!"
            ], 406);
        }
    }

    public function eliminarPedido($id, Request $req){
        $hash = $req->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if($checkToken){
            $cliente = Pedido::find($id);
            $cliente->update([
                'is_active'     => 0,
                'is_deleted'    => 1,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Pedido eliminado exitosamente!'
            ], 200);
        }else{
            return response()->json([
                'success' => false,
                'message' => "El usuario que intento hacer esta operación no se encuentra logeado en el sistema!"
            ], 406);
        }  
    }

    public function obtenerPedidoById($id){
        $objResult = new stdClass();
        if ($id > 0) {
            //obtenemos el pedido
            $objPedido = Pedido::find($id);
            if (is_object($objPedido)) {
                $detalle = $this->getPedidoDetalleByPedidoId($objPedido->id);
                $objResult->id          = $objPedido->id;
                $objResult->serie       = $objPedido->chr_serie;
                $objResult->numero      = $objPedido->chr_numero;
                $objResult->fecha       = $objPedido->date_fecha;
                $objResult->documento   = $objPedido->chr_numdocumento;
                $objResult->cliente     = $objPedido->int_clienteid;
                $objResult->formPago    = $objPedido->int_tipopagoid;
                $objResult->mozo        = $objPedido->int_empleadoid;
                $objResult->tipoCambio  = $objPedido->double_tipocambio;
                $objResult->moneda      = $objPedido->int_monedaid;
                $objResult->direccion   = $objPedido->txt_direccion;
                $objResult->motorizado  = $objPedido->txt_motorizado;
                $objResult->observaciones = $objPedido->txt_observaciones;
                $objResult->subTotal    = $objPedido->double_subTotal;
                $objResult->subTotalIgv = $objPedido->double_igvTotal;
                // $objResult->igv100      = $objPedido->double_igv100;
                // $objResult->igvTotal    = $objPedido->double_igvTotal;
                // $objResult->descuento   = $objPedido->double_descuento;
                $objResult->total       = $objPedido->double_Total;
                
                // $objResult->detalles    = is_array($objPedido->detalle)?$objPedido->detalle:[];

            }
        }else{
            //registramos un pedido en status borrador
            $pedido    = new Pedido();
            $pedido->int_estadoid = self::PEDIDO_STATUS_BORRADOR;
            $pedido->int_usercreated = 1;
            $pedido->int_datecreated = time();
            $pedido->save();

            $objResult->id          = $pedido->id;
            $objResult->serie       = "";
            $objResult->numero      = "";
            $objResult->fecha       = date('Y-m-d');
            $objResult->documento   = "";
            $objResult->cliente     = "";
            $objResult->mozo        = "";
            $objResult->formPago    = 1;
            $objResult->tipoCambio  = 4.10;
            $objResult->moneda      = "";
            $objResult->direccion   = "";
            $objResult->motorizado  = "";
            $objResult->observaciones = "";
            $objResult->subTotal    = 0.00;
            // $objResult->igv100      = "";
            // $objResult->igvTotal    = "";
            // $objResult->descuento   = "";
            $objResult->subTotalIgv = 0.00;
            $objResult->total       = 0.00;
            
            $detalle = [];
        }

        $data = array(
            'status' => 'success',
            'code' => 200,
            'Pedido' => $objResult,
            'PedidoDetalle' => $detalle
                
        );
        return response()->json($data,200);
    }

    public function savePedido(Request $request){
        $json = $request->input('json',null);
        $params = json_decode($json);
        return response()->json($params,200);
    }

    public function saveProductoPedido(Request $request){
        $json = $request->input('json',null);
        $params = json_decode($json);

        $pDetalle = new PedidoDetalle();
        $pDetalle->int_pedidoid         = $params->pedidoid;
        $pDetalle->int_productoid       = $params->productoid;
        $pDetalle->int_cantidad         = $params->cantidad;
        $pDetalle->double_subTotal      = $params->subtotal;
        $pDetalle->double_igv100        = $params->igv100;
        $pDetalle->double_igvTotal      = $params->igv;
        $pDetalle->double_descuento     = $params->descuento;
        $pDetalle->double_Total         = $params->precio;
        $pDetalle->int_usercreated      = $params->user;
        $pDetalle->int_datecreated      = time();
        $pDetalle->int_estadoid         = self::PEDIDO_STATUS_BORRADOR;
        $pDetalle->is_active            = 0;
        $pDetalle->save();

        //obtenemos el detalle del pedido
        $detalle = $this->getPedidoDetalleByPedidoId($params->pedidoid);//);
        $data = array(
            'status' => 'success',
            'code' => 200,
            'message' => 'Producto agregado correctamente',
            'PedidoDetalle' => $detalle
        );
        return response()->json($data,200);
    }

    public function registrar(Request $request){
        //obtenemos los datos enviados por json
        $json = $request->input('json',null);
        $params = json_decode($json);
        die;
    }

    public function getPedidoDetalleByPedidoId($id){
        return PedidoDetalle::from('tbl_pedido_detalle as pd')
                     ->join('tbl_producto as p', 'p.id', '=', 'pd.int_productoid')
                     ->where('pd.int_pedidoid',$id)
                     ->where('pd.is_deleted',0)
                     ->select('pd.*','p.chr_nombre as nombre_producto')
                     ->get();
    }
}
