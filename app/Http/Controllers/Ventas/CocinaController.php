<?php

namespace App\Http\Controllers\Ventas;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Models\Ventas\Pedido;
use App\Models\Ventas\PedidoDetalle;
use stdClass;


class CocinaController extends Controller
{
    public function listarPedidoCocina(Request $request){
  
        $objectData[] =new stdClass();    

        $ordenes = DB::select("SELECT concat(b.chr_nombre,' ' ,b.chr_apellido) as nombre_trabajador , cab.txt_observaciones,cab.int_estadoid , cab.id FROM tbl_pedido cab, tbl_empleados b  WHERE cab.is_deleted ='0' 
        AND b.id = cab.int_empleadoid 
        AND cab.id in (52,115)
        AND cab.int_estadoid in (3,4)
        ");

        	foreach ($ordenes as $key=>$row){

            $objectData[$key]->id_cabecera[0] = $row->id;
            $objectData[$key]->nombre_trabajador[0] = $row->nombre_trabajador;
            $objectData[$key]->txt_observaciones[0] = $row->txt_observaciones; 
            $objectData[$key]->int_estadoid[0] = $row->int_estadoid;       
            $ordenesDetails = DB::select( 
                "SELECT b.chr_nombre, a.int_cantidad FROM tbl_pedido_detalle a, tbl_producto b WHERE a.int_pedidoid = ".$row->id." AND a.is_active ='0' AND a.is_deleted ='0'  AND b.id = a.int_productoid "
            );

            $objectData[$key]->detalle = $ordenesDetails;
		}

        $dataResponse = array(
            'status' => 'success',
            'code' => 200,
            'resultData' => $objectData
        );
 
        return response()->json($dataResponse,200);

    }

    public function actualizarEnCurso($id){


        Pedido::where('id',$id)->update(array('int_estadoid' => '4'));
        
         
        $data = array(
            'status' => 'success',
            'code' => 200,
            'message' => 'La orden se encuentra en Proceso.'
        );
       

       return response()->json($data,200);
    }
}
