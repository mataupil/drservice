<?php

namespace App\Http\Controllers\Mantenimiento;

use App\Helpers\JwtAuth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Mantenimiento\Rol;
use App\Models\Principal\Empleado;

class RolController extends Controller
{

    public function registrarRol (Request $req){
        $hash = $req->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if($checkToken){
            $this->validate($req, [
                'chr_nombre'        => 'required|max:20|string',
                'chr_descripcion'   => 'required|max:200|string',         
                'int_usercreated'   => 'required|integer',
            ]);

            $checkExistsNombre = Rol::where('chr_nombre', $req->chr_nombre)->count();
            if ($checkExistsNombre >= 1) {
                return response()->json([
                    'success' => false,
                    'message' => 'Este rol ya se encuentra registrado!'
                ], 406);
            }
            $checkExistsUserCreated = Empleado::find($req->int_usercreated);
            if(!$checkExistsUserCreated){
                return response()->json([
                    'success' => false,
                    'message' => 'El usuario que intento hacer esta operación, no existe en el sistema!',
                ], 406);
            }
            
            if($checkExistsUserCreated){
                $time = time();

                $new_rol = Rol::create([
                    'chr_nombre'        => strtoupper($req->chr_nombre),
                    'chr_descripcion'   => strtoupper($req->chr_descripcion), 
                    'int_usercreated'   => $req->int_usercreated,
                    'int_datecreated'   => $time,
                ]);

                return response()->json([
                    'success'   => true,
                    'message'   => 'Usuario registrado exitosamente!',
                    'data'      => $new_rol
                ], 200);
            }
        }else{
            return response()->json([
                'success' => false,
                'message' => "El usuario que intento hacer esta operación no se encuentra logeado en el sistema!"
            ], 406);
        }
    }

    public function listarRoles (Request $req){
        $hash = $req->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if($checkToken){
            $lista_roles = DB::table('tbl_rol')->where('is_active', 1)
                                               ->select('id', 'chr_nombre', 'chr_descripcion')
                                               ->orderBy('id', 'desc')
                                               ->get();
            
            return response()->json([
                'success'   => true,
                'data'      => $lista_roles
            ], 200);
        }else{
            return response()->json([
                'success' => false,
                'message' => "El usuario que intento hacer esta operación no se encuentra logeado en el sistema!"
            ], 406);
        }
    }

    public function buscarRolID($id, Request $req){
        $hash = $req->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if($checkToken){
            $rol = DB::table('tbl_rol')->where('id', $id)
                                       ->where('is_active', 1)
                                       ->select('id', 'chr_nombre', 'chr_descripcion')
                                       ->orderBy('id', 'desc')
                                       ->get();
            
            return response()->json([
                'success'   => true,
                'data'      => $rol
            ], 200);
        }else{
            return response()->json([
                'success' => false,
                'message' => "El usuario que intento hacer esta operación no se encuentra logeado en el sistema!"
            ], 406);
        }
    }

    public function actualizarRol($id, Request $req){
        $hash = $req->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if($checkToken){
            $this->validate($req, [
                'chr_nombre'        => 'required|max:20|string',
                'chr_descripcion'   => 'required|max:200|string',         
                'int_usermodified'   => 'required|integer',
            ]);

            $rol = Rol::find($id);
        
            $checkExistsNombre = Rol::where('chr_nombre', $req->chr_nombre)->count();
            if ($checkExistsNombre >= 1 && $rol->chr_nombre != strtoupper($req->chr_nombre)) {
                return response()->json([
                    'success' => false,
                    'message' => 'Este nombre ya se encuentra registrado!'
                ], 406);
            }
            $checkExistsUserCreated = Empleado::find($req->int_usermodified);
            if(!$checkExistsUserCreated){
                return response()->json([
                    'success' => false,
                    'message' => 'El usuario que intento hacer esta operación, no existe en el sistema!',
                ], 406);
            }
            
            if($checkExistsUserCreated){
                $time = time();

                $rol->update([
                    'chr_nombre'        => strtoupper($req->chr_nombre),
                    'chr_descripcion'   => strtoupper($req->chr_descripcion), 
                    'int_usermodified'   => $req->int_usermodified,
                    'int_datemodified'   => $time,
                ]);

                return response()->json([
                    'success'   => true,
                    'message'   => 'Rol actualizado exitosamente!',
                    'data'      => $rol
                ], 200);
            }
        }else{
            return response()->json([
                'success' => false,
                'message' => "El usuario que intento hacer esta operación no se encuentra logeado en el sistema!"
            ], 406);
        }
    }

    public function eliminarRol($id, Request $req){
        $hash = $req->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if($checkToken){
            $rol = Rol::find($id);
            $rol->update([
                'is_active' => 0,
                'is_deleted' => 1,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Rol eliminado exitosamente!'
            ], 200);
        }else{
            return response()->json([
                'success' => false,
                'message' => "El usuario que intento hacer esta operación no se encuentra logeado en el sistema!"
            ], 406);
        }
    }
}
