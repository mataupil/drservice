<?php

namespace App\Http\Controllers\Mantenimiento;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Mantenimiento\TipoPedido;

class TipoPedidoController extends Controller
{
    public function listarTipoPedido(){   

        $tipoPedido = TipoPedido::where('is_active', '1')->where('is_deleted', '0')->get();
        $data = array(
            'status' => 'success',
            'code' => 200,
            'tipoPedido' => $tipoPedido
        );

        return response()->json($data,200);    
    }

    public function insertarTipoPedido(Request $request){

        $json = $request->input('json',null);
        $params = json_decode($json);
        $chr_nombre = $params->chr_nombre;
        $int_usercreated = $params->int_usercreated;

        $tipoPedido = new TipoPedido();
        $tipoPedido->chr_nombre = $chr_nombre; 
        $tipoPedido->int_usercreated = $int_usercreated;
        $tipoPedido->int_datecreated = time();

        $tipoPedido->save();

        $data = array(
            'status' => 'success',
            'code' => 200,
            'message' => 'Tipo de pedido creado correctamente'
        );
    
    return response()->json($data,200);
    }

    public function listarTipoPedidoById($id){

        $tipoPedido = TipoPedido::find($id);

        $data = array(
            'status' => 'success',
            'code' => 200,
            'tipoPedido' => $tipoPedido
                
        );

        return response()->json($data,200);
    }


    public function actualizarTipoPedido($id, Request $request){

        $json = $request->input('json',null);
        $params = json_decode($json);
        $params_array = json_decode($json,true);


        TipoPedido::where('id',$id)->update($params_array);

        $data = array(
            'status' => 'success',
            'code' => 200,
            'message' => 'Tipo pedido actualizado correctamente'
        );
       

       return response()->json($data,200);
    }

    public function eliminarTipoPedidoById($id, Request $request){


        TipoPedido::where('id',$id)->update(array('is_active' => '0', 'is_deleted' => '1'));
        
         
        $data = array(
            'status' => 'success',
            'code' => 200,
            'message' => 'Tipo pedido eliminado correctamente'
        );
       

       return response()->json($data,200);
    }
}
