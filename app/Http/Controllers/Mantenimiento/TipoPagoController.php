<?php

namespace App\Http\Controllers\Mantenimiento;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Mantenimiento\TipoPago;

class TipoPagoController extends Controller
{

    public function listarTipoPago(){

            $tipoLocal = TipoPago::where('is_active', '1')->where('is_deleted', '0')->get();
            $data = array(
                'status' => 'success',
                'code' => 200,
                'tipoPago' => $tipoLocal
            );
    
            return response()->json($data,200);    
    }

    public function insertarTipoPago(Request $request){

        $json = $request->input('json',null);
        $params = json_decode($json);
        $chr_nombre = $params->chr_nombre;
        $int_usercreated = $params->int_usercreated;

        $tipoLocal = new TipoPago();
        $tipoLocal->chr_nombre = $chr_nombre; 
        $tipoLocal->int_usercreated = $int_usercreated;
        $tipoLocal->int_datecreated = time();

        $tipoLocal->save();

        $data = array(
            'status' => 'success',
            'code' => 200,
            'message' => 'Tipo Pago creado correctamente'
        );
       
       return response()->json($data,200);
    }

    public function listarTipoPagoById($id){

        $tipoPago = TipoPago::find($id);

        $data = array(
            'status' => 'success',
            'code' => 200,
            'tipoPago' => $tipoPago
                
        );

        return response()->json($data,200);
    }

    public function actualizarTipoPago($id, Request $request){

        $json = $request->input('json',null);
        $params = json_decode($json);
        $params_array = json_decode($json,true);


        TipoPago::where('id',$id)->update($params_array);

        $data = array(
            'status' => 'success',
            'code' => 200,
            'message' => 'Tipo pago actualizado correctamente'
        );
       

       return response()->json($data,200);
    }

    public function eliminarTipoPagoById($id, Request $request){


        TipoPago::where('id',$id)->update(array('is_active' => '0', 'is_deleted' => '1'));
        
         
        $data = array(
            'status' => 'success',
            'code' => 200,
            'message' => 'Tipo pago eliminado correctamente'
        );
       

       return response()->json($data,200);
    }

}
