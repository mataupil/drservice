<?php

namespace App\Http\Controllers;

use App\Helpers\JwtAuth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UbigeoController extends Controller
{
    
    public function getDepartments(Request $req){
        $hash = $req->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if($checkToken){
            $departments = DB::table('ubigeo_peru_departments')->select('id', 'name')
                                                               ->orderBy('name', 'ASC')
                                                               ->get();

            return response()->json([
                'success' => true,
                'data' => $departments,
            ], 200);
        }else{
            return response()->json([
                'success' => false,
                'message' => "El usuario que intento hacer esta operación no se encuentra logeado en el sistema!"
            ], 406);
        }
    }

    public function getProvinces($department_id, Request $req){
        $hash = $req->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if($checkToken){
            $provinces = DB::table('ubigeo_peru_provinces')->where('department_id', '=', $department_id)
                                                           ->select('id', 'name')
                                                           ->orderBy('name', 'ASC')
                                                           ->get();

            return response()->json([
                'success' => true,
                'data' => $provinces,
            ], 200);
        }else{
            return response()->json([
                'success' => false,
                'message' => "El usuario que intento hacer esta operación no se encuentra logeado en el sistema!"
            ], 406);
        }
    }

    public function getDistricts($province_id, Request $req){
        $hash = $req->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if($checkToken){
            $districts = DB::table('ubigeo_peru_districts')->where('province_id', '=', $province_id)
                                                           ->select('id', 'name')
                                                           ->orderBy('name', 'ASC')
                                                           ->get();

            return response()->json([
                'success' => true,
                'data' => $districts,
            ], 200);
        }else{
            return response()->json([
                'success' => false,
                'message' => "El usuario que intento hacer esta operación no se encuentra logeado en el sistema!"
            ], 406);
        }
    }
}
