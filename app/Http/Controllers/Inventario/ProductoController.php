<?php

namespace App\Http\Controllers\Inventario;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Inventario\Producto;
use App\Models\Inventario\MateriaPrima;
use App\Models\Inventario\MateriaPrimaProducto;
use DB;

class ProductoController extends Controller
{

    public function listarProducto(){

        $producto = Producto::where('is_active', '1')->where('is_deleted', '0')->get();       
        $materiaPrima = MateriaPrima::all();     

        $data = array(
            'status' => 'success',
            'code' => 200,
            'Producto' => $producto,
            'MateriaPrima' => $materiaPrima
        );

        return response()->json($data,200);

    }


        
    public function insertarProducto(Request $request){

        $json = $request->input('json',null);
        $params = json_decode($json);
        $chr_codproducto = $params->chr_codproducto;
        $chr_nombre = $params->chr_nombre;
        $double_preciounitario = $params->double_preciounitario;
        $int_usercreated = $params->int_usercreated;

        $detalle = $params->detalle;


        $producto = new Producto();
        $producto->chr_codproducto = $chr_codproducto;
        $producto->chr_nombre = $chr_nombre; 
        $producto->double_preciounitario = $double_preciounitario; 
        $producto->int_usercreated = $int_usercreated;
        $producto->int_datecreated = time();

        $producto->save();
        
        foreach ($detalle as $key => $value) {

            $materiaPrimaProducto = new MateriaPrimaProducto();
            $materiaPrimaProducto->chr_codproducto = $chr_codproducto;
            $materiaPrimaProducto->int_ingredienteid = $value->id; 
            $materiaPrimaProducto->int_usercreated = $int_usercreated;
            $materiaPrimaProducto->int_datecreated = time();
            $materiaPrimaProducto->int_idproducto = $producto->id;
            $materiaPrimaProducto->save();
        }

                 
        $data = array(
            'status' => 'success',
            'code' => 200,
            'message' => 'Producto creado correctamente'
        );

        return response()->json($data,200);
    }

    public function obtenerProductoById($id){

        $producto = Producto::find($id);
        $materiaPrimaProducto = DB::select( 
            "select a.* from tbl_ingrediente a,  tbl_ingrediente_producto b
            WHERE a.id = b.int_ingredienteid AND b.int_idproducto =". $id ."           
            "
        );

        


        $data = array(
            'status' => 'success',
            'code' => 200,
            'Producto' => $producto,
            'MateriaPrimaProducto' => $materiaPrimaProducto
        );

        return response()->json($data,200);

    }

    
    public function actualizarProducto($id, Request $request){

        $json = $request->input('json',null);
      
        $params = json_decode($json);

        $detalle = $params->detalle;
        
        $params_array = json_decode($json,true);
        unset($params_array["detalle"]);

        Producto::where('id',$id)->update($params_array);

        $materiaPrimaProducto = MateriaPrimaProducto::where('int_idproducto', $id)->delete();

        $chr_codproducto = $params->chr_codproducto;
        $chr_nombre = $params->chr_nombre;
        $double_preciounitario = $params->double_preciounitario;
        $int_usercreated = $params->int_usercreated;

        $producto = new Producto();
        $producto->chr_codproducto = $chr_codproducto;
        $producto->chr_nombre = $chr_nombre; 
        $producto->double_preciounitario = $double_preciounitario; 
        $producto->int_usercreated = $int_usercreated;
        $producto->int_datecreated = time();


        foreach ($detalle as $key => $value) {

            $materiaPrimaProducto = new MateriaPrimaProducto();
            $materiaPrimaProducto->chr_codproducto = $chr_codproducto;
            $materiaPrimaProducto->int_ingredienteid = $value->id; 
            $materiaPrimaProducto->int_usercreated = $int_usercreated;
            $materiaPrimaProducto->int_datecreated = time();
            $materiaPrimaProducto->int_idproducto = $id;
            $materiaPrimaProducto->save();
        }


        $data = array(
            'status' => 'success',
            'code' => 200,
            'message' => $params
        );
       
       return response()->json($data,200);
    }



}
