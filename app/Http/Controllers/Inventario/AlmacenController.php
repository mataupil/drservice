<?php

namespace App\Http\Controllers\Inventario;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Inventario\Almacen;
use DB;
use App\Models\Principal\Local;

class AlmacenController extends Controller
{
    public function listarAlmacen(){

        $almacen = DB::select( 
            "select a.*,b.chr_nombre as localNombre from tbl_almacen a, tbl_locales b
            where  a.int_localid = b.id " );

        $local = local::where('is_active', '1')->where('is_deleted', '0')->get();

            
        $data = array(
            'status' => 'success',
            'code' => 200,
            'Almacen' => $almacen,
            'cboLocales' => $local
        );

        return response()->json($data,200);

    }
 
    public function insertarAlmacen(Request $request){

        $json = $request->input('json',null);
        $params = json_decode($json);
        $chr_code = $params->chr_code;
        $chr_nombre = $params->chr_nombre;
        $chr_descripcion = $params->chr_descripcion;
        $int_localid = $params->int_localid;
        $int_usercreated = $params->int_usercreated;


        $almacen = new Almacen();
        $almacen->chr_code = $chr_code;
        $almacen->chr_nombre = $chr_nombre; 
        $almacen->chr_descripcion = $chr_descripcion; 
        $almacen->int_localid = $int_localid;
        $almacen->int_usercreated = $int_usercreated;
        $almacen->int_datecreated = time();

        $almacen->save();

                 
        $data = array(
            'status' => 'success',
            'code' => 200,
            'message' => 'Almacen creado correctamente'
        );

        return response()->json($data,200);
    }

    public function obtenerAlmacenById($id){


        $almacen = Almacen::find($id);

        $data = array(
            'status' => 'success',
            'code' => 200,
            'Almacen' => $almacen
        );

        return response()->json($data,200);

    }

    public function actualizarAlmacen($id, Request $request){

        $json = $request->input('json',null);
        $params = json_decode($json);
        $params_array = json_decode($json,true);

        Almacen::where('id',$id)->update($params_array);

        $data = array(
            'status' => 'success',
            'code' => 200,
            'message' => 'Almacen actualizado correctamente'
        );
       
       return response()->json($data,200);
    }
    
}
