<?php

namespace App\Http\Controllers\Inventario;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Inventario\MateriaPrima;
use App\Models\Inventario\MateriaPrimaLocal;
use App\Models\Principal\Local;
use DB;
class MateriaPrimaController extends Controller
{
    public function listarMateriaPrima(){

        $materiaPrima = MateriaPrima::all();
        $data = array(
            'status' => 'success',
            'code' => 200,
            'MateriaPrima' => $materiaPrima
        );

        return response()->json($data,200);

    }

    public function insertarMateriaPrima(Request $request){

        $json = $request->input('json',null);
        $params = json_decode($json);
        $chr_codingrediente = $params->chr_codingrediente;
        $chr_marca = $params->chr_marca;
        $chr_nombre = $params->chr_nombre;
        $chr_unidadmedida = $params->chr_unidadmedida;
        $int_usercreated = $params->int_usercreated;


        $materiaPrima = new MateriaPrima();
        $materiaPrima->chr_codingrediente = $chr_codingrediente;
        $materiaPrima->chr_marca = $chr_marca; 
        $materiaPrima->chr_nombre = $chr_nombre; 
        $materiaPrima->chr_unidadmedida = $chr_unidadmedida;
        $materiaPrima->int_usercreated = $int_usercreated;
        $materiaPrima->int_datecreated = time();

        $materiaPrima->save();

                 
        $data = array(
            'status' => 'success',
            'code' => 200,
            'message' => 'Materia prima creada correctamente'
        );

        return response()->json($data,200);
    }

    public function obtenerMateriaPrimaLocalById($id){


        $materiaPrima = MateriaPrima::find($id);
                                    
        $materiaPrimaLocalAsignad = DB::select( 
             "select a.* from tbl_locales a
              where a.id  in (select b.int_local from tbl_ingrediente_local b where b.int_ingredienteid = ". $id .")" );

       // $direccionCliente = DireccionCliente::where('int_empleado', $id)->where('is_assigned', '1')->get();

       $materiaPrimaLocalNotAsignad = DB::select( 
           "select a.* from tbl_locales a
           where a.id not in (select b.int_local from tbl_ingrediente_local b where b.int_ingredienteid = ". $id .")           
           "
       );


        $data = array(
            'status' => 'success',
            'code' => 200,
            'MateriaPrima' => $materiaPrima,
            'materiaPrimaLocalAsignad' => $materiaPrimaLocalAsignad,
            'materiaPrimaLocalNotAsignad' => $materiaPrimaLocalNotAsignad
        );

        return response()->json($data,200);

    }

    public function actualizarMateriaPrima($id, Request $request){

        $json = $request->input('json',null);
        $params = json_decode($json);
        $params_array = json_decode($json,true);

        MateriaPrima::where('id',$id)->update($params_array);

        $data = array(
            'status' => 'success',
            'code' => 200,
            'message' => 'MateriaPrima actualizada correctamente'
        );
       
       return response()->json($data,200);
    }

    public function obtenerMateriaPrimaById($id){

        $materiaPrima = MateriaPrima::find($id);


        $data = array(
            'status' => 'success',
            'code' => 200,
            'MateriaPrima' => $materiaPrima
                
        );

        return response()->json($data,200);

    }

    
    public function agregarMateriaPrimaLocal(Request $request){

        $json = $request->input('json',null);
        $params = json_decode($json);

       // $chr_codlocal = $params->chr_codlocal;
        $int_local = $params->int_local;
        $int_ingredienteid = $params->int_ingredienteid;
        $int_usercreated = $params->int_usercreated;
       // $is_assigned = $params->is_assigned;

        $local = Local::where('id', $int_local)->first();
            
        $materiaPrimaLocal = new MateriaPrimaLocal();
       // $materiaPrimaLocal->chr_codlocal = $chr_codlocal;
        $materiaPrimaLocal->int_local = $int_local;
        $materiaPrimaLocal->int_ingredienteid = $int_ingredienteid;
        $materiaPrimaLocal->int_usercreated = $int_usercreated;
        $materiaPrimaLocal->int_datecreated = time();

        $materiaPrimaLocal->save();

        $data = array(
            'status' => 'success',
            'code' => 200,
            'message' => 'Materia Prima Local creado correctamente'
        );
       

       return response()->json($data,200);
    }


    public function quitarMateriaPrimaLocalById($idLocal,$idMateriaPrima, Request $request){


        $materiaPrimaLocal = MateriaPrimaLocal::where('int_local', $idLocal)->where('int_ingredienteid', $idMateriaPrima)->first();

        $respuesta = $materiaPrimaLocal->delete();
        $message = "";
        if($respuesta == true){
            $message = "Se designo local a la materia prima correstamente";
        }else{
            $message = "Error al designar local a la materia prima";
        }

         
        $data = array(
            'status' => 'success',
            'code' => 200,
            'message' => $message
        );
       

       return response()->json($data,200);
    }
}
