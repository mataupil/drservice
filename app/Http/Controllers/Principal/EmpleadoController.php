<?php

namespace App\Http\Controllers\Principal;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Principal\Empleado;
use App\Models\Principal\EmpleadoLocal;
use App\Models\Principal\Local;
use DB;

class EmpleadoController extends Controller
{
    const EMPLEADO_ROL_MOZO = 4;
    public function listarEmpleado(){

         $empleado = empleado::select('*')->where('is_active', '1')->where('is_deleted', '0')->get();
         $data = array(
             'status' => 'success',
             'code' => 200,
             'Empleado' => $empleado
         );
 
         return response()->json($data,200);
 
     }

    public function listarMozos(){
        $empleado = empleado::select('*')->where('is_active', '1')->where('is_deleted', '0')->where('int_rolid',self::EMPLEADO_ROL_MOZO)->get();
        $data = array(
            'status' => 'success',
            'code' => 200,
            'Mozos' => $empleado
        );
 
         return response()->json($data,200);
    }

     public function insertarEmpleado(Request $request){

        $json = $request->input('json',null);
        $params = json_decode($json);
        $chr_codempleado = $params->chr_codempleado;
        $chr_nombre = $params->chr_nombre;
        $chr_apellido = $params->chr_apellido;
        $chr_celular = $params->chr_celular;
        $chr_telefono = $params->chr_telefono;
        $int_usercreated = $params->int_usercreated;
        $chr_direccion = $params->chr_direccion;
        $chr_documento = $params->chr_documento;
        $chr_correo = $params->chr_correo;
        $int_local = $params->int_local;
        $chr_local_nombre = $params->chr_local_nombre;

        $empleado = new Empleado();
        $empleado->chr_codempleado = $chr_codempleado;
        $empleado->chr_nombre = $chr_nombre; 
        $empleado->chr_apellido = $chr_apellido; 
        $empleado->chr_telefono = $chr_telefono;
        $empleado->chr_celular = $chr_celular;
        $empleado->int_usercreated = $int_usercreated;
        $empleado->int_datecreated = time();
        $empleado->chr_direccion = $chr_direccion;
        $empleado->chr_documento = $chr_documento;
        $empleado->chr_correo = $chr_correo;
        // $empleado->int_local = $int_local;
        // $empleado->chr_local_nombre = $chr_local_nombre;
        $empleado->chr_password = $chr_documento;

        $empleado->save();

        $data = array(
            'status' => 'success',
            'code' => 200,
            'message' => 'Empleado creado correctamente'
        );
       
       return response()->json($data,200);
    }

    public function listarEmpleadoById($id){

        $empleado = Empleado::find($id);


        $data = array(
            'status' => 'success',
            'code' => 200,
            'Empleado' => $empleado
                
        );

        return response()->json($data,200);

    }

    public function actualizarEmpleado($id, Request $request){

        $json = $request->input('json',null);
        $params = json_decode($json);
        $params_array = json_decode($json,true);

        Empleado::where('id',$id)->update($params_array);

        $data = array(
            'status' => 'success',
            'code' => 200,
            'message' => 'Empleado actualizado correctamente'
        );
       
       return response()->json($data,200);
    }

    public function listarEmpleadoLocalByid($id){


        $Empleado = Empleado::find($id);
                                    
        $empleadoLocalAsignad = DB::select( 
             "select a.* from tbl_locales a
              where a.id  in (select b.int_local from tbl_empleados_local b where b.int_empleado = ". $id .")" );

       // $direccionCliente = DireccionCliente::where('int_empleado', $id)->where('is_assigned', '1')->get();

       $empleadoLocalNotAsignad = DB::select( 
           "select a.* from tbl_locales a
           where a.id not in (select b.int_local from tbl_empleados_local b where b.int_empleado = ". $id .")           
           "
       );


        $data = array(
            'status' => 'success',
            'code' => 200,
            'Empleado' => $Empleado,
            'empleadoLocalAsignad' => $empleadoLocalAsignad,
            'empleadoLocalNotAsignad' => $empleadoLocalNotAsignad
        );

        return response()->json($data,200);

    }

    public function agregarEmpleadoLocal(Request $request){

        $json = $request->input('json',null);
        $params = json_decode($json);

        $int_empleado = $params->int_empleado;
        $chr_codempleado = $params->chr_codempleado;
        $int_local = $params->int_local;
        $int_usercreated = $params->int_usercreated;
       // $is_assigned = $params->is_assigned;

        $local = Local::where('id', $int_local)->first();
            
        $empleadoLocal = new EmpleadoLocal();
        $empleadoLocal->int_empleado = $int_empleado;
        $empleadoLocal->int_local = $int_local;
        $empleadoLocal->chr_codempleado = $chr_codempleado;
        $empleadoLocal->chr_codlocal = $local->chr_codlocal;
        $empleadoLocal->int_usercreated = $int_usercreated;
        $empleadoLocal->int_datecreated = time();
        //$empleadoLocal->is_assigned = $is_assigned;

        $empleadoLocal->save();

        $data = array(
            'status' => 'success',
            'code' => 200,
            'message' => 'Empleado Local creado correctamente'
        );
       

       return response()->json($data,200);
    }


    public function quitarEmpleadoLocalById($idLocal,$idEmpleado, Request $request){


        $empleadoLocal = EmpleadoLocal::where('int_local', $idLocal)->where('int_empleado', $idEmpleado)->first();

        $respuesta = $empleadoLocal->delete();
        $message = "";
        if($respuesta == true){
            $message = "Se designo local al cliente correstamente";
        }else{
            $message = "Error al designar local del cliente";
        }

         
        $data = array(
            'status' => 'success',
            'code' => 200,
            'message' => $message
        );
       

       return response()->json($data,200);
    }
}
