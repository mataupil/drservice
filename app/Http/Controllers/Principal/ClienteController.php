<?php

namespace App\Http\Controllers\Principal;

use App\Http\Requests;
use App\Helpers\JwtAuth;

use Illuminate\Http\Request;

use App\Models\Principal\Cliente;
use App\Models\Principal\Empleado;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Principal\DireccionCliente;
use App\Models\Principal\Empresa;
use App\Models\Principal\Persona;

class ClienteController extends Controller
{
    const CLIENTE_BORRADOR = 9;
    const CLIENTE_REGISTRADO = 10;
    const DIRECCION_BORRADOR = 11;
    const DIRECCION_REGISTRADO = 12;
    const NATURAL = 1;
    const JURIDICO = 2;

    // customer
    
    public function registrarCliente(Request $req){
        $hash = $req->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if($checkToken){
            $time = time();
            $cliente_register = '';
            $lista_direcciones = [];
            $message = '';
            $success = false;

            if($req->int_tipo){
                $cliente = CLiente::find($req->id);

                $cliente->update([
                    'chr_celular'       => $req->chr_celular ? $req->chr_celular : null,
                    'chr_telefono'      => $req->chr_telefono ? $req->chr_telefono : null,
                    'chr_correo'        => $req->chr_correo ? strtoupper($req->chr_correo) : null,
                    'int_tipo'          => $req->int_tipo,
                    'int_mst_estado'    => self::CLIENTE_REGISTRADO,
                    'int_datecreated'   => $time,
                    'date_fecha_inicio' => $req->date_fecha_inicio ? $req->date_fecha_inicio: null,
                    'date_fecha_fin'    => $req->date_fecha_fin ? $req->date_fecha_fin: null,
                    'is_subscriptor'    => $req->is_subscriptor ? $req->is_subscriptor : 0,
                    'chr_ruc'           => $req->chr_ruc ? $req->chr_ruc : null,
                ]);

                if($req->int_tipo == self::NATURAL){
                    if($req->chr_dni){
                        $checkExistsDNI = Persona::where('chr_dni', $req->chr_dni)->count();
                        if ($checkExistsDNI >= 1) {
                            return response()->json([
                                'success' => false,
                                'message' => "Este DNI ya se encuentra registrado!"
                            ], 406);
                        }
                    }

                    $persona = Persona::create([
                        'chr_dni'           => $req->chr_dni ? $req->chr_dni : null,
                        'chr_ruc'           => $req->chr_ruc ? $req->chr_ruc : null,
                        'chr_nombre'        => strtoupper($req->chr_nombre),
                        'chr_apellido'      => $req->chr_apellido ? strtoupper($req->chr_apellido) : null,
                    ]);

                    $cliente->update([
                        'int_id_persona' => $persona->id,
                    ]);

                    $cliente_register = DB::table('tbl_clientes as cli')->join('tbl_personas as per', 'per.id', '=', 'cli.int_id_persona')
                                                                        ->join('tbl_tipo_cliente as tc', 'tc.id', '=', 'cli.int_tipo')
                                                                        ->where('cli.is_active', 1)
                                                                        ->where('per.id', $persona->id)
                                                                        ->select('cli.id', 'per.chr_nombre', 'per.chr_apellido', 'per.chr_dni', 'cli.chr_celular', 'cli.chr_correo', 'tc.chr_nombre as tipo')
                                                                        ->first();
                }

                if($req->int_tipo == self::JURIDICO){
                    if($req->chr_ruc){
                        $checkExistsRUC = Empresa::where('chr_ruc', $req->chr_ruc)->count();
                        if ($checkExistsRUC >= 1) {
                            return response()->json([
                                'success' => false,
                                'message' => "Este RUC ya se encuentra registrado!"
                            ], 406);
                        }
                    }

                    $empresa = Empresa::create([
                        'chr_ruc'               => $req->chr_ruc,
                        'chr_razon_social'      => strtoupper($req->chr_razon_social),
                        'chr_nombre_comercial'  => $req->chr_nombre_comercial ? strtoupper($req->chr_nombre_comercial) : null,
                    ]);

                    $cliente->update([
                        'int_id_empresa' => $empresa->id,
                    ]);

                    $cliente_register = DB::table('tbl_clientes as cli')->join('tbl_empresas as em', 'em.id', '=', 'cli.int_id_empresa')
                                                                        ->join('tbl_tipo_cliente as tc', 'tc.id', '=', 'cli.int_tipo')
                                                                        ->where('cli.is_active', 1)
                                                                        ->where('em.id', $empresa->id)
                                                                        ->select('cli.id', 'em.chr_razon_social', 'em.chr_nombre_comercial', 'em.chr_ruc', 'cli.chr_celular', 'cli.chr_correo', 'tc.chr_nombre as tipo')
                                                                        ->first();
                }    

                $lista_direcciones = DireccionCliente::where('int_clienteid', $cliente->id)
                                                     ->get();
    
                if(count($lista_direcciones) > 0){
                    foreach ($lista_direcciones as $valor) {
                        $direccion = DireccionCliente::find($valor->id);
                        $direccion->update([
                            'int_mst_estado' => self::DIRECCION_REGISTRADO,
                        ]);
                    }
                }    
            }

            if($cliente_register){
                $message = 'Cliente registrado existosamente!';
                $success = true;
            }else{
                $message = 'Error, el cliente no fue registrado!';
                $success = false;
            }

            return response()->json([
                'success'   => $success,
                'message'   => $message,
                'data'      => [
                    'cliente'           => $cliente_register,
                    'lista_direcciones' => $lista_direcciones,
                ]
            ], 200);
        }else{
            return response()->json([
                'success' => false,
                'message' => "El usuario que intento hacer esta operación no se encuentra logeado en el sistema!"
            ], 406);
        }    
    }

    public function buscarClienteID($id, $id_user_auth = null, Request $req){
        $hash = $req->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if($checkToken){
            if($id == 0){                   
                $time = time();

                $cliente = Cliente::create([
                    'int_usercreated'   => $id_user_auth,
                    'int_datecreated'   => $time,
                ]);

                return response()->json([
                    'success'   => true,
                    'message'   => 'Cliente registrado exitosamente!',
                    'data'      => [
                        'cliente'           => $cliente,
                        'lista_direcciones' => [],
                    ],
                ], 200);
            }else{

                $cliente = Cliente::find($id);

                if($cliente->int_tipo == self::NATURAL){
                    $cliente_query = DB::table('tbl_clientes as cli')->join('tbl_personas as per', 'per.id', '=', 'cli.int_id_persona')
                                                                     ->join('tbl_tipo_cliente as tc', 'tc.id', '=', 'cli.int_tipo')
                                                                     ->where('cli.is_active', 1)
                                                                     ->where('cli.id', $id)
                                                                     ->select('cli.id as id_cliente', 'per.chr_nombre', 'per.chr_apellido', 'per.chr_dni', 'per.chr_ruc', 'cli.chr_celular', 'cli.chr_correo', 'cli.date_fecha_inicio', 'cli.date_fecha_fin', 'cli.is_subscriptor', 'cli.chr_telefono', 'tc.chr_nombre as tipo', 'tc.id as id_tipo')
                                                                     ->first();
                                                                     
                }

                if($cliente->int_tipo == self::JURIDICO){
                    $cliente_query = DB::table('tbl_clientes as cli')->join('tbl_empresas as em', 'em.id', '=', 'cli.int_id_empresa')
                                                                     ->join('tbl_tipo_cliente as tc', 'tc.id', '=', 'cli.int_tipo')
                                                                     ->where('cli.is_active', 1)
                                                                     ->where('cli.id', $id)
                                                                     ->select('cli.id as id_cliente', 'em.chr_razon_social', 'em.chr_nombre_comercial', 'em.chr_ruc', 'cli.chr_celular', 'cli.chr_correo', 'cli.date_fecha_inicio', 'cli.date_fecha_fin', 'cli.is_subscriptor', 'cli.chr_telefono', 'tc.chr_nombre as tipo', 'tc.id as id_tipo')
                                                                     ->first();
                }
                
                $lista_direcciones = DB::table('tbl_direccion_cliente')->where('int_clienteid', $id)
                                                                       ->where('is_active', 1)
                                                                       ->select('id', 'chr_direccion', 'is_default')
                                                                       ->get();

                return response()->json([
                    'success'   => true,
                    'data'      => [
                        'cliente'           => $cliente_query,
                        'lista_direcciones' => $lista_direcciones
                    ]
                ], 200);
            }  
        }else{
            return response()->json([
                'success' => false,
                'message' => "El usuario que intento hacer esta operación no se encuentra logeado en el sistema!"
            ], 406);
        }
    }
    
    public function buscarCliente($row_by_page, Request $req){
        $hash = $req->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if($checkToken){

            $lista_clientes = [];

            if($req->type_customer == self::NATURAL){

                switch($req->type_search){
                    case 1: // RUC
                        $lista_clientes = DB::table('tbl_clientes as cli')->join('tbl_personas as pe', 'pe.id', '=', 'cli.int_id_persona')
                                                                          ->where('cli.is_active', 1)
                                                                          ->where('cli.int_mst_estado', self::CLIENTE_REGISTRADO)
                                                                          ->where('pe.chr_ruc', 'LIKE', "%$req->filter_value%")
                                                                          ->select('cli.id as id_cliente', 'cli.chr_correo', 'pe.chr_nombre', 'pe.chr_apellido', 'pe.chr_dni', 'pe.chr_ruc')
                                                                          ->orderBy('cli.id', 'desc')
                                                                          ->paginate($row_by_page);
                        break;
                    case 2: // DNI
                        $lista_clientes = DB::table('tbl_clientes as cli')->join('tbl_personas as pe', 'pe.id', '=', 'cli.int_id_persona')
                                                                          ->where('cli.is_active', 1)
                                                                          ->where('cli.int_mst_estado', self::CLIENTE_REGISTRADO)
                                                                          ->where('pe.chr_dni', 'LIKE', "%$req->filter_value%")
                                                                          ->select('cli.id as id_cliente', 'cli.chr_correo', 'pe.chr_nombre', 'pe.chr_apellido', 'pe.chr_dni', 'pe.chr_ruc')
                                                                          ->orderBy('cli.id', 'desc')
                                                                          ->paginate($row_by_page);
                        break;
                    case 3: // NOMBRE Y APELLIDO
                        $lista_clientes = DB::table('tbl_clientes as cli')->join('tbl_personas as pe', 'pe.id', '=', 'cli.int_id_persona')
                                                                          ->where('cli.is_active', 1)
                                                                          ->where('cli.int_mst_estado', self::CLIENTE_REGISTRADO)
                                                                          ->where(DB::raw("CONCAT_WS(' ', pe.chr_nombre, pe.chr_apellido)"), 'LIKE', "%$req->filter_value%")
                                                                          ->orWhere(DB::raw("CONCAT_WS(' ', pe.chr_apellido, pe.chr_nombre)"), 'LIKE', "%$req->filter_value%")
                                                                          ->select('cli.id as id_cliente', 'cli.chr_correo', 'pe.chr_nombre', 'pe.chr_apellido', 'pe.chr_dni', 'pe.chr_ruc')
                                                                          ->orderBy('cli.id', 'desc')
                                                                          ->paginate($row_by_page);
                        break;
                    
                    default:
                        $lista_clientes = DB::table('tbl_clientes as cli')->join('tbl_personas as pe', 'pe.id', '=', 'cli.int_id_persona')
                                                                          ->where('cli.is_active', 1)
                                                                          ->where('cli.int_mst_estado', self::CLIENTE_REGISTRADO)
                                                                          ->select('cli.id as id_cliente', 'cli.chr_correo', 'pe.chr_nombre', 'pe.chr_apellido', 'pe.chr_dni', 'pe.chr_ruc')
                                                                          ->orderBy('cli.id', 'desc')
                                                                          ->paginate($row_by_page);
                }
            }
            if($req->type_customer == self::JURIDICO){
                switch($req->type_search){
                    case 1: // RUC
                        $lista_clientes = DB::table('tbl_clientes as cli')->join('tbl_empresas as em', 'em.id', '=', 'cli.int_id_empresa')
                                                                          ->where('cli.is_active', 1)
                                                                          ->where('cli.int_mst_estado', self::CLIENTE_REGISTRADO)
                                                                          ->where('em.chr_ruc', 'LIKE', "%$req->filter_value%")
                                                                          ->select('cli.id', 'cli.chr_correo', 'em.chr_ruc', 'em.chr_razon_social', 'em.chr_nombre_comercial')
                                                                          ->orderBy('cli.id', 'desc')
                                                                          ->paginate($row_by_page);
                        break;
                    case 2: // RAZON SOCIAL
                        $lista_clientes = DB::table('tbl_clientes as cli')->join('tbl_empresas as em', 'em.id', '=', 'cli.int_id_empresa')
                                                                          ->where('cli.is_active', 1)
                                                                          ->where('cli.int_mst_estado', self::CLIENTE_REGISTRADO)
                                                                          ->where('em.chr_razon_social', 'LIKE', "%$req->filter_value%")
                                                                          ->select('cli.id as id_cliente', 'cli.chr_correo', 'em.chr_ruc', 'em.chr_razon_social', 'em.chr_nombre_comercial')
                                                                          ->orderBy('cli.id', 'desc')
                                                                          ->paginate($row_by_page);
                        break;
                    case 3: // NOMBRE COMERCIAL
                        $lista_clientes = DB::table('tbl_clientes as cli')->join('tbl_empresas as em', 'em.id', '=', 'cli.int_id_empresa')
                                                                          ->where('cli.is_active', 1)
                                                                          ->where('cli.int_mst_estado', self::CLIENTE_REGISTRADO)
                                                                          ->where('em.chr_nombre_comercial', 'LIKE', "%$req->filter_value%")
                                                                          ->select('cli.id as id_cliente', 'cli.chr_correo', 'em.chr_ruc', 'em.chr_razon_social', 'em.chr_nombre_comercial')
                                                                          ->orderBy('cli.id', 'desc')
                                                                          ->paginate($row_by_page);
                        break;
                    default:
                        $lista_clientes = DB::table('tbl_clientes as cli')->join('tbl_empresas as em', 'em.id', '=', 'cli.int_id_empresa')
                                                                          ->where('cli.is_active', 1)
                                                                          ->where('cli.int_mst_estado', self::CLIENTE_REGISTRADO)
                                                                          ->select('cli.id as id_cliente', 'cli.chr_correo', 'em.chr_ruc', 'em.chr_razon_social', 'em.chr_nombre_comercial')
                                                                          ->orderBy('cli.id', 'desc')
                                                                          ->paginate($row_by_page);
                }
            }

            return response()->json([
                'success'   => true,
                'data'      => $lista_clientes,
            ], 200);
          
        }else{
            return response()->json([
                'success' => false,
                'message' => "El usuario que intento hacer esta operación no se encuentra logeado en el sistema!"
            ], 406);
        }
    }
    
    public function actualizarCliente($id, Request $req){
        $hash = $req->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);
       
        if($checkToken){
            $this->validate($req, [
                'chr_nombre'            => 'max:80',
                'chr_apellido'          => 'max:80',
                'chr_razon_social'      => 'max:80',
                'chr_nomber_comercial'  => 'max:80',
                'chr_dni'               => 'max:8',
                'chr_celular'           => 'required|min:9|max:9|string',
                'chr_telefono'          => 'max:9',
                'chr_correo'            => 'max:80',
                'chr_ruc'               => 'max:11',
                'is_subscriptor'        => 'required|integer',
                'date_fecha_inicio'     => '',
                'date_fecha_fin'        => '',
                'int_usermodified'      => 'required|integer',
                'id_tipo'               => 'required|integer',
            ]);

            $cliente = Cliente::find($id);
           
            $persona = Persona::find($cliente->int_id_persona);

            if($req->chr_dni){
                $checkExistsDNI = Persona::where('chr_dni', $req->chr_dni)->count();
                if ($checkExistsDNI >= 1 && $persona->chr_dni != $req->chr_dni) {
                    return response()->json([
                        'success' => false,
                        'message' => "Este DNI ya se encuentra registrado!"
                    ], 406);
                }
            }
            if($req->chr_ruc){
                $checkExistsRUC = Cliente::where('chr_ruc', $req->chr_ruc)->count();
                if ($checkExistsRUC >= 1 && $cliente->chr_ruc != $req->chr_ruc) {
                    return response()->json([
                        'success' => false,
                        'message' => "Este RUC ya se encuentra registrado!"
                    ], 406);
                }
            }
            $checkExistsUserModified = Empleado::find($req->int_usermodified);
            if(!$checkExistsUserModified){
                return response()->json([
                    'success' => false,
                    'message' => 'El usuario que intento hacer esta operación, no existe en el sistema!',
                ], 406);
            }
            if($checkExistsUserModified){
                $time = time();
                $change_one = 0;
                $change_two = 0;
                $cliente_update = '';
                $message = '';
                $success = false;

                $change_one = $cliente->update([
                    'chr_celular'       => $req->chr_celular ? trim($req->chr_celular) : null,
                    'chr_telefono'      => $req->chr_telefono ? trim($req->chr_telefono) : null,
                    'chr_correo'        => $req->chr_correo ? trim(strtoupper($req->chr_correo)) : null,
                    'is_subscriptor'    => $req->is_subscriptor,
                    'date_fecha_inicio' => $req->date_fecha_inicio ? trim($req->date_fecha_inicio) : null,
                    'date_fecha_fin'    => $req->date_fecha_fin ? trim($req->date_fecha_fin) : null,
                    'int_usermodified'  => $req->int_usermodified,
                    'int_datemodified'  => $time,
                    'chr_ruc'           => $req->chr_ruc ? trim($req->chr_ruc) : null
                ]);

                if($req->id_tipo == self::NATURAL){
                    $persona = Persona::find($cliente->int_id_persona);

                    $change_two = $persona->update([
                        'chr_dni'       => trim($req->chr_dni), 
                        'chr_nombre'    => trim(strtoupper($req->chr_nombre)),
                        'chr_apellido'  => $req->chr_apellido ? trim(strtoupper($req->chr_apellido)) :  null,
                        'chr_ruc'       => $req->chr_ruc ? trim($req->chr_ruc) : null
                    ]);
                    

                    $cliente_update = DB::table('tbl_clientes as cli')->join('tbl_personas as per', 'per.id', '=', 'cli.int_id_persona')
                                                                      ->join('tbl_tipo_cliente as tc', 'tc.id', '=', 'cli.int_tipo')
                                                                      ->where('cli.is_active', 1)
                                                                      ->where('per.id', $persona->id)
                                                                      ->select('cli.id as id_cliente', 'per.chr_nombre', 'per.chr_apellido', 'per.chr_dni', 'cli.chr_ruc', 'cli.chr_celular', 'cli.chr_correo', 'cli.chr_telefono', 'cli.is_subscriptor', 'cli.date_fecha_inicio', 'cli.date_fecha_fin', 'tc.chr_nombre as tipo', 'tc.id as id_tipo')
                                                                      ->first();
                }

                if($req->id_tipo == self::JURIDICO){
                    $empresa = Empresa::find($cliente->int_id_empresa);

                    $change_two = $empresa->update([
                        'chr_razon_social'      => strtoupper($req->chr_razon_social),
                        'chr_nombre_comercial'  => $req->chr_nombre_comercial ? strtoupper($req->chr_nombre_comercial) :  null,
                        'chr_ruc'               => $req->chr_ruc ? $req->chr_ruc : null
                    ]);

                    $cliente_update = DB::table('tbl_clientes as cli')->join('tbl_empresas as em', 'em.id', '=', 'cli.int_id_empresa')
                                                                      ->join('tbl_tipo_cliente as tc', 'tc.id', '=', 'cli.int_tipo')
                                                                      ->where('cli.is_active', 1)
                                                                      ->where('em.id', $empresa->id)
                                                                      ->select('cli.id as id_cliente', 'em.chr_razon_social', 'em.chr_nombre_comercial', 'em.chr_ruc', 'cli.chr_celular', 'cli.chr_correo', 'cli.chr_telefono', 'cli.is_subscriptor', 'cli.date_fecha_inicio', 'cli.date_fecha_fin', 'tc.chr_nombre as tipo', 'tc.id as id_tipo')
                                                                      ->first();
                }

                $lista_direcciones = DireccionCliente::where('int_clienteid', $cliente->id)
                                                     ->get();

                if(count($lista_direcciones) > 0){
                    foreach ($lista_direcciones as $valor) {
                        $direccion = DireccionCliente::find($valor->id);
                        $direccion->update([
                            'int_mst_estado' => self::DIRECCION_REGISTRADO,
                        ]);
                    }
                }    

                if($change_one == 1 && $change_two == 1 && $cliente_update){
                    $message = 'Cliente actualizado exitosamente!';
                    $success = true;
                }else{
                    $message = 'Error, el cliente no se ectualizo correctamente!';
                    $success = false;
                }

                return response()->json([
                    'success'   => $success,
                    'message'   => $message,
                    'data'      => $cliente_update,
                ], 200);
            }
        }else{
            return response()->json([
                'success' => false,
                'message' => "El usuario que intento hacer esta operación no se encuentra logeado en el sistema!"
            ], 406);
        }
    }

    public function eliminarCliente($id, Request $req){
        $hash = $req->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if($checkToken){
            $cliente = Cliente::find($id);
            $cliente->update([
                'is_active'     => 0,
                'is_deleted'    => 1,
            ]);

            $lista_direcciones = DireccionCliente::where('int_clienteid', $cliente->id)
                                                 ->where('is_active', 1)
                                                 ->get();

            foreach ($lista_direcciones as $value) {
                $direccion = DireccionCliente::find($value->id);
                $direccion->update([
                    'is_active' => 0,
                    'is_delete' => 1,
                ]);
            }
    
            return response()->json([
                'success' => true,
                'message' => 'Cliente eliminado exitosamente!'
            ], 200);
        }else{
            return response()->json([
                'success' => false,
                'message' => "El usuario que intento hacer esta operación no se encuentra logeado en el sistema!"
            ], 406);
        }  
    }

    // address

    public function registrarDireccion(Request $req){
        $hash = $req->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if($checkToken){
            $this->validate($req, [
                'id_cliente'        => 'required|integer',
                'chr_direccion'     => 'required|max:150|string',
                'is_default'        => 'required|integer',
                'int_usercreated'   => 'required|integer',
            ]);
            
            $checkExistsUserCreated = Empleado::find($req->int_usercreated);
            if(!$checkExistsUserCreated){
                return response()->json([
                    'success' => false,
                    'message' => 'El usuario que intento hacer esta operación, no existe en el sistema!',
                ], 406);
            }
            if($checkExistsUserCreated){
                $time = time();

                $cliente = Cliente::find($req->id_cliente);

                if($cliente){
                    $direccion = DireccionCliente::create([
                        'int_clienteid'     => $req->id_cliente,
                        'chr_direccion'     => strtoupper($req->chr_direccion),
                        'chr_referencia'    => $req->chr_referencia ? strtoupper($req->chr_referencia) : null,
                        'is_default'        => $req->is_default,
                        'int_usercreated'   => $req->int_usercreated,
                        'int_datecreated'   => $time,
                        'int_mst_estado'    => self::DIRECCION_BORRADOR,
                    ]);
    
                    if($direccion->is_default == 1){
                        $lista_direcciones = DB::table('tbl_direccion_cliente')->where('int_clienteid',  $req->id_cliente)
                                                                               ->where('id', '<>', $direccion->id)
                                                                               ->where('is_active', 1)
                                                                               ->get();
    
                        foreach ($lista_direcciones as $valor) {
                            $d = DireccionCliente::find($valor->id);
                            $d->update([
                                'is_default' => 0
                            ]);
                        }
                    }

                    $direccion = DB::table('tbl_direccion_cliente')->where('id',  $direccion->id)
                                                                   ->select('id', 'int_clienteid as id_cliente', 'chr_direccion', 'chr_referencia', 'is_default')
                                                                   ->first();

                    return response()->json([
                        'success'   => true,
                        'message'   => 'Direccion registrada exitosamente!',
                        'data'      => $direccion,
                    ], 200);

                }else{
                    return response()->json([
                        'success'   => false,
                        'message'   => 'Al cliente que intento asignar una dirección no existe en el sistema!',
                    ], 400);
                }
            }
        }else{
            return response()->json([
                'success' => false,
                'message' => "El usuario que intento hacer esta operación no se encuentra logeado en el sistema!"
            ], 406);
        }
    }

    public function listarDirecciones($id_cli, Request $req){
        $hash = $req->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if($checkToken){
            $lista_direcciones = DB::table('tbl_direccion_cliente as dc')->join('tbl_mst_estado as mst', 'mst.id', '=', 'dc.int_mst_estado')
                                                                         ->where('dc.int_clienteid', $id_cli)
                                                                         ->where('dc.is_active', 1)
                                                                         ->select('dc.id', 'dc.int_clienteid as id_cliente', 'dc.chr_direccion', 'dc.chr_referencia','dc.is_default', 'mst.chr_descripcion as status')
                                                                         ->orderBy('dc.id', 'desc')
                                                                         ->get();

            return response()->json([
                'success'   => true,
                'data'      => $lista_direcciones,
            ], 200);
            
        }else{
            return response()->json([
                'success'   => false,
                'message'   => "El usuario que intento hacer esta operación no se encuentra logeado en el sistema!"
            ], 406);
        }
    }

    public function buscarDireccionID($id, Request $req){
        $hash = $req->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if($checkToken){
            $direccion = DB::table('tbl_direccion_cliente')->where('id', $id)
                                                           ->where('is_active', 1)
                                                           ->select('id', 'chr_direccion', 'chr_referencia')
                                                           ->first();

            return response()->json([
                'success'   => true,
                'data'      => $direccion,
            ], 200);
        }else{
            return response()->json([
                'success'   => false,
                'message'   => "El usuario que intento hacer esta operación no se encuentra logeado en el sistema!"
            ], 406);
        }
    }

    public function actualizarDireccion($id, Request $req){
        $hash = $req->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if($checkToken){
            $this->validate($req, [
                'chr_direccion' => 'required|max:150|string',
            ]);

            $direccion_cliente = DireccionCliente::find($id);

            $direccion_cliente->update([
               'chr_direccion'  => strtoupper($req->chr_direccion),
               'chr_referencia' => $req->chr_referencia ? strtoupper($req->chr_referencia) : null,
            ]);

            // if($direccion_cliente->is_default == 1){
            //     $lista_direcciones = DB::table('tbl_direccion_cliente')->where('int_clienteid',  $id_cli)
            //                                                            ->where('id', '<>', $direccion_cliente->id)
            //                                                            ->where('is_active', 1)
            //                                                            ->get();

            //     foreach ($lista_direcciones as $valor) {
            //         $d = DireccionCliente::find($valor->id);
            //         $d->update([
            //             'is_default' => 0
            //         ]);
            //     }
            // }

            return response()->json([
                'success'   => true,
                'message'   => 'Direccion actualizada exitosamente',
                'data'      => $direccion_cliente,
            ], 200);
        }else{
            return response()->json([
                'success'   => false,
                'message'   => "El usuario que intento hacer esta operación no se encuentra logeado en el sistema!"
            ], 406);
        }
    }

    public function eliminarDireccion($id, $id_cli, Request $req){
        $hash = $req->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if($checkToken){
    
            $direccion = DireccionCliente::find($id);

            $direccion->update([
               'is_active'  => 0,
               'is_delete'  => 1,
               'is_default' => 0,
            ]);

            $dir = DireccionCliente::latest('int_datecreated')
                                   ->where('int_clienteid', $id_cli)
                                   ->where('is_active', 1)
                                   ->first();

            if($dir){
                $dir->update([
                    'is_default' => 1,
                ]);
            }

            return response()->json([
                'success'   => true,
                'message'   => 'Direccion eliminada exitosamente',
            ], 200);
        }else{
            return response()->json([
                'success'   => false,
                'message'   => "El usuario que intento hacer esta operación no se encuentra logeado en el sistema!"
            ], 406);
        }
    }

    public function setDireccionDefault(Request $req){
        $hash = $req->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if($checkToken){
            $this->validate($req, [
                'id'            => 'required',
                'is_default'    => 'required',
                'id_customer'   => 'required',
            ]);
            $message = '';

            if($req->is_default == 1){
                $lista_direcciones = DB::table('tbl_direccion_cliente')->where('int_clienteid', $req->id_customer)
                                                                       ->where('is_active', 1)
                                                                       ->get();
                
                if($lista_direcciones){
                    foreach ($lista_direcciones as $valor){
                        $dir = DireccionCliente::find($valor->id);
                        $dir->update([
                            'is_default' => 0,
                        ]);
                    }
                }
                $message = 'Direccion seleccionada!';
            }

            if($req->is_default == 0){
                $dir = DireccionCliente::latest('int_datecreated')
                                       ->where('int_clienteid', $req->id_customer)
                                       ->where('id', '<>', $req->id)
                                       ->where('is_active', 1)
                                       ->first();
                if($dir){
                    $dir->update([
                        'is_default' => 1,
                    ]);
                }   
                $message = 'Direccion desseleccionada!';
            }

            $direccion = DireccionCliente::find($req->id);
            $direccion->update([
                'is_default' => $req->is_default
            ]);

            return response()->json([
                'success'   => true,
                'message'   => $message,
            ], 200);
        }else{
            return response()->json([
                'success'   => false,
                'message'   => "El usuario que intento hacer esta operación no se encuentra logeado en el sistema!"
            ], 406);
        }
    }
}
