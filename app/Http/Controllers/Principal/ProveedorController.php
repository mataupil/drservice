<?php

namespace App\Http\Controllers\Principal;

use App\Helpers\JwtAuth;
use Illuminate\Http\Request;
use App\Models\Principal\ContactoProveedor;
use App\Models\Principal\Empleado;
use Illuminate\Support\Facades\DB;
use App\Models\Principal\Proveedor;
use App\Http\Controllers\Controller;

class ProveedorController extends Controller
{

    public function registrarProveedor(Request $req){
        $hash = $req->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if($checkToken){
            $this->validate($req, [
                'int_tipo'          => 'required|integer',
                'int_usercreated'   => 'required|integer',
            ]);

            if($req->chr_dni){
                $checkExistsDNI = Proveedor::where('chr_dni', $req->chr_dni)->count();
                if ($checkExistsDNI >= 1) {
                    return response()->json([
                        'success' => false,
                        'message' => 'Este DNI ya se encuentra registrado!'
                    ], 406);
                }
            }
            if($req->chr_ruc){
                $checkExistsRUC = Proveedor::where('chr_ruc', $req->chr_ruc)->count();
                if ($checkExistsRUC >= 1) {
                    return response()->json([
                        'success' => false,
                        'message' => 'Este RUC ya se encuentra registrado!'
                    ], 406);
                }
            }
            $checkExistsUserCreated = Empleado::find($req->int_usercreated);
            if(!$checkExistsUserCreated){
                return response()->json([
                    'success' => false,
                    'message' => 'El usuario que intento hacer esta operación, no existe en el sistema!',
                ], 406);
            }
            if($checkExistsUserCreated){
                $time = time();

                $proveedor = Proveedor::create([
                    'int_tipo'              => $req->int_tipo, 
                    'chr_dni'               => $req->chr_dni,
                    'chr_ruc'               => $req->chr_ruc,
                    'chr_telefono'          => $req->chr_telefono,
                    'chr_celular'           => $req->chr_celular,
                    'chr_telefono2'         => $req->chr_telefono2,
                    'chr_razonsocial'       => $req->chr_razonsocial ? strtoupper($req->chr_razonsocial) : null,
                    'chr_nombrecomercial'   => $req->chr_nombrecomercial ? strtoupper($req->chr_nombrecomercial) : null,
                    'chr_nombre'            => $req->chr_nombre ? strtoupper($req->chr_nombre) : null,
                    'chr_apellido'          => $req->chr_apellido ? strtoupper($req->chr_apellido) : null,
                    'int_usercreated'       => $req->int_usercreated,
                    'int_datecreated'       => $time,
                    'chr_direccion'         => $req->chr_direccion ? strtoupper($req->chr_direccion) : null,
                ]);
        
                return response()->json([
                    'success'   => true,
                    'message'   => 'Proveedor registrado exitosamente!',
                    'data'      => $proveedor,
                ], 200);
            }
        }else{
            return response()->json([
                'success' => false,
                'message' => "El usuario que intento hacer esta operación no se encuentra logeado en el sistema!"
            ], 406);
        }
    }

    public function listarProveedores(Request $req){
        $hash = $req->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if($checkToken){
            $lista_proveedores = DB::table('tbl_proveedores')->where('is_active', 1)
                                                             ->select('id', 'int_tipo', 'chr_dni', 'chr_ruc', 'chr_telefono', 'chr_celular', 'chr_telefono2', 'chr_razonsocial', 'chr_nombrecomercial', 'chr_nombre', 'chr_apellido', 'chr_direccion')
                                                             ->orderBy('id', 'desc')
                                                             ->get();
            foreach ($lista_proveedores as $valor) {
                if(!$valor->chr_dni){
                    $valor->chr_dni = '--';
                }
                if(!$valor->chr_ruc){
                    $valor->chr_ruc = '--';
                }
                if(!$valor->chr_telefono){
                    $valor->chr_telefono = '--';
                }
                if(!$valor->chr_celular){
                    $valor->chr_celular = '--';
                }
                if(!$valor->chr_telefono2){
                    $valor->chr_telefono2 = '--';
                }
                if(!$valor->chr_razonsocial){
                    $valor->chr_razonsocial = '--';
                }
                if(!$valor->chr_nombrecomercial){
                    $valor->chr_nombrecomercial = '--';
                }
                if(!$valor->chr_nombre){
                    $valor->chr_nombre = '--';
                }
                if(!$valor->chr_apellido){
                    $valor->chr_apellido = '--';
                }
                if(!$valor->chr_direccion){
                    $valor->chr_direccion = '--';
                }
            }

            return response()->json([
                'success'   => true,
                'data'      => $lista_proveedores,
            ], 200);
        }else{
            return response()->json([
                'success' => false,
                'message' => "El usuario que intento hacer esta operación no se encuentra logeado en el sistema!"
            ], 406);
        }
    }
  
    public function buscarProveedorID($id, Request $req){
        $hash = $req->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if($checkToken){
            $proveedor = DB::table('tbl_proveedores')->where('id', $id)
                                                     ->where('is_active', 1)
                                                     ->select('id', 'int_tipo', 'chr_dni', 'chr_ruc', 'chr_telefono', 'chr_celular', 'chr_telefono2', 'chr_razonsocial', 'chr_nombrecomercial', 'chr_nombre', 'chr_apellido', 'chr_direccion')
                                                     ->first();

            $lista_contactos = DB::table('tbl_contacto_proveedor')->where('int_proveedor', $proveedor->id)
                                                                  ->where('is_active', 1)
                                                                  ->select('id', 'chr_nombre', 'chr_descripcion', 'chr_telefono', 'chr_celular')
                                                                  ->orderBy('id', 'desc')
                                                                  ->get();
            
            return response()->json([
                'success'   => true,
                'data'      => [
                    'proveedor'         => $proveedor,
                    'lista_contactos'   => $lista_contactos
                ]
            ], 200);
          
        }else{
            return response()->json([
                'success' => false,
                'message' => "El usuario que intento hacer esta operación no se encuentra logeado en el sistema!"
            ], 406);
        }
    }

    public function actualizarProveedor($id, Request $req){
        $hash = $req->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if($checkToken){
            $this->validate($req, [
                'int_tipo'      => 'required|integer',
                'int_usermodified'  => 'required|integer',
            ]);

            $proveedor = Proveedor::find($id);

            if($req->chr_dni){
                $checkExistsDNI = Proveedor::where('chr_dni', $req->chr_dni)->count();
                if ($checkExistsDNI >= 1 && $proveedor->chr_dni != $req->chr_dni) {
                    return response()->json([
                        'success' => false,
                        'message' => 'Este DNI ya se encuentra registrado!'
                    ], 406);
                }
            }
            if($req->chr_ruc){
                $checkExistsRUC = Proveedor::where('chr_ruc', $req->chr_ruc)->count();
                if ($checkExistsRUC >= 1 && $proveedor->chr_ruc != $req->chr_ruc) {
                    return response()->json([
                        'success' => false,
                        'message' => 'Este RUC ya se encuentra registrado!'
                    ], 406);
                }
            }
            $checkExistsUserModified = Empleado::find($req->int_usermodified);
            if(!$checkExistsUserModified){
                return response()->json([
                    'success' => false,
                    'message' => 'El usuario que intento hacer esta operación, no existe en el sistema!',
                ], 406);
            }
            if($checkExistsUserModified){
                $time = time();

                $proveedor->update([
                    'int_tipo'              => $req->int_tipo, 
                    'chr_dni'               => $req->chr_dni,
                    'chr_ruc'               => $req->chr_ruc,
                    'chr_telefono'          => $req->chr_telefono,
                    'chr_celular'           => $req->chr_celular,
                    'chr_telefono2'         => $req->chr_telefono2,
                    'chr_razonsocial'       => $req->chr_razonsocial ? strtoupper($req->chr_razonsocial) : null,
                    'chr_nombrecomercial'   => $req->chr_nombrecomercial ? strtoupper($req->chr_nombrecomercial) : null,
                    'chr_nombre'            => $req->chr_nombre ? strtoupper($req->chr_nombre) : null,
                    'chr_apellido'          => $req->chr_apellido ? strtoupper($req->chr_apellido) : null,
                    'int_usermodified'      => $req->int_usermodified,
                    'int_datemodified'      => $time,
                    'chr_direccion'         => $req->chr_direccion ? strtoupper($req->chr_direccion) : null,
                ]);
        
                return response()->json([
                    'success'   => true,
                    'message'   => 'Proveedor actualizado exitosamente!',
                    'data'      => $proveedor,
                ], 200);
            }
        }else{
            return response()->json([
                'success' => false,
                'message' => "El usuario que intento hacer esta operación no se encuentra logeado en el sistema!"
            ], 406);
        }
    }
    
    public function eliminarProveedor($id, Request $req){
        $hash = $req->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if($checkToken){
            $proveedor = Proveedor::find($id);

            $proveedor->update([
                'is_active'  => 0,
                'is_deleted' => 1,
            ]);

            return response()->json([
                'success'   => true,
                'message'   => 'Proveedor eliminado exitosamente!',
            ], 200);
            
        }else{
            return response()->json([
                'success' => false,
                'message' => "El usuario que intento hacer esta operación no se encuentra logeado en el sistema!"
            ], 406);
        }
    }

    public function registrarContacto($id_pro, Request $req){
        $hash = $req->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if($checkToken){
            $this->validate($req, [
                'chr_nombre'        => 'required|max:35|string',
                'int_usercreated'   => 'required|integer'
            ]);

            $checkExistsUserCreated = Empleado::find($req->int_usercreated);
            if(!$checkExistsUserCreated){
                return response()->json([
                    'success' => false,
                    'message' => 'El usuario que intento hacer esta operación, no existe en el sistema!',
                ], 406);
            }
            if($checkExistsUserCreated){
                $time = time();

                $contacto = ContactoProveedor::create([
                    'int_proveedor'     => $id_pro,
                    'chr_nombre'        => $req->chr_nombre,
                    'chr_descripcion'   => $req->chr_descripcion ? strtoupper($req->chr_descripcion) : null,
                    'chr_telefono'      => $req->chr_telefono,
                    'chr_celular'       => $req->chr_celular,
                    'int_usercreated'   => $req->int_usercreated,
                    'int_datecreated'   => $time,
                ]);
        
                return response()->json([
                    'success'   => true,
                    'message'   => 'Contacto registrado exitosamente!',
                    'data'      => $contacto,
                ], 200);
            }
        }else{
            return response()->json([
                'success' => false,
                'message' => "El usuario que intento hacer esta operación no se encuentra logeado en el sistema!"
            ], 406);
        }
    }

    public function listarContactos($id_pro, Request $req){
        $hash = $req->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if($checkToken){
            $lista_contactos = DB::table('tbl_contacto_proveedor')->where('int_proveedor', $id_pro)
                                                                  ->where('is_active', 1)
                                                                  ->select('id', 'chr_nombre', 'chr_descripcion', 'chr_telefono', 'chr_celular')
                                                                  ->orderBy('id', 'desc')
                                                                  ->get();
                                                                    
            foreach ($lista_contactos as $valor) {
                if(!$valor->chr_descripcion){
                    $valor->chr_ruc = '--';
                }
                if(!$valor->chr_telefono){
                    $valor->chr_telefono = '--';
                }
                if(!$valor->chr_celular){
                    $valor->chr_celular = '--';
                }
            }

            return response()->json([
                'success'   => true,
                'data'      => $lista_contactos,
            ], 200);
        }else{
            return response()->json([
                'success' => false,
                'message' => "El usuario que intento hacer esta operación no se encuentra logeado en el sistema!"
            ], 406);
        }
    }

    public function buscarContactoID($id, Request $req){
        $hash = $req->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if($checkToken){
            $contacto = DB::table('tbl_contacto_proveedor')->where('id', $id)
                                                           ->where('is_active', 1)
                                                           ->select('id', 'chr_nombre', 'chr_descripcion', 'chr_telefono', 'chr_celular')
                                                           ->first();
            
            return response()->json([
                'success'   => true,
                'data'      => [
                    'contacto' => $contacto,
                ]
            ], 200);
          
        }else{
            return response()->json([
                'success' => false,
                'message' => "El usuario que intento hacer esta operación no se encuentra logeado en el sistema!"
            ], 406);
        }
    }

    public function actualizarContacto($id, Request $req){
        $hash = $req->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if($checkToken){
            $this->validate($req, [
                'chr_nombre'        => 'required|max:35|string',
                'int_usermodified'  => 'required|integer'
            ]);

            $contacto = ContactoProveedor::find($id);

            $checkExistsUserModified = Empleado::find($req->int_usermodified);
            if(!$checkExistsUserModified){
                return response()->json([
                    'success' => false,
                    'message' => 'El usuario que intento hacer esta operación, no existe en el sistema!',
                ], 406);
            }
            if($checkExistsUserModified){
                $time = time();

                $contacto->update([
                    'chr_nombre'        => $req->chr_nombre,
                    'chr_descripcion'   => $req->chr_descripcion ? strtoupper($req->chr_descripcion) : null,
                    'chr_telefono'      => $req->chr_telefono,
                    'chr_celular'       => $req->chr_celular,
                    'int_usermodified'  => $req->int_usermodified,
                    'int_datemodified'  => $time,
                ]);
            
                return response()->json([
                    'success'   => true,
                    'message'   => 'Contacto actualizado exitosamente!',
                    'data'      => $contacto,
                ], 200);
            }
        }else{
            return response()->json([
                'success' => false,
                'message' => "El usuario que intento hacer esta operación no se encuentra logeado en el sistema!"
            ], 406);
        }
    }

    public function eliminarContacto($id, Request $req){
        $hash = $req->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if($checkToken){
            $contacto = ContactoProveedor::find($id);

            $contacto->update([
                'is_active'  => 0,
                'is_deleted' => 1,
            ]);

            return response()->json([
                'success'   => true,
                'message'   => 'Contacto eliminado exitosamente!',
            ], 200);
            
        }else{
            return response()->json([
                'success' => false,
                'message' => "El usuario que intento hacer esta operación no se encuentra logeado en el sistema!"
            ], 406);
        }
    }

    public function listarProveedor(){

       // $proveedor = Proveedor::all();
        $proveedor = proveedor::where('is_active', '1')->where('is_deleted', '0')->get();
        $data = array(
            'status' => 'success',
            'code' => 200,
            'Proveedor' => $proveedor
        );

        return response()->json($data,200);

    }

    public function insertarProveedor(Request $request){

        $json = $request->input('json',null);
        $params = json_decode($json);
    
        $int_tipo = $params->int_tipo;

        if($int_tipo == '1'){

            $chr_ruc = $params->chr_ruc;
            $chr_telefono = $params->chr_telefono;
            $chr_celular = $params->chr_celular;
            $chr_telefono2 = $params->chr_telefono2;
            $chr_telefono = $params->chr_telefono;
            $chr_razonsocial = $params->chr_razonsocial;
            $chr_nombrecomercial = $params->chr_nombrecomercial;
            $chr_direccion = $params->chr_direccion;
            $int_usercreated = $params->int_usercreated;

            $proveedor = new Proveedor();
            $proveedor->int_tipo = $int_tipo;
            $proveedor->chr_ruc = $chr_ruc; 
            $proveedor->chr_telefono = $chr_telefono;
            $proveedor->chr_celular = $chr_celular;
            $proveedor->chr_telefono2 = $chr_telefono2;
            $proveedor->chr_razonsocial = $chr_razonsocial;
            $proveedor->chr_nombrecomercial = $chr_nombrecomercial;
            $proveedor->chr_direccion = $chr_direccion;
            $proveedor->int_datecreated = time();
            $proveedor->int_usercreated = $int_usercreated;


        }else if($int_tipo == '2'){

            $chr_documento = $params->chr_documento;
            $chr_telefono = $params->chr_telefono;
            $chr_celular = $params->chr_celular;
            $chr_nombre = $params->chr_nombre;
            $chr_apellido = $params->chr_apellido;
            $chr_direccion = $params->chr_direccion;
            $int_usercreated = $params->int_usercreated;

            $proveedor = new Proveedor();
            $proveedor->int_tipo = $int_tipo;
            $proveedor->chr_documento = $chr_documento;
            $proveedor->chr_telefono = $chr_telefono; 
            $proveedor->chr_celular = $chr_celular;
            $proveedor->chr_nombre = $chr_nombre;
            $proveedor->chr_apellido = $chr_apellido;
            $proveedor->chr_direccion = $chr_direccion;
            $proveedor->int_datecreated = time();
            $proveedor->int_usercreated = $int_usercreated;
            $proveedor->chr_razonsocial = $chr_nombre.' '.$chr_apellido;
            $proveedor->chr_ruc = $chr_documento; 


        }

    
        $proveedor->save();
                
    
        $data = array(
                'status' => 'success',
                'code' => 200,
                'message' => 'Proveedor creado correctamente'
        );
           
    
        return response()->json($data,200);

    }

    public function listarProveedorById($id){

        $proveedor = Proveedor::find($id);   

        $data = array(
            'status' => 'success',
            'code' => 200,
            'Proveedor' => $proveedor
        );

        return response()->json($data,200);

    }

    public function actualizarProveedor2($id, Request $request){

        $json = $request->input('json',null);
        $params = json_decode($json);
        $params_array = json_decode($json,true);


        Proveedor::where('id',$id)->update($params_array);

        $data = array(
            'status' => 'success',
            'code' => 200,
            'message' => 'Proveedor actualizado correctamente'
        );
        
        
        return response()->json($data,200);
    }

    public function eliminarProveedorById($id, Request $request){

        Proveedor::where('id',$id)->update(array('is_active' => '0', 'is_deleted' => '1'));
        
            
        $data = array(
            'status' => 'success',
            'code' => 200,
            'message' => 'Proveedor eliminado correctamente'
        );
        

        return response()->json($data,200);
    }

}
