<?php

namespace App\Http\Controllers\Principal;

use App\Helpers\JwtAuth;
use Illuminate\Http\Request;
use App\Models\Principal\Local;
use App\Models\Principal\Empleado;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class LocalController extends Controller
{
    
    public function registrarLocal(Request $req){
        $hash = $req->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if($checkToken){
            $this->validate($req, [
                'chr_nombre'        => 'required|max:40|string',
                'chr_direccion'     => 'required|max:80|string',
                'chr_telefono'      => 'required||integer',
                'int_usercreated'   => 'required|integer',
            ]);
        
            $checkExistsUserCreated = Empleado::find($req->int_usercreated);
            if(!$checkExistsUserCreated){
                return response()->json([
                    'success' => false,
                    'message' => 'El usuario que intento hacer esta operación, no existe en el sistema!',
                ], 406);
            }
            if($checkExistsUserCreated){
                $time = time();

                $chr_codlocal = '';
                $ultimo_local_registrado = Local::latest('id')->first();

                if($ultimo_local_registrado){
                    $lenght = 3;
                    $num_string = substr($ultimo_local_registrado->chr_codlocal, -3);
                    $num_increment = intval($num_string) + 1;
                    $num_ok = str_pad($num_increment, $lenght, "0", STR_PAD_LEFT);
                    $chr_codlocal = 'L' . $num_ok;
                }else{
                    $chr_codlocal = 'L001';
                }

                $local = Local::create([
                    'chr_codlocal'      => $chr_codlocal,
                    'chr_nombre'        => strtoupper($req->chr_nombre),
                    'chr_direccion'     => strtoupper($req->chr_direccion),
                    'chr_telefono'      => $req->chr_telefono,
                    'int_latitud'       => $req->int_latitud,
                    'int_longitud'      => $req->int_longitud,
                    'int_usercreated'   => $req->int_usercreated,
                    'int_datecreated'   => $time,
                ]);

                return response()->json([
                    'success'   => true,
                    'message'   => 'Local registrado exitosamente!',
                    'data'      => $local,
                ], 200);
            }
        }else{
            return response()->json([
                'success' => false,
                'message' => "El usuario que intento hacer esta operación no se encuentra logeado en el sistema!"
            ], 406);
        }
    }

    public function listarLocales(Request $req){
        $hash = $req->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if($checkToken){
            $lista_locales = DB::table('tbl_locales')->where('is_active', 1)
                                                     ->select('id',  'chr_codlocal', 'chr_nombre', 'chr_direccion', 'chr_telefono')
                                                     ->orderBy('id', 'desc')
                                                     ->get();

            return response()->json([
                'success'   => true,
                'data'      => $lista_locales
            ], 200);
        }else{
            return response()->json([
                'success' => false,
                'message' => "El usuario que intento hacer esta operación no se encuentra logeado en el sistema!"
            ], 406);
        }
    }

    public function buscarLocalID($id, Request $req){
        $hash = $req->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if($checkToken){
            $local = DB::table('tbl_locales')->where('id', $id)
                                             ->where('is_active', 1)
                                             ->select('id', 'chr_codlocal', 'chr_nombre', 'chr_direccion', 'chr_telefono', 'int_latitud', 'int_longitud')
                                             ->first();

            return response()->json([
                'success'   => true,
                'data'      => $local,
            ], 200);
        }else{
            return response()->json([
                'success' => false,
                'message' => "El usuario que intento hacer esta operación no se encuentra logeado en el sistema!"
            ], 406);
        }
    }
    
    public function actualizarLocal($id, Request $req){
        $hash = $req->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if($checkToken){
            $this->validate($req, [
                'chr_nombre'        => 'required|max:40|string',
                'chr_direccion'     => 'required|max:80|string',
                'chr_telefono'      => 'required||integer',
                'int_usermodified'  => 'required|integer',
            ]);
        
            $checkExistsUserModifiedd = Empleado::find($req->int_usermodified);
            if(!$checkExistsUserModifiedd){
                return response()->json([
                    'success' => false,
                    'message' => 'El usuario que intento hacer esta operación, no existe en el sistema!',
                ], 406);
            }
            if($checkExistsUserModifiedd){
                $time = time();

                $local = Local::find($id);

                $local->update([
                    'chr_nombre'        => strtoupper($req->chr_nombre),
                    'chr_direccion'     => strtoupper($req->chr_direccion),
                    'chr_telefono'      => $req->chr_telefono,
                    'int_latitud'       => $req->int_latitud,
                    'int_longitud'      => $req->int_longitud,
                    'int_usermodified'  => $req->int_usermodified,
                    'int_datemodified'   => $time,
                ]);

                return response()->json([
                    'success'   => true,
                    'message'   => 'Local atualizado exitosamente!',
                    'data'      => $local,
                ], 200);
            }
        }else{
            return response()->json([
                'success' => false,
                'message' => "El usuario que intento hacer esta operación no se encuentra logeado en el sistema!"
            ], 406);
        }
    }

    public function eliminarLocal($id, Request $req){
        $hash = $req->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if($checkToken){
            $local = Local::find($id);
            $local->update([
                'is_active'     => 0,
                'is_deleted'    => 1,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Local eliminado exitosamente!'
            ], 200);
        }else{
            return response()->json([
                'success' => false,
                'message' => "El usuario que intento hacer esta operación no se encuentra logeado en el sistema!"
            ], 406);
        }  
    }

    public function listarLocal(){

        $local = local::where('is_active', '1')->where('is_deleted', '0')->get();
        $data = array(
            'status' => 'success',
            'code' => 200,
            'Local' => $local
        );

        return response()->json($data,200);

    }

    public function insertarLocal(Request $request){

        $json = $request->input('json',null);
        $params = json_decode($json);

        $chr_codlocal = $params->chr_codlocal;
        $chr_nombre = $params->chr_nombre;
        $chr_telefono = $params->chr_telefono;
        $int_usercreated = $params->int_usercreated;
        $chr_direccion = $params->chr_direccion;

        $local = new Local();
        $local->chr_codlocal = $chr_codlocal;
        $local->chr_nombre = $chr_nombre; 
        $local->chr_telefono = $chr_telefono;
        $local->int_usercreated = $int_usercreated;
        $local->int_datecreated = time();
        $local->chr_direccion = $chr_direccion;

        $local->save();
            
        $data = array(
            'status' => 'success',
            'code' => 200,
            'message' => 'Local creado correctamente'
        );
       

       return response()->json($data,200);
    }

    public function listarLocalById($id){

        $local = Local::find($id);

        $data = array(
            'status' => 'success',
            'code' => 200,
            'Local' => $local
                
        );

        return response()->json($data,200);
    }

    public function actualizarLocal2($id, Request $request){

        $json = $request->input('json',null);
        $params = json_decode($json);
        $params_array = json_decode($json,true);


         Local::where('id',$id)->update($params_array);

        $data = array(
            'status' => 'success',
            'code' => 200,
            'message' => 'Local actualizado correctamente'
        );
       

       return response()->json($data,200);
    }
}
