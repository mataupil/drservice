<?php

namespace App\Http\Controllers\Security;

use stdClass;
use App\Helpers\JwtAuth;
use App\Helpers\ImgBase64;
use App\Helpers\StrUtf8;
use App\Models\Security\Rol;
use Illuminate\Http\Request;
use App\Models\Security\Menu;
use App\Models\Principal\Local;
use App\Models\Security\RoleMenu;
use App\Models\Principal\Empleado;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use App\Models\Principal\EmpleadoLocal;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    const MENU_LEVEL_PADRE = 1;
    const MENU_LEVEL_HIJO = 2;

    public function login(Request $req){

        $this->validate($req, [
            'chr_codempleado'   => 'required',
            'chr_password'      => 'required',
            'int_local'         => 'required'
        ]);

        $empleadoExists = Empleado::where('chr_codempleado',$req->chr_codempleado)
                                  ->where('is_active',1)
                                  ->first();

        if($empleadoExists){
            
            if($empleadoExists->chr_password == hash('sha256', $req->chr_password)){
                $empleadoLocalExists = DB::table('tbl_empleados_local as el')->join('tbl_locales as lo','lo.id','=','el.int_local')
                                                                             ->where('el.int_empleado',$empleadoExists->id)
                                                                             ->where('el.int_local',$req->int_local)
                                                                             ->select('el.id','el.int_rolid','el.int_local','el.int_empleado','lo.chr_nombre as local','lo.chr_direccion as direccion')
                                                                             ->first();
                if($empleadoLocalExists){
                    //obtenemos el menu mediante el rol
                    $menu = $this->getMenuByRol($empleadoLocalExists->int_rolid);
                    //obtenemos el rol mediante el id
                    $rolExists = Rol::find($empleadoLocalExists->int_rolid);
                    if (is_object($rolExists)) {
                        $objAcceso = new stdClass(); // instanciamos el objeto de acceso

                        $objAcceso->id              = $empleadoExists->id;
                        $objAcceso->id_local        = $empleadoLocalExists->int_local;
                        $objAcceso->nombre_local    = $empleadoLocalExists->local;
                        $objAcceso->nombre_user     = $empleadoExists->chr_nombre." ".$empleadoExists->chr_apellido;
                        $objAcceso->usuario         = $empleadoExists->chr_codempleado;
                        $objAcceso->id_cargo        = $rolExists->id;
                        $objAcceso->nombre_cargo    = $rolExists->chr_nombre;
                        $objAcceso->menu            = $menu;
                        $objAcceso->token           = "";

                        $jwtAuth = new JwtAuth();
                        $pwd = hash('sha256', $req->chr_password);

                        if(!$req->getToken){
                            $objAcceso->token = $jwtAuth->signup($req->chr_codempleado, $pwd);
                        }else if($req->getToken){
                            $objAcceso->token = $jwtAuth->signup($req->chr_codempleado, $pwd, $req->getToken);
                        }else{
                            return response()->json([
                                'success'   => false,
                                'message'   => 'Error Login',
                            ], 200); 
                        }

                        return response()->json([
                            'success'   => true,
                            'message'   => 'Usuario logeado exitosamente!',
                            'data'      => $objAcceso,
                        ], 200); 
                    }else{
                        return response()->json([
                            'success' => false,
                            'message' => 'Este usuario no cuenta con un rol asignado, por favor, contacte con un administrador!'
                        ], 406);
                    }
                }else{
                    return response()->json([
                        'success' => false,
                        'message' => 'Este usuario no cuenta con acceso a este local!'
                    ], 406);
                }
            }else{
                return response()->json([
                    'success' => false,
                    'message' => 'La contraseña es incorrecta!'
                ], 406);
            }
        }else{
            return response()->json([
                'success' => false,
                'message' => 'Este usuario no existe!'
            ], 406);
        }
    }

    public function registrarUsuario(Request $req){

        $hash = $req->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if($checkToken){
            $this->validate($req, [
                'chr_nombre'        => 'required|max:80|string',
                'chr_apellido'      => 'required|max:80|string',
                'chr_celular'       => 'required|max:9|min:9|string',
                'chr_direccion'     => 'required|max:80|string',
                'chr_documento'     => 'required|max:8|string',
                'chr_correo'        => 'required|email',
                'int_usercreated'   => 'required|integer',
                'int_rolid'         => 'required|integer',
                'ubigeo_id'         => 'required',
                'list_id_local'     => 'required',
            ]);

            $checkExistsCelular = Empleado::where('chr_celular', $req->chr_celular)->count();
            if ($checkExistsCelular >= 1) {
                return response()->json([
                    'success' => false,
                    'message' => 'Este celular ya se encuentra registrado!'
                ], 406);
            }
            $checkExistsDocument = Empleado::where('chr_documento', $req->chr_documento)->count();
            if ($checkExistsDocument >= 1) {
                return response()->json([
                    'success' => false,
                    'message' => "Este número de documento ya se encuentra registrado!"
                ], 406);
            }
            $checkExistsEmail = Empleado::where('chr_correo', $req->chr_correo)->count();
            if ($checkExistsEmail >= 1) {
                return response()->json([
                    'success' => false,
                    'message' => "Este correo electrónico ya se encuentra registrado!"
                ], 406);
            }
            $checkExistsUserCreated = Empleado::find($req->int_usercreated);
            if(!$checkExistsUserCreated){
                return response()->json([
                    'success' => false,
                    'message' => 'El usuario que intento hacer esta operación, no existe en el sistema!',
                ], 406);
            }
            if($checkExistsUserCreated){
                $time = time();

                $code1 = strtoupper(substr($req->chr_nombre, 0, 2));
                $code2 = strtoupper(substr($req->chr_apellido, 0, 2));
                $code3 = strtoupper(substr($req->chr_documento, 0, 2));

                $imgBase64 = new ImgBase64();
                $img_data = $imgBase64->getB64Image($req->file_photo);  // get data image
                $img_extension = $imgBase64->getB64Extension($req->file_photo); // get extension imagen
                $img_name = time() .  $req->chr_documento . '.' . $img_extension; // set name random for image
                Storage::disk('public')->put('images/' . $img_name, $img_data);  // save image in disk storage
                
                $new_empleado = Empleado::create([
                    'chr_codempleado'   => $code1 . $code2 . $code3, 
                    'chr_nombre'        => strtoupper($req->chr_nombre),
                    'chr_apellido'      => strtoupper($req->chr_apellido),
                    'chr_telefono'      => $req->chr_telefono,
                    'chr_celular'       => $req->chr_celular,
                    'chr_direccion'     => strtoupper($req->chr_direccion),
                    'chr_documento'     => strtoupper($req->chr_documento),
                    'chr_correo'        => strtoupper($req->chr_correo),
                    'chr_password'      => hash('sha256', $req->chr_documento),
                    'int_usercreated'   => $req->int_usercreated,
                    'int_datecreated'   => $time,
                    'chr_urlfoto'       => '/storage/images/' . $img_name,
                    'int_rolid'         => $req->int_rolid,
                    'ubigeo_id'         => $req->ubigeo_id,
                ]);

                $new_empleado = Empleado::find($new_empleado->id)->makeHidden(['chr_password']);

                foreach ($req->list_id_local as $id_local) {
                    EmpleadoLocal::create([
                        'int_empleado'      => $new_empleado->id,
                        'int_local'         => $id_local,
                        'int_rolid'         => $new_empleado->int_rolid,
                        'int_usercreated'   => $new_empleado->int_usercreated,
                        'int_datecreated'   => $time,
                    ]);
                }

                $list_local = EmpleadoLocal::where("int_empleado", $new_empleado->id)->get();

                return response()->json([
                    'success'   => true,
                    'message'   => 'Usuario registrado exitosamente!',
                    'data'      => [
                        "new_empleado"  => $new_empleado,
                        "list_local"    => $list_local,
                    ],
                ], 200);
            }
        }else{
            return response()->json([
                'success' => false,
                'message' => "El usuario que intento hacer esta operación no se encuentra logeado en el sistema!"
            ], 406);
        }
    }

    public function buscarUsuario($row_by_page, Request $req){
        $hash = $req->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if($checkToken){

            switch($req->type_search){
                case 1:
                    $lista_empleados = DB::table('tbl_empleados as emp')->join('tbl_rol as ro', 'ro.id', '=', 'emp.int_rolid')
                                                                        ->where('emp.is_active', 1)
                                                                        ->where('emp.chr_codempleado', 'LIKE', "%$req->filter_value%")
                                                                        ->select('emp.id', 'emp.chr_codempleado', 'emp.chr_nombre', 'emp.chr_apellido', 'emp.chr_celular', 'emp.chr_documento', 'ro.chr_nombre as rol')
                                                                        ->orderBy('emp.id', 'desc')
                                                                        ->paginate($row_by_page);
                    break;
                case 2:
                    $lista_empleados = DB::table('tbl_empleados as emp')->join('tbl_rol as ro', 'ro.id', '=', 'emp.int_rolid')
                                                                        ->where('emp.is_active', 1)
                                                                        ->where(DB::raw("CONCAT_WS(' ', emp.chr_nombre, emp.chr_apellido)"), 'LIKE', "%$req->filter_value%")
                                                                        ->orWhere(DB::raw("CONCAT_WS(' ', emp.chr_apellido, emp.chr_nombre)"), 'LIKE', "%$req->filter_value%")
                                                                        ->select('emp.id', 'emp.chr_codempleado', 'emp.chr_nombre', 'emp.chr_apellido', 'emp.chr_celular', 'emp.chr_documento', 'ro.chr_nombre as rol')
                                                                        ->orderBy('emp.id', 'desc')
                                                                        ->paginate($row_by_page);
                    break;
                case 3:
                    $lista_empleados = DB::table('tbl_empleados as emp')->join('tbl_rol as ro', 'ro.id', '=', 'emp.int_rolid')
                                                                        ->where('emp.is_active', 1)
                                                                        ->where('emp.chr_documento', 'LIKE', "%$req->filter_value%")
                                                                        ->select('emp.id', 'emp.chr_codempleado', 'emp.chr_nombre', 'emp.chr_apellido', 'emp.chr_celular', 'emp.chr_documento', 'ro.chr_nombre as rol')
                                                                        ->orderBy('emp.id', 'desc')
                                                                        ->paginate($row_by_page);
                    break;
                default:
                    $lista_empleados = DB::table('tbl_empleados as emp')->join('tbl_rol as ro', 'ro.id', '=', 'emp.int_rolid')
                                                                        ->where('emp.is_active', 1)
                                                                        ->select('emp.id', 'emp.chr_codempleado', 'emp.chr_nombre', 'emp.chr_apellido', 'emp.chr_celular', 'emp.chr_documento', 'ro.chr_nombre as rol')
                                                                        ->orderBy('emp.id', 'desc')
                                                                        ->paginate($row_by_page);
            }
    
            return response()->json([
                'success'   => true,
                'data'      => $lista_empleados,
            ], 200);
          
        }else{
            return response()->json([
                'success' => false,
                'message' => "El usuario que intento hacer esta operación no se encuentra logeado en el sistema!"
            ], 406);
        }
    }

    public function buscarUsuarioID($id, Request $req){
        $hash = $req->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if($checkToken){
            $empleado = DB::table('tbl_empleados as emp')->join('tbl_empleados_local as el', 'el.int_empleado', '=', 'emp.id')
                                                         ->join('ubigeo_peru_districts as updi', 'updi.id', '=', 'emp.ubigeo_id')
                                                         ->join('ubigeo_peru_departments as upde', 'upde.id', '=', 'updi.department_id')
                                                         ->join('ubigeo_peru_provinces as uppr', 'uppr.id', '=', 'updi.province_id')
                                                         ->where('emp.id', $id)
                                                         ->where('emp.is_active', 1)
                                                         ->select('emp.id', 'emp.chr_codempleado', 'emp.chr_nombre', 'emp.chr_apellido', 'emp.chr_telefono', 'emp.chr_celular', 'emp.chr_direccion', 'emp.chr_documento', 'emp.chr_correo', 'emp.chr_direccion', 'emp.chr_urlfoto', 'emp.int_rolid', 'el.id as empleado_local_id', 'updi.id as district_id', 'upde.id as department_id', 'uppr.id as province_id', 'emp.chr_urlfoto')
                                                         ->first();

            $list_local = DB::table('tbl_empleados_local as el')->join('tbl_locales as lo', 'lo.id', '=', 'el.int_local')
                                                                ->where('el.int_empleado', $empleado->id)
                                                                ->where('el.is_active', 1)
                                                                ->select('lo.id', 'lo.chr_nombre', 'lo.chr_codlocal', 'el.id as id_empleado_local')
                                                                ->get();
        
            return response()->json([
                'success'   => true,
                'data'      => [
                    'empleado'      => $empleado,
                    'list_local'    => $list_local,
                ],
            ], 200);
          
        }else{
            return response()->json([
                'success' => false,
                'message' => "El usuario que intento hacer esta operación no se encuentra logeado en el sistema!"
            ], 406);
        }
    }

    public function actualizarUsuario($id, Request $req){
        $hash = $req->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if($checkToken){
            $this->validate($req, [
                'chr_nombre'        => 'required|max:80|string',
                'chr_apellido'      => 'required|max:80|string',
                'chr_celular'       => 'required|max:9|min:9|string',
                'chr_direccion'     => 'required|max:80|string',
                'chr_correo'        => 'required|email',
                'int_usermodified'  => 'required|integer',
                'int_rolid'         => 'required|integer',
                'ubigeo_id'         => 'required',
                'list_local'        => 'required',
            ]);

            $empleado = Empleado::find($id);

            $checkExistsCelular = Empleado::where('chr_celular', $req->chr_celular)->count();
            if ($checkExistsCelular >= 1 && $empleado->chr_celular != $req->chr_celular) {
                return response()->json([
                    'success' => false,
                    'message' => 'Este celular ya se encuentra registrado!'
                ], 406);
            }
            $checkExistsEmail = Empleado::where('chr_correo', $req->chr_correo)->count();
            if ($checkExistsEmail >= 1 && $empleado->chr_correo != strtoupper($req->chr_correo)) {
                return response()->json([
                    'success' => false,
                    'message' => 'Este correo electrónico ya se encuentra registrado!'
                ], 406);
            }
            $checkExistsUserModified = Empleado::find($req->int_usermodified);
            if(!$checkExistsUserModified){
                return response()->json([
                    'success' => false,
                    'message' => 'El usuario que intento hacer esta operación, no existe en el sistema!',
                ], 406);
            }
            if($checkExistsUserModified){
                $time = time();

                $img_name = '';
                if($req->file_photo){
                    $imgBase64 = new ImgBase64();
                    $img_data = $imgBase64->getB64Image($req->file_photo);  // get data image
                    $img_extension = $imgBase64->getB64Extension($req->file_photo); // get extension imagen
                    $img_name = $req->chr_documento . time() . '.' . $img_extension; // set name random for image
                    Storage::disk('public')->put('images/' . $img_name, $img_data);  // save image in disk storage
                }

                $empleado->update([
                    'chr_codempleado'   => $empleado->chr_codempleado,
                    'chr_nombre'        => strtoupper($req->chr_nombre),
                    'chr_apellido'      => strtoupper($req->chr_apellido),
                    'chr_telefono'      => $req->chr_telefono,
                    'chr_celular'       => $req->chr_celular,
                    'chr_direccion'     => strtoupper($req->chr_direccion),
                    'chr_correo'        => strtoupper($req->chr_correo),
                    'int_usermodified'  => $req->int_usermodified,
                    'int_datemodified'  => $time,
                    'chr_urlfoto'       => $req->file_photo ? '/storage/images/' . $img_name : $empleado->chr_urlfoto, 
                    'int_rolid'         => $req->int_rolid,
                    'ubigeo_id'         => $req->ubigeo_id,
                ]);
               
                $empleado = Empleado::find($empleado->id)->makeHidden(['chr_password']);

                 $list_empleado_local = EmpleadoLocal::where('int_empleado', $empleado->id)
                                                      ->where('is_active', 1)
                                                      ->get();
                
                $list_delete_empleado_local = [];
                $list_delete_local = [];
                $list_new_local = [];

                foreach ($req->list_local as $local) { 
                                      
                    $exists = false;
                    foreach ($list_empleado_local as $emp_loc) {  
                        
                        if($emp_loc->int_local == $local['id']){ 
                            $exists = true;
                            break;
                        }
                    }

                    if($exists==false){
                        array_push($list_new_local, $local['id']);
                    }
                }

                foreach ($list_new_local as $id) {  
                    EmpleadoLocal::create([
                        'int_empleado'      => $empleado->id,
                        'int_local'         => $id,
                        'int_rolid'         => $empleado->int_rolid,
                        'int_usercreated'   => $req->int_usermodified,
                        'int_datecreated'   => $time,
                    ]);
                }
                          
                foreach ($list_empleado_local as $emp_local) { 
                    $exists = false;

                    foreach ($req->list_local as $local) { 

                        if($local['id'] == $emp_local->int_local){ 
                            $exists = true;
                            break;
                        }
                    }

                    if($exists == false){
                        array_push($list_delete_local, $emp_local->int_local);
                        array_push($list_delete_empleado_local, $emp_local->id);
                    }
                }

                foreach ($list_delete_empleado_local as $id_empleado_local) {
                    $empleado_local = EmpleadoLocal::find($id_empleado_local);

                    $empleado_local->update([
                        'int_usermodified'  => $empleado->id,
                        'int_datemodified'  => $time,
                        'is_active'         => 0,
                        'is_deleted'        => 1,
                    ]);
                }

                return response()->json([
                    'success'   => true,
                    'message'   => 'Usuario actualizado exitosamente!',
                    'data'      => $empleado,
                ], 200);
            }
        }else{
            return response()->json([
                'success' => false,
                'message' => "El usuario que intento hacer esta operación no se encuentra logeado en el sistema!"
            ], 406);
        }
    }

    public function eliminarUsuario($id_emp, Request $req){
        $hash = $req->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if($checkToken){
            $empleado = Empleado::find($id_emp);
            $empleado->update([
                'is_active' => 0,
                'is_deleted' => 1,
            ]);

            $list_empleado_local = EmpleadoLocal::where('int_empleado', $empleado->id)
                                                ->where('is_active', 1)
                                                ->get();

            foreach ($list_empleado_local as $value) {
                $local = EmpleadoLocal::find($value->id);
                $local->update([
                    'is_active' => 0,
                    'is_deleted' => 1,
                ]);
            }

            return response()->json([
                'success' => true,
                'message' => 'Usuario eliminado exitosamente!'
            ], 200);
        }else{
            return response()->json([
                'success' => false,
                'message' => "El usuario que intento hacer esta operación no se encuentra logeado en el sistema!"
            ], 406);
        }
    }

    public function getMenuByRol($rolid){
        //obtenemos los padres
        $padres = RoleMenu::from('tbl_rol_menu as rm')
                         ->select('m.*')
                         ->join('tbl_menu as m','m.id','=','rm.int_menuid')
                         ->where('rm.int_rolid',$rolid)
                         ->where('m.int_level',self::MENU_LEVEL_PADRE)
                         ->where('rm.is_active',1)
                         ->where('m.is_active',1)
                         ->where('rm.is_deleted',0)
                         ->where('m.is_deleted',0)
                         ->get();
        if (count($padres)>0) {
            foreach ($padres as $p => $padre) {
                //obtenemos los hijos por padre
                $hijos = RoleMenu::from('tbl_rol_menu as rm')
                         ->select('m.*')
                         ->join('tbl_menu as m','m.id','=','rm.int_menuid')
                         ->where('rm.int_rolid',$rolid)
                         ->where('m.int_padreid',$padre->id)
                         ->where('m.int_level',self::MENU_LEVEL_HIJO)
                         ->where('rm.is_active',1)
                         ->where('m.is_active',1)
                         ->where('rm.is_deleted',0)
                         ->where('m.is_deleted',0)
                         ->get();
                $padre->hijos = $hijos;
                $padre->num_hijos = count($hijos) > 0 ? count($hijos) : 0;   
            }
        }
        return $padres;
    }

    public function test(Request $request){
        echo "MARCO CABROOO222";
        die;
    }

    public function getLocales(){
        $lista_locales = DB::table('tbl_locales')->where('is_active', 1)
                                                 ->select('id',  'chr_codlocal', 'chr_nombre', 'chr_direccion', 'chr_telefono')
                                                 ->orderBy('id', 'asc')
                                                 ->get();

        return response()->json([
            'success'   => true,
            'data'      => $lista_locales
        ], 200);
    }
}
